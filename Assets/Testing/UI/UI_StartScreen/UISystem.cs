// GENERATED AUTOMATICALLY FROM 'Assets/Testing/UI/UI_StartScreen/UISystem.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @UISystem : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @UISystem()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""UISystem"",
    ""maps"": [
        {
            ""name"": ""StartScreen"",
            ""id"": ""eb26a73a-a952-4bcb-b425-1e27711f159f"",
            ""actions"": [
                {
                    ""name"": ""Return"",
                    ""type"": ""Button"",
                    ""id"": ""3a4d6470-8675-4250-b527-1b878cc5e25b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2d6c8ce5-9b72-43f8-b1f9-46d8775b4cbb"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Return"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // StartScreen
        m_StartScreen = asset.FindActionMap("StartScreen", throwIfNotFound: true);
        m_StartScreen_Return = m_StartScreen.FindAction("Return", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // StartScreen
    private readonly InputActionMap m_StartScreen;
    private IStartScreenActions m_StartScreenActionsCallbackInterface;
    private readonly InputAction m_StartScreen_Return;
    public struct StartScreenActions
    {
        private @UISystem m_Wrapper;
        public StartScreenActions(@UISystem wrapper) { m_Wrapper = wrapper; }
        public InputAction @Return => m_Wrapper.m_StartScreen_Return;
        public InputActionMap Get() { return m_Wrapper.m_StartScreen; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(StartScreenActions set) { return set.Get(); }
        public void SetCallbacks(IStartScreenActions instance)
        {
            if (m_Wrapper.m_StartScreenActionsCallbackInterface != null)
            {
                @Return.started -= m_Wrapper.m_StartScreenActionsCallbackInterface.OnReturn;
                @Return.performed -= m_Wrapper.m_StartScreenActionsCallbackInterface.OnReturn;
                @Return.canceled -= m_Wrapper.m_StartScreenActionsCallbackInterface.OnReturn;
            }
            m_Wrapper.m_StartScreenActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Return.started += instance.OnReturn;
                @Return.performed += instance.OnReturn;
                @Return.canceled += instance.OnReturn;
            }
        }
    }
    public StartScreenActions @StartScreen => new StartScreenActions(this);
    public interface IStartScreenActions
    {
        void OnReturn(InputAction.CallbackContext context);
    }
}
