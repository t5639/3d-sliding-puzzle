namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using UnityEngine;

    public class TestTileDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The doors on each side.")]
        private CardinalPointCollection<GameObject> entries = default;

        [SerializeField, Tooltip("The walls of the tile.")]
        private GameObject walls = default;

        [SerializeField, Tooltip("The ground of the tile.")]
        private GameObject ground = default;

        [SerializeField, Tooltip("The size of the tile.")]
        private Vector2 size = default;
        /// <summary>
        /// The size of the tile.
        /// </summary>
        public Vector2 Size => size;


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            Vector3[] corners = new Vector3[4];
            corners[0] = transform.position;
            corners[1] = corners[0] + transform.forward * size.y * transform.lossyScale.z;
            corners[3] = corners[0] + transform.right * size.x * transform.lossyScale.x;
            corners[2] = corners[1] + transform.right * size.x * transform.lossyScale.x;
            Gizmos.color = Color.red;
            for (int i = 0; i < corners.Length; i++)
            {
                Gizmos.DrawLine(corners[i], corners[(i + 1) % 4]);
            }
        }
#endif

        /// <summary>
        /// Displays the tile.
        /// </summary>
        /// <param name="tile">The tile to display.</param>
        public void Display(TileStructure tile)
        {
            if (tile is BorderedStructure)
            {
                foreach (CardinalPoint point in System.Enum.GetValues(typeof(CardinalPoint)))
                {
                    entries.GetValue(point).SetActive(!(tile as BorderedStructure).Entries.HasFlag((CardinalPoints)point));
                }
                walls.SetActive(true);
                ground.SetActive(true);
            }
            else
            {
                if (!tile)
                {
                    ground.SetActive(false);
                }
                foreach (var entry in entries)
                {
                    entry.Value.SetActive(false);
                }
                walls.SetActive(false);
            }
        }
    }
}
