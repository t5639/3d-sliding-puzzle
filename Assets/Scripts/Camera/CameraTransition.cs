using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class CameraTransition : MonoBehaviour
{
    private bool isScaling = false;
    private Queue<GameObject> nextPosition = new Queue<GameObject>();
    public Transform target, bigTarget;
    private Transform smallTransform, bigTransform;
    [Range(0,50)]
    public float transitionSpeedMS;
    public bool isDuration = false;
    [Range(0,0.49f)]
    public float curveHeightPercentage;

    private float startTime, endTime, currTime;

    public UnityEvent OnTransitionEnd;

    private void Start()
    {
        smallTransform = new GameObject("smallTransform").transform;
        bigTransform = new GameObject("bigTransform").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (isScaling)
        {
            UpdatePosition();
        }

    }

    public void Transition(Transform from, Transform to, bool fromQueue = false)
    {
        if (!isScaling || fromQueue)
        {
            smallTransform.position = from.transform.position;
            smallTransform.rotation = from.transform.rotation;
            smallTransform.localScale = from.transform.localScale;
            bigTransform.position = to.transform.position;
            bigTransform.rotation = to.transform.rotation;
            bigTransform.localScale = to.transform.localScale;

            startTime = Time.realtimeSinceStartup;
            endTime = isDuration ? startTime + transitionSpeedMS : startTime + Vector3.Distance(from.position, to.position)/transitionSpeedMS;
            isScaling = true;
        }
        else
        {
            nextPosition.Enqueue(to.gameObject);
        }
    }


    public void UpdatePosition()
    {
        currTime = Time.realtimeSinceStartup;
        if (currTime > startTime && currTime < endTime)
        {
                //zoomout

                float newScale = map(currTime, endTime, startTime, bigTransform.localScale.x, smallTransform.localScale.x);
                float newX = map(currTime, endTime, startTime, bigTransform.position.x, smallTransform.position.x);
                float newY = map(currTime, endTime, startTime, bigTransform.position.y, smallTransform.position.y);
                float newZ = map(currTime, endTime, startTime, bigTransform.position.z, smallTransform.position.z);

                //QualitySettings.shadowDistance = map(currTime, endTime, startTime, 100, 20);
               // mixer.SetFloat("bigVolume", map(currTime, endTime, startTime, 0, -80));

                float rotY,rotX,rotZ;
                rotX = map(currTime, endTime, startTime, bigTransform.eulerAngles.x, smallTransform.eulerAngles.x);
                rotZ = map(currTime, endTime, startTime, bigTransform.eulerAngles.z, smallTransform.eulerAngles.z);
                if (smallTransform.eulerAngles.y > 180)
                {
                    rotY = map(currTime, endTime, startTime, bigTransform.eulerAngles.y+360, smallTransform.eulerAngles.y);

                }
                else
                {
                    rotY = map(currTime, endTime, startTime, bigTransform.eulerAngles.y, smallTransform.eulerAngles.y);

                }
            target.transform.eulerAngles = new Vector3(rotX, rotY, rotZ);
            target.transform.localScale = new Vector3(newScale, newScale, newScale);
            target.transform.position = new Vector3(newX, newY, newZ);
        }
        else
        {
            if (nextPosition.Count > 0)
            {
                GameObject go = nextPosition.Dequeue();
                Transition(target.transform, go.transform, true);
                if(go.tag == "cameraTransform")
                {
                    Destroy(go);
                }
            }
            else
            {
                OnTransitionEnd.Invoke();
                isScaling = false;
            }
        }

    }


    public void PlayerTransition(Transform player, Transform bigTransform)
    {
        Vector3 distance = bigTransform.position - player.position;
        float curveHeight = Mathf.Abs(distance.y) * curveHeightPercentage;
        float curveX = Mathf.Abs(distance.x) * curveHeightPercentage;
        float curveZ = Mathf.Abs(distance.z) * curveHeightPercentage;
        GameObject t = new GameObject();
        t.tag = "cameraTransform";
        t.transform.position = player.position + new Vector3(0, distance.y*curveHeightPercentage, 0);
        t.transform.eulerAngles = new Vector3(90,0, 0);
        Transition(player, t.transform);
        Transition(player, bigTransform);
    }


    public bool IsScaling()
    {
        return isScaling;
    }

    private float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

}
