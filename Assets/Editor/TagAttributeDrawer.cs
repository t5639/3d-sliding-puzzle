using UnityEngine;
using UnityEditor;

/// <summary>
/// The <see cref="PropertyDrawer"/> for the <see cref="TagAttribute"/>.
/// </summary>
[CustomPropertyDrawer(typeof(TagAttribute))]
public class TagAttributeDrawer : PropertyDrawer
{
    /// <inheritdoc/>
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
            property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
        }
        else
        {
            EditorGUI.HelpBox(position, $"Unable to draw the \"{ label.text }\" field, the tag attribute can only be used on strings.", MessageType.Error);
        }
    }

    /// <inheritdoc/>
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.String)
        {
            return 37f;
        }
        return base.GetPropertyHeight(property, label);
    }
}