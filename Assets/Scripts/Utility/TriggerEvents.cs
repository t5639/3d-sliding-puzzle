namespace ShiftingMaze.Utilities
{
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Events;

    public class TriggerEvents : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tag, Tooltip("The tags to listen for.")]
        private string[] tags = default;
        /// <summary>
        /// The tags to listen for.
        /// </summary>
        public string[] Tags => tags;

        [Space, SerializeField, Tooltip("Called when an object with one of he given tags enters the trigger.")]
        private UnityEvent<Collider> onEnter = default;
        /// <summary>
        /// Called when an object with one of he given tags enters the trigger.
        /// </summary>
        public event UnityAction<Collider> OnEnter
        {
            add => onEnter.AddListener(value);
            remove => onEnter.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when an object with on of the given tags leaves the trigger.")]
        private UnityEvent<Collider> onExit = default;
        /// <summary>
        /// Called when an object with on of the given tags leaves the trigger.
        /// </summary>
        public event UnityAction<Collider> OnExit
        {
            add => onExit.AddListener(value);
            remove => onExit.RemoveListener(value);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnTriggerEnter(Collider other)
        {
            if (tags.Contains(other.tag))
            {
                onEnter?.Invoke(other);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (tags.Contains(other.tag))
            {
                onExit?.Invoke(other);
            }
        }
    }
}
