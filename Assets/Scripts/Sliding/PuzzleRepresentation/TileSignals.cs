namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Signals;
    using UnityEngine;

    public class TileSignals : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The signal points of the tile.")]
        private CardinalPointCollection<SignalPoint> points = default;
        /// <summary>
        /// The signal points of the tile.
        /// </summary>
        public ICardinalPointCollection<SignalPoint, SignalPoint, SignalPoint, SignalPoint, SignalPoint> Points => points;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            if (transform.localEulerAngles.y != 0f)
            {
                CardinalPointCollection<SignalPoint> points = new CardinalPointCollection<SignalPoint>();
                foreach (var point in this.points)
                {
                    points.SetValue(CardinalPointUtilities.RotatateClockwise(point.Key, transform.localEulerAngles.y), point.Value);
                }
                this.points = points;
            }
        }

        /// <summary>
        /// Updates the position of the points.
        /// </summary>
        public void UpdatePointPositions()
        {
            foreach (var point in points)
            {
                point.Value?.UpdatePosition(true);
            }
        }
    }
}
