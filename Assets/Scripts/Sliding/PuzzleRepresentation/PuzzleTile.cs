namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// The base class for all level tiles.
    /// </summary>
    public class PuzzleTile : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The structure of the tile.")]
        protected TileStructure structure = default;
        /// <summary>
        /// The structure of the tile.
        /// </summary>
        public TileStructure Structure => structure;


        // --- | Variables | -----------------------------------------------------------------------------------------------

//private CardinalPointCollection<PuzzleTile> neighbours = new CardinalPointCollection<PuzzleTile>();

//public CardinalPointCollection<PuzzleTile> Neighbours => neighbours;


// --- | Methods | -------------------------------------------------------------------------------------------------


        /*
        /// <summary>
        /// Sets the neighbour of the tile.
        /// </summary>
        /// <param name="direction">The direction of the neighbour.</param>
        /// <param name="tile">The next tile in the given direction.</param>
        public virtual void SetNeighbour(CardinalPoint direction, PuzzleTile tile)
        {
            neighbours.SetValue(direction, tile);
        }
        public virtual void SetNeighbours(CardinalPointCollection<PuzzleTile> neighbours)
        {
            this.neighbours = neighbours;
        }
        */

        /// <summary>
        /// Checks if a tile can be move.
        /// </summary>
        /// <returns>True if the tile can be moved</returns>
        public virtual bool MoveCheck() => structure == null ? false : structure is BorderedStructure ? (structure as BorderedStructure).Moveable : true;

        /// <summary>
        /// Checks if the tile can be entered from a spesific side.
        /// </summary>
        /// <param name="side">The side to enter from.</param>
        /// <returns>True if the tile can be entered from the given side.</returns>
        public virtual bool HasEntry(CardinalPoint side)
            => structure is BorderedStructure ? (structure as BorderedStructure).Entries.HasFlag((CardinalPoints)side) : false;

        /// <summary>
        /// Returns all sides that can be reachd from a entry.
        /// </summary>
        /// <param name="point">The point to enter from.</param>
        /// <returns>The side reachable from the entry.</returns>
        public IEnumerable<CardinalPoint> GetConnections(CardinalPoint point)
        {
            if (HasEntry(point))
            {
                if (structure is FieldStructure)
                {
                    foreach (CardinalConnection connection in (CardinalConnection[])System.Enum.GetValues(typeof(CardinalConnection)))
                    {
                        (CardinalPoint a, CardinalPoint b) connetedPoints = connection.ToPoint();
                        if (connetedPoints.a == point || connetedPoints.b == point)
                        {
                            if ((structure as FieldStructure).Connections.HasFlag((CardinalConnections)connection))
                            {
                                yield return connetedPoints.a == point ? connetedPoints.b : connetedPoints.a;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds the <see cref="PuzzleTile"/> script to a <see cref="GameObject"/>.
        /// </summary>
        /// <param name="target">The gameobject to add the script.</param>
        /// <param name="strcuture">The structure of the tile.</param>
        /// <returns>The tile script with the given structure.</returns>
        public static PuzzleTile AddComponent(GameObject target, TileStructure strcuture)
        {
            PuzzleTile tile = target.AddComponent<PuzzleTile>();
            tile.structure = strcuture;
            return tile;
        }
    }
}
