namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class PuzzleMatrix : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The layout of the puzzle.")]
        protected PuzzleLayout layout = default;
        /// <summary>
        /// The layout of the puzzle.
        /// </summary>
        public PuzzleLayout Layout => layout;

        [SerializeField, Tooltip("The size of the puzzle.")]
        protected Vector2 size = default;

        /// <summary>
        /// The size of the puzzle.
        /// </summary>
        public Vector2 Size => size;

        [SerializeField, Tooltip("The object holding the tiles.")]
        protected Transform container = default;
        /// <summary>
        /// The object holding the tiles.
        /// </summary>
        protected Transform Container => ObjectSetupCheck<Transform>(ref container, () => transform);

        [SerializeField, Tooltip("The offset from the bottom left corner of the grid to the center of the container.")]
        protected Vector3 offset = default;

        [Space, SerializeField, Tooltip("Called when a tile was moved.")]
        protected UnityEvent<Vector2Int, PuzzleTile, Vector2Int> onMoved = default;
        /// <summary>
        /// Called when a tile was moved.
        /// </summary>
        public event UnityAction<Vector2Int, PuzzleTile, Vector2Int> OnMoved
        {
            add => onMoved.AddListener(value);
            remove => onMoved.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the order of the tiles changed.")]
        protected UnityEvent<IEnumerable<(PuzzleTile tile, Vector2Int coorinate)>> onSynchronized = default;
        /// <summary>
        /// Called when the order of the tiles changed.
        /// </summary>
        public event UnityAction<IEnumerable<(PuzzleTile tile, Vector2Int coorinate)>> OnSynchronized
        {
            add => onSynchronized.AddListener(value);
            remove => onSynchronized.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The tiles managed by the matrix.
        /// </summary>
        protected abstract PuzzleTile[] Tiles { get; set; }

        private PuzzleTile[] backupLayout = null;

        /// <summary>
        /// The amount of tiles along the x axis.
        public int Width => layout.Width;
        /// </summary>
        /// The amount of tiles along the y axis.
        /// </summary>
        public int Height => layout.Height;

        /// <summary>
        /// True if the matrix is setup.
        /// </summary>
        public bool IsSetup { get; protected set; }
        private System.Action<PuzzleMatrix> onSetup = default;

        private static Dictionary<PuzzleLayout, List<PuzzleMatrix>> groups = new Dictionary<PuzzleLayout, List<PuzzleMatrix>>();

        private Vector3 localOrigin = GetVectorNaN(), globalOrigin = GetVectorNaN();
        /// <summary>
        /// The offset from the bottom left corner of the grid to the center of the container.
        /// </summary>
        public Vector3 LocalOrigin => VectorSetupCheck(ref localOrigin, () => offset);
        /// <summary>
        /// The origin of the grid in world space.
        /// </summary>
        public Vector3 GlobalOrigin => VectorSetupCheck(ref globalOrigin, () => Container.TransformPoint(offset));

        private Vector3 localGridSize = GetVectorNaN(), globalGridSize = GetVectorNaN();
        /// <summary>
        /// The size of the grid in object scace.
        /// </summary>
        public Vector3 LocalGridSize => VectorSetupCheck(ref localGridSize, () => new Vector3(size.x, 0f, size.y));
        /// <summary>
        /// The size of the grid in world space.
        /// </summary>
        public Vector3 GlobalGridSize => VectorSetupCheck(ref globalGridSize, () => new Vector3(size.x * Container.lossyScale.x, 0f, size.y * Container.lossyScale.z));


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            if (layout == null)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireCube(GlobalOrigin, GlobalGridSize);
            }
            else
            {
                Gizmos.color = Color.cyan;
                float xStep = size.x / layout.Width;
                float yStep = size.y / layout.Height;

                for (int y = 0; y <= layout.Height; y++)
                {
                    Gizmos.DrawRay
                    (
                        GlobalOrigin + Container.forward * y * yStep * Container.lossyScale.z,
                        Container.right * size.x * Container.lossyScale.x
                    );
                }
                for (int x = 0; x <= layout.Width; x++)
                {
                    Gizmos.DrawRay
                    (
                        GlobalOrigin + Container.right * x * xStep * Container.lossyScale.x,
                        Container.forward * size.y * Container.lossyScale.z
                    );
                }
            }
        }

#endif

        protected virtual void OnEnable()
        {
            if (!groups.ContainsKey(layout))
            {
                groups[layout] = new List<PuzzleMatrix>();
            }
            groups[layout].Add(this);
            CallAfterSetup(m =>
            {
                if (groups[layout].Count > 1)
                {
                    SynchronizeMatrix(groups[layout][0].Tiles);
                }
            });
        }

        protected virtual void OnDisable()
        {
            groups[layout].Remove(this);
        }


        // Movement -----------------------------------------------------------------------------------

        /// <summary>
        /// Moves a tile in the managed matrix.
        /// </summary>
        /// <param name="cooridinates">The coordinates of the tile.</param>
        /// <param name="direction">The direction to move the tile.</param>
        public void Move(Vector2Int cooridinates, CardinalPoint direction)
        {
            // Try to move the tile.
            if (TryMoveTile(cooridinates, direction))
            {
                // Check if a backup is needed.
                if (backupLayout == null)
                {
                    MakeBackup();
                }
                // Get the coordinates of the tile the current one has been swapped to.
                Vector2Int otherCord = cooridinates + direction.ToVector2Int();
                // Signal other scripts that the coordinates went through a change.
                onMoved?.Invoke(cooridinates, GetTile(otherCord), otherCord);
                onMoved?.Invoke(otherCord, GetTile(cooridinates), cooridinates);
            }
        }


        /// <summary>
        /// Try to move a tile inside the matrix.
        /// </summary>
        /// <param name="coordinates">The coordinates of the tile.</param>
        /// <param name="direction">The direction to move the tile.</param>
        /// <returns>True if the tile could be moved.</returns>
        protected bool TryMoveTile(Vector2Int coordinates, CardinalPoint direction)
        {
            Vector2Int otherCoordinate = coordinates + direction.ToVector2Int();
            // Check if the coordinates of the two tiles are inside the matrix.
            if (!InsideCheck(coordinates) || !InsideCheck(otherCoordinate))
            {
                return false;
            }
            // Get the indexes of the two tiles.
            int targetndex = CoordinatesToIndex(coordinates);
            int otherIndex = CoordinatesToIndex(otherCoordinate);
            // Get the to tiles.
            PuzzleTile targetTile = Tiles[targetndex];
            PuzzleTile otherTile = Tiles[otherIndex];
            // Check if the tiles can be moved.
            if (!targetTile || !otherTile)
            {
                return false;
            }
            else
            {
                int targetState = targetTile.Structure is PitStructure ? 2 : targetTile.MoveCheck() ? 1 : 0;
                int otherState = otherTile.Structure is PitStructure ? 2 : otherTile.MoveCheck() ? 1 : 0;
                if (targetState + otherState < 3)
                {
                    return false;
                }
                else
                {
                    // Switch the two tiles.
                    Tiles[otherIndex] = Tiles[targetndex];
                    Tiles[targetndex] = otherTile;
                    //SwitchNeighbours(targetTile, otherTile);
                    return true;
                }
            }
        }

        // Backup -------------------------------------------------------------------------------------

        /// <summary>
        /// Applys the changes in the matrix to all matrixes unsing the same layout.
        /// </summary>
        [ContextMenu("Apply Changes")]
        public void ApplyChanges()
        {
            foreach (PuzzleMatrix matrix in groups[layout])
            {
                if (matrix != this)
                {
                    matrix.SynchronizeMatrix(Tiles);
                }
            }
            MakeBackup();
        }

        /// <summary>
        /// Reverts to the saved layout.
        /// </summary>
        public void RevertToBackup()
        {
            if (backupLayout != null)
            {
                Tiles = backupLayout.Clone() as PuzzleTile[];
                onSynchronized?.Invoke(GetAllTilesWithCoordinates());
            }
        }

        /// <summary>
        /// Saves the current level layout.
        /// </summary>
        private void MakeBackup()
        {
            backupLayout = Tiles.Clone() as PuzzleTile[];
        }

        // Synchronization ----------------------------------------------------------------------------

        /// <summary>
        /// Applies the layout of a matrix.
        /// </summary>
        /// <param name="tiles">The new order of the tiles.</param>
        private void SynchronizeMatrix(PuzzleTile[] tiles)
        {
            int length = Width * Height;
            int foundTiles = 0;
            // Loop over all tiles in the new layout.
            for (int i = 0; i < length; i++)
            {
                // Skip if the tile is at the correct position.
                if (Tiles[i].Structure == tiles[i].Structure)
                {
                    foundTiles++;
                    continue;
                }
                // Find the matching tile.
                for (int j = i + 1; j < length; j++)
                {
                    if (Tiles[j].Structure == tiles[i].Structure)
                    {
                        // Swap the tile with the one in it's position.
                        PuzzleTile tile = Tiles[i];
                        Tiles[i] = Tiles[j];
                        Tiles[j] = tile;
                        foundTiles++;
                        break;
                    }
                }
            }
            if (foundTiles < length)
            {
                throw new System.Exception("Matrix could not be synchronized, not all tiles were found.");
            }
            backupLayout = null;
            //SetNeighbours();
            onSynchronized?.Invoke(GetAllTilesWithCoordinates());
        }

        public IEnumerable<(PuzzleTile tile, Vector2Int coordinate)> GetAllTilesWithCoordinates()
        {
            List<(PuzzleTile tile, Vector2Int coordinate)> positions = new List<(PuzzleTile tile, Vector2Int coordinate)>();
            foreach (var position in EachTile())
            {
                positions.Add(position);
            }
            return positions;
        }

        // Setup --------------------------------------------------------------------------------------

        /// <summary>
        /// Performs the given action when the matrix is setup.
        /// </summary>
        /// <param name="onSetup">The action to perform.</param>
        public void CallAfterSetup(System.Action<PuzzleMatrix> onSetup)
        {
            if (IsSetup)
            {
                onSetup?.Invoke(this);
            }
            else
            {
                this.onSetup += onSetup;
            }
        }

        /// <summary>
        /// Enables other scripts to use the manager.
        /// </summary>
        protected void FinishSetup()
        {
            //SetNeighbours();
            IsSetup = true;
            onSetup?.Invoke(this);
            onSetup = null;
        }

        // Neighbours -----------------------------------------------------------------------------

        /*
        private void SetNeighbours()
        {
            Vector2Int coordinate = Vector2Int.zero;
            Vector2Int other;
            for (; coordinate.y < Height; coordinate.y++)
            {
                for (; coordinate.x < Width; coordinate.x++)
                {
                    foreach (CardinalPoint direction in (CardinalPoint[])System.Enum.GetValues(typeof(CardinalPoint)))
                    {
                        other = coordinate + direction.ToVector2Int();
                        Tiles[coordinate.x + coordinate.y * Width].SetNeighbour(direction, InsideCheck(other) ? GetTile(other) : null);
                    }
                }
                coordinate.x = 0;
            }
        }

        private void SwitchNeighbours(PuzzleTile a, PuzzleTile b)
        {
            CardinalPoint reflection = default;
            foreach (var tiles in a.Neighbours)
            {
                if (tiles.Value == b)
                {
                    a.SetNeighbour(tiles.Key, b.Neighbours.GetValue(tiles.Key));
                    a.Neighbours.GetValue(tiles.Key).SetNeighbour(tiles.Key.Invert(), a);
                }
                else if (b.Neighbours.GetValue(tiles.Key) == a)
                {
                    reflection = tiles.Key;
                    b.SetNeighbour(tiles.Key, a.Neighbours.GetValue(tiles.Key));
                    b.Neighbours.GetValue(tiles.Key).SetNeighbour(tiles.Key.Invert(), b);
                }
                else
                {
                    a.SetNeighbour(tiles.Key, b.Neighbours.GetValue(tiles.Key));
                    a.Neighbours.GetValue(tiles.Key).SetNeighbour(tiles.Key.Invert(), a);
                    b.SetNeighbour(tiles.Key, tiles.Value);
                    b.Neighbours.GetValue(tiles.Key).SetNeighbour(tiles.Key.Invert(), b);
                }
            }
            a.SetNeighbour(reflection, b);
            b.SetNeighbour(reflection.Invert(), a);
        }
        */

        // Tiles --------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the the tile at the given coordinates.
        /// </summary>
        /// <param name="coordinates">The coordinates of the tile.</param>
        /// <returns>The tile at the given coordinates.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the given coordinates are outside the matrix.</exception>
        public PuzzleTile GetTile(Vector2Int coordinates)
        {
            try
            {
                return Tiles[CoordinatesToIndex(coordinates)];
            }
            catch (System.IndexOutOfRangeException)
            {
                throw new System.ArgumentOutOfRangeException("The given coordinates are outside the matrix.");
            }
        }

        /// <summary>
        /// Returns the first tile of the pit structure in the matrix.
        /// </summary>
        /// <returns>The first pit in the matrix.</returns>
        public (PuzzleTile tile, Vector2Int coordinates) GetPit()
        {
            foreach (var tileAtCooridnate in EachTile())
            {
                if (tileAtCooridnate.tile.Structure is PitStructure)
                {
                    return (tileAtCooridnate.tile, tileAtCooridnate.cooridnates);
                }
            }
            return (null, -Vector2Int.one);
        }

        /// <summary>
        /// Find a port in the matrix.
        /// </summary>
        /// <param name="port">The port to look for.</param>
        /// <returns>The tile for the port.</returns>
        public (PuzzleTile tile, Vector2Int coordinates) FindPort(StartStructure port)
        {
            foreach (var tileAtCooridnate in EachTile())
            {
                if (tileAtCooridnate.tile.Structure == port)
                {
                    return (tileAtCooridnate.tile, tileAtCooridnate.cooridnates);
                }
            }
            return (null, -Vector2Int.one);
        }


        /// <summary>
        /// Iterates over all tiles in the matrix.
        /// </summary>
        /// <returns>The tile and its coordinates.</returns>
        protected IEnumerable<(PuzzleTile tile, Vector2Int cooridnates)> EachTile()
        {
            Vector2Int coordinates = Vector2Int.zero;
            for (; coordinates.y < Height; coordinates.y++)
            {
                for (; coordinates.x < Width; coordinates.x++)
                {
                    yield return (GetTile(coordinates), coordinates);
                }
                coordinates.x = 0;
            }
        }

        // Coordinates --------------------------------------------------------------------------------

        /// <summary>
        /// Returns the index of a tile in with the given coordinates.
        /// </summary>
        /// <param name="coordinates">The coordinates of the tile.</param>
        /// <returns>The index of the tile at the given coordinates.</returns>
        protected int CoordinatesToIndex(Vector2Int coordinates)
            => coordinates.x + coordinates.y * layout.Width;

        /// <summary>
        /// Checks if the x value is inside the matrix.
        /// </summary>
        /// <param name="x">The value to check.</param>
        /// <returns>True if the value is inside the matrix.</returns>
        public bool HorizontalInsideCheck(int x)
        {
            return x >= 0 && x < layout.Width;
        }

        /// <summary>
        /// Checks if the y value is inside the matrix.
        /// </summary>
        /// <param name="y">The value to check.</param>
        /// <returns>True if the value is inside the matrix.</returns>
        public bool VerticalInsideCheck(int y)
        {
            return y >= 0 && y < layout.Height;
        }

        /// <summary>
        /// Checks if a coordinate is inside the matrix.
        /// </summary>
        /// <param name="x">The x coordinate to check.</param>
        /// <param name="y">the y coordinate to check.</param>
        /// <returns>True if the coordinate are inside.</returns>
        public bool InsideCheck(int x, int y)
        {
            return HorizontalInsideCheck(x) && VerticalInsideCheck(y);
        }
        /// <summary>
        /// Checks if a coordinate is inside the matrix.
        /// </summary>
        /// <param name="coordinates">The coordinates to check.</param>
        /// <returns>True if the coordinate are inside.</returns>
        public bool InsideCheck(Vector2Int coordinates)
            => InsideCheck(coordinates.x, coordinates.y);


        /// <summary>
        /// Checks if the vector is set up.
        /// </summary>
        /// <param name="target">The target vector.</param>
        /// <param name="value">The value to apply.</param>
        /// <returns>The vector.</returns>
        private static Vector3 VectorSetupCheck(ref Vector3 target, System.Func<Vector3> value)
        {
#if (UNITY_EDITOR)
            if (!UnityEditor.EditorApplication.isPlaying)
            {
                return value.Invoke();
            }
            else if (float.IsNaN(target.x))
#else
            if (target.x == float.NaN)
#endif
            {
                target = value.Invoke();
            }
            return target;
        }

        private static T ObjectSetupCheck<T>(ref T target, System.Func<T> value) where T: UnityEngine.Object
        {

#if (UNITY_EDITOR)
            if (!UnityEditor.EditorApplication.isPlaying)
            {
                return value.Invoke();
            }
            else if (target == null)
#else
            if (target == null)
#endif
            {
                target = value.Invoke();
            }
            return target;
        }

        /// <summary>
        /// Returns a vector with NaN values.
        /// </summary>
        /// <returns>A vector with NaN values.</returns>
        private static Vector3 GetVectorNaN() => new Vector3(float.NaN, float.NaN, float.NaN);


        // --- | Classes | -------------------------------------------------------------------------------------------------

        private class TileMove
        {
            public Vector2Int from;
            public Vector2Int to;

            public TileMove(Vector2Int from, Vector2Int to)
            {
                this.from = from;
                this.to = to;
            }
        }
















        /*


        private List<HashSet<SignalStrength>> tilesBySignalStrength = new List<HashSet<SignalStrength>>();
        private int[,] signalMap;

        [ContextMenu("Perform Signal Check")]
        public void PerformSignalCheck()
        {
            signalMap = new int[Width, Height];
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    Vector2Int cords = new Vector2Int(x, y);
                    PuzzleTile tile = GetTile(cords);
                    if (tile.Strcuture is StartStructure)
                    {
                        if (tilesBySignalStrength.Count == 0)
                        {
                            tilesBySignalStrength.Add(new HashSet<SignalStrength>());
                        }
                        tilesBySignalStrength[0].Add(new SignalStrength(cords, default));
                        signalMap[x, y] = 0;
                    }
                    signalMap[x, y] = -1;
                }
            }
            StartCoroutine(TestSignal(0));
        }

        private System.Collections.IEnumerator TestSignal(int strength)
        {
            // Iterate over all fields with the given signal strength.
            foreach (SignalStrength signalFlow in tilesBySignalStrength[strength])
            {
                Debug.Log($"Processing {signalFlow.coordinate}");
                PuzzleTile targetTile = GetTile(signalFlow.coordinate);
                if (targetTile.Strcuture is StartStructure)
                {
                    foreach (CardinalPoint direction in (CardinalPoint[])System.Enum.GetValues(typeof(CardinalPoint)))
                    {
                        if (targetTile.HasEntry(direction))
                        {
                            Vector2Int otherCoordinate = signalFlow.coordinate + direction.ToVector2Int();
                            Debug.Log($"Checking {direction} connection of {signalFlow.coordinate} leading to {otherCoordinate}");
                            yield return MakeCheck(direction, otherCoordinate);
                        }
                    }
                }
                else
                {
                    // Get the connections of the tile.
                    CardinalConnections connections = (targetTile.Strcuture as FieldStructure).Connections;
                    // Get all possible connections for the origin.
                    foreach (CardinalConnection connection in signalFlow.origin.EachConnection())
                    {
                        // Match the connections.
                        if (connections.HasFlag((CardinalConnections)connection))
                        {
                            // Get the other end of the connection.
                            (CardinalPoint a, CardinalPoint b) points = connection.ToPoint();
                            CardinalPoint otherEnd = points.a == signalFlow.origin ? points.b : points.a;
                            Vector2Int otherCoordinate = signalFlow.coordinate + signalFlow.origin.ToVector2Int();
                            Debug.Log($"Checking {otherEnd} connection of {signalFlow.coordinate} leading to {otherCoordinate}");
                            yield return MakeCheck(otherEnd, otherCoordinate);
                        }
                    }
                }
            }
            if (tilesBySignalStrength.Count > strength + 1)
            {
                yield return TestSignal(strength + 1);
            }

            System.Collections.IEnumerator MakeCheck(CardinalPoint otherEnd, Vector2Int otherCoordinate)
            {
                // Check if there is a tile at the other end of the connection.
                if (InsideCheck(otherCoordinate))
                {
                    Debug.Log($"Checking {otherCoordinate} from {otherEnd}");
                    int currentStrength = signalMap[otherCoordinate.x, otherCoordinate.y];
                    if (currentStrength < 0 || currentStrength > strength + 1)
                    {
                        PuzzleTile otherTile = GetTile(otherCoordinate);
                        //CardinalPoint otherDirection = otherEnd.Invert();
                        if (otherTile.HasEntry(otherEnd.Invert()))
                        {
                            signalMap[otherCoordinate.x, otherCoordinate.y] = strength + 1;
                            if (tilesBySignalStrength.Count == strength + 1)
                            {
                                tilesBySignalStrength.Add(new HashSet<SignalStrength>());
                            }
                            tilesBySignalStrength[strength + 1].Add(new SignalStrength(otherCoordinate, otherEnd));
                            Debug.Log($"Adding {otherCoordinate}");
                        }
                    }
                }
                yield return new WaitForSeconds(0.3f);
            }
            Debug.Log("Done");
        }

        private void DrawHeatmap()
        {
            Gradient gradient = new Gradient();
            GradientColorKey[] keys = new GradientColorKey[2];
            keys[0] = new GradientColorKey(new Color(1f, 0f, 0, 0.5f), 0f);
            keys[1] = new GradientColorKey(new Color(0f, 1f, 0, 0.5f), 1f);
            gradient.colorKeys = keys;
            if (signalMap != null)
            {
                Vector3 tileSize = new Vector3(size.x / Width, size.y / Height);
                float heatStep = 1f / tilesBySignalStrength.Count;
                for (int y = 0; y < signalMap.GetLength(1); y++)
                {
                    for (int x = 0; x < signalMap.GetLength(0); x++)
                    {
                        if (signalMap[x, y] >= 0)
                        {
                            Gizmos.color = gradient.Evaluate(heatStep * (float)signalMap[x, y]);
                            Gizmos.DrawCube(transform.position + new Vector3(x * tileSize.x, y * tileSize.y) + tileSize * 0.5f, tileSize);
                        }
                    }
                }
            }
        }

        private struct SignalStrength
        {
            public Vector2Int coordinate;
            public CardinalPoint origin;

            public SignalStrength(Vector2Int coordinate, CardinalPoint origin)
            {
                this.coordinate = coordinate;
                this.origin = origin;
            }
        }

        */
    }
}
