namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using ShiftingMaze.Signals;
    using System;
    using UnityEngine;
    using UnityEngine.Events;

    public class PuzzleConnectionCheck : MonoBehaviour
    {
        [SerializeField]
        private StartStructure port = default;

        [SerializeField]
        private PuzzleMatrix manager = default;

        [SerializeField]
        private Signal signalIn = default;

        [SerializeField]
        private UnityEvent<Signal> onSignalChange = default;


        private PuzzleTile tile;

        private void OnEnable()
        {
            manager.CallAfterSetup(SetPort);
            manager.OnMoved += CheckForConnection;
        }

        private void OnDisable()
        {
            manager.OnMoved -= CheckForConnection;
        }

        private void SetPort(PuzzleMatrix matrix)
        {
            Vector2Int cords;
            (tile, cords) = manager.FindPort(port);
        }

        private void CheckForConnection(Vector2Int from, PuzzleTile tile, Vector2Int to)
        {

        }
    }
}
