using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class AdaptiveMusicManager : MonoBehaviour
{
    public static AdaptiveMusicManager Instance;

    public float loopLength;
    private float loopTimer = 0;
    public UnityEvent OnLoopstart;
    public CustomAudioSource[] tracks;
    private music[] trackNames;
    private int[] trackLoopCount;

    private void Awake()
    {
        Instance = this;
        trackNames = new music[tracks.Length];
        trackLoopCount = Enumerable.Repeat(-1, tracks.Length).ToArray(); 
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        loopTimer += Time.deltaTime;

        if(loopTimer >= loopLength)
        {
            OnLoopstart.Invoke();
            OnLoopstart.RemoveAllListeners();
            CheckLoopCounts();
            loopTimer = 0;
        }
    }

    public void Play(music mus)
    {
        int i = EmptyTrack();
        if (i != -1 && !HasTrack(mus))
        {
            trackNames[i] = mus;
            AudioClip c = AudioManager.Instance.clips.music[(int)(mus)].clip;

            PlayMusicClip(c, tracks[i], true);
        }
    }

    public void Play(music mus, int loopCount)
    {
        int i = EmptyTrack();
        if (i != -1)
        {
            trackLoopCount[i] = loopCount;
            Debug.Log(mus + " loops " + loopCount + " times");
            trackNames[i] = mus;
            AudioClip c = AudioManager.Instance.clips.music[(int)(mus)].clip;

            PlayMusicClip(c, tracks[i], false);
        }
        else
        {
            Debug.LogError("All tracks are currently playing something");
        }


    }

    public void Stop(music loop)
    {
        int i = 0;
        foreach(music m in trackNames)
        {
            if(loop == m)
            {

                tracks[i].PlayFadeOut(1f);
                trackNames[i] = music.empty;
            }
            i++;
        }
    }

    private bool HasTrack(music mus)
    {
        foreach (music m in trackNames)
        {
            if (m == mus)
            {
                return true;
            }

        }
        return false;
    }

    private int EmptyTrack()
    {
        int i = 0;
        foreach(music m in trackNames)
        {
            if(m == music.empty)
            {
                return i;
            }

            if (!tracks[i].IsPlaying())
            {
                Stop(m);
                return i;
            }

            i++;
        }
        return -1;
    }

    private void CheckLoopCounts()
    {
        for(int i = 0; i < tracks.Length; i++)
        {
            if(trackLoopCount[i] > 0)
            {
                trackLoopCount[i] -= 1;
            } 
            else if (trackLoopCount[i] == 0)
            {
                Debug.Log("Stopped Loop" + trackNames[i]);
                trackLoopCount[i] = -1;
                Stop(trackNames[i]);
            }
        }
    }

    private void PlayMusicClip(AudioClip music, CustomAudioSource musicSource, bool loop = true)
    {
        musicSource.SetClip(music);
        musicSource.SetStartTime(loopTimer);
        musicSource.loop = loop;
        musicSource.PlayFadeIn(1);
    }

    public void PlaySource(AudioSource source)
    {
        source.time = loopTimer;
        source.Play();
    }

    public void PlaySource(CustomAudioSource source)
    {
        source.SetStartTime(loopTimer);
        source.Play();
    }

}
