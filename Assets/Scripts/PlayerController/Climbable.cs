namespace ShiftingMaze.Player
{
    using System.Collections.Generic;
    using UnityEngine;

    public class Climbable : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The ladders collider.")]
        private new BoxCollider collider = default;

        [SerializeField, Min(0f), Tooltip("The offset of the first step on the ladder.")]
        private float bottomStep = default;
        [SerializeField, Min(0f), Tooltip("The offset of the last step on the ladder.")]
        private float topStep = default;
        [SerializeField, Min(2), Tooltip("The amount of steps on the ladder.")]
        private int stepCount = default;

        [SerializeField, Tooltip("The position to put the climber after dismounting at the top.")]
        private Vector3 topUnmount = default;
        [SerializeField, Tooltip("The position to put the climber after dismounting at the bottom.")]
        private Vector3 bottomUnmount = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Vector3 localBottom, localTop;
        private float stepSize, localStepSize;
        private Vector3 localTopStep, localBottomStep;
        private Vector3 GlobalBottomStep => TransformPosition(localBottomStep);


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            if (collider)
            {
                Gizmos.color = Color.black;

                Vector3 topCorner = collider.transform.TransformPoint(collider.center + collider.size * 0.5f);
                Vector3 bottomCorner = topCorner + collider.transform.TransformVector(Vector3.down * collider.size.y);

                float height = collider.size.y * collider.transform.localScale.y;
                float distance = height - topStep - bottomStep;
                float stepSize = distance / (stepCount - 1);

                for (int i = 0; i < stepCount; i++)
                {
                    Gizmos.DrawRay(bottomCorner + collider.transform.up * (bottomStep + i * stepSize), collider.transform.TransformVector(Vector3.left * collider.size.x));
                }

                DrawUnmount(bottomCorner + collider.transform.TransformVector(Vector3.left * collider.size.x * 0.5f + bottomUnmount));
                DrawUnmount(topCorner + collider.transform.TransformVector(Vector3.left * collider.size.x * 0.5f + topUnmount));

            }
        }

        private void DrawUnmount(Vector3 position)
        {
            float colliderWidth = collider.size.x * transform.localScale.x;
            Vector3 forward = position - transform.forward * colliderWidth * 0.4f;
            Vector3 right = position + transform.forward * colliderWidth * 0.3f + transform.right * colliderWidth * 0.5f;
            Vector3 left = position + transform.forward * colliderWidth * 0.3f + -transform.right * colliderWidth * 0.5f;

            Gizmos.DrawLine(position, forward);
            Gizmos.DrawLine(position, right);
            Gizmos.DrawLine(position, left);

            Gizmos.DrawLine(forward, left);
            Gizmos.DrawLine(forward, right);
            Gizmos.DrawLine(left, right);
        }

#endif

        private void OnEnable()
        {
            localBottom = collider.center + Vector3.down * collider.size.y * 0.5f + Vector3.forward * collider.size.z * 0.5f;
            localTop = collider.center + Vector3.up * collider.size.y * 0.5f + Vector3.forward * collider.size.z * 0.5f;
            localBottomStep = localBottom + transform.InverseTransformVector(Vector3.up * bottomStep);
            localTopStep = localTop + transform.InverseTransformVector(Vector3.down * topStep);

            stepSize = (transform.TransformVector(collider.size).y - topStep - bottomStep) / (stepCount - 1);
            localStepSize = Vector3.Distance(localTopStep, localBottomStep) / stepCount;
        }

        private void OnPlayerHit(PlayerController player)
        {
            ControllerBehaviour.GetController<ClimbingController>(player)?.Mount(this);
        }

        private (Vector3 position, Vector3 normal) GetStep(int index)
        {
            index = Mathf.Clamp(index, 0, stepCount - 1);
            return (GlobalBottomStep + transform.up * stepSize * index, transform.forward);
        }

        private int GetClosestStep(Vector3 position)
        {
            position = transform.InverseTransformPoint(position);
            if (position.y > localTopStep.y - localStepSize * 0.5f)
            {
                return stepCount;
            }
            else if (position.y <= localBottomStep.y + localStepSize * 0.5f)
            {
                return 0;
            }
            else
            {
                return Mathf.RoundToInt((position.y - localBottomStep.y) / localStepSize);
            }
        }

        public Agent GetAgent(Vector3 position, System.Action<(Vector3 position, Vector3 normal)> mount)
        {
            return new Agent(this, position, mount);
        }

        private Vector3 TransformPosition(Vector3 position)
        {
            return transform.position + transform.TransformVector(position);
        }

        public class Agent
        {
            // Variables --------------------------------------------------------------------------
            private Climbable climbable;
            private int currentStep;

            // Constructor ------------------------------------------------------------------------

            /// <summary>
            /// Creates a new agent to navigate through the climbale.
            /// </summary>
            /// <param name="climbable">The climbable to navigate through.</param>
            /// <param name="position">The position to start navigating from.</param>
            /// <param name="mount">Called with the first positions on the climbable.</param>
            public Agent(Climbable climbable, Vector3 position, System.Action<(Vector3 position, Vector3 normal)> mount)
            {
                this.climbable = climbable;
                currentStep = climbable.GetClosestStep(position);
                mount?.Invoke(climbable.GetStep(currentStep));
            }

            // Methods ----------------------------------------------------------------------------

            /// <summary>
            /// Moves the agent up the climbable.
            /// </summary>
            /// <param name="move">Called with the next position when the agent climbed.</param>
            /// <param name="unmount">Called with the step off position if the agent was at the top.</param>
            public void MoveUp(System.Action<(Vector3 position, Vector3 normal)> move, System.Action<(Vector3 position, Vector3 normal)> unmount)
            {
                if (currentStep < climbable.stepCount)
                {
                    currentStep++;
                    move?.Invoke(climbable.GetStep(currentStep));
                }
                else
                {
                    unmount?.Invoke((climbable.TransformPosition(climbable.localTop + climbable.topUnmount), climbable.transform.forward));
                }
            }

            /// <summary>
            /// Moves the agent down the climbable.
            /// </summary>
            /// <param name="move">Called woth the next position when the agent climbed.</param>
            /// <param name="unmount">Called with the step off position if the agent was at the bottom.</param>
            public void MoveDown(System.Action<(Vector3 position, Vector3 normal)> move, System.Action<(Vector3 position, Vector3 normal)> unmount)
            {
                if (currentStep >= 1)
                {
                    currentStep--;
                    move?.Invoke(climbable.GetStep(currentStep));
                }
                else
                {
                    unmount?.Invoke((climbable.TransformPosition(climbable.localBottom + climbable.bottomUnmount), climbable.transform.forward));
                }
            }
        }
    }
}
