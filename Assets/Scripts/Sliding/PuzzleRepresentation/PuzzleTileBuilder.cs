namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using UnityEngine;

    public abstract class PuzzleTileBuilder : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The size of a tile.
        /// </summary>
        public abstract Vector2 TileSize { get; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a tile for the given structure.
        /// </summary>
        /// <param name="structure">The structure to build the tile for.</param>
        /// <param name="isBorder">True if the tile is a border tile.</param>
        /// <returns>The tile for the structure.</returns>
        public abstract PuzzleTile BuildTile(TileStructure structure, CardinalPoints borders);
    }
}
