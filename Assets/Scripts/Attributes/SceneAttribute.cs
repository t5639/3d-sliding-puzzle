﻿using UnityEngine;

/// <summary>
/// Used to draw a scene property into the inscector.
/// </summary>
public class SceneAttribute : PropertyAttribute { }
