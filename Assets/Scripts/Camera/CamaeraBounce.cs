namespace ShiftingMaze.Camera
{
    using System.Collections;
    using UnityEngine;

    public class CamaeraBounce : MonoBehaviour
    {
        [SerializeField, Min(0f), Tooltip("The settle speed.")]
        private AnimationCurve motion = new AnimationCurve(new Keyframe(), new Keyframe(0.15f, -0.25f), new Keyframe(0.3f, 0f));
        private Vector3 originalPosition;
        private Coroutine bounceLoop;

        private void Awake()
        {
            originalPosition = transform.localPosition;
        }

        public void Bounce()
        {
            if (bounceLoop == null)
            {
                bounceLoop = StartCoroutine(DoBounce());
            }
        }

        private IEnumerator DoBounce()
        {
            float time = 0f;
            float maxTime = motion.keys[motion.length - 1].time;
            while (time < maxTime)
            {
                time += Time.deltaTime;
                transform.localPosition = originalPosition + Vector3.up * motion.Evaluate(time);
                yield return null;
            }
            transform.localPosition = originalPosition;
            bounceLoop = null;
        }
    }
}
