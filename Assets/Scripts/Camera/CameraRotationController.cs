﻿namespace bugiflo
{
    using ShiftingMaze;
    using UnityEngine;
    using UnityEngine.InputSystem;

    public class CameraRotationController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The object to rotate on the horitontal axis.")]
        private Transform horizontalTarget = null;
        [SerializeField, Tooltip("The object to rotate on the vertical axis.")]
        private Transform verticalTarget = null;

        [SerializeField, Min(0f), Tooltip("The speed of the rotation.")]
        public float rotationSpeed = 10f;

        [SerializeField, Range(0f, 180f)]
        private float verticalRoationAngle = 180f;

        [SerializeField, Tooltip("Manages if the camera can be controlled.")]
        private Enableable active = default;
        /// <summary>
        /// Manages if the camera can be controlled.
        /// </summary>
        public IEnableable Active => active;

        [SerializeField, Tooltip("The events that are called when the vertical rotation lock state changes.")]
        private Enableable verticalRotation = default;
        /// <summary>
        /// Manages the locked state of the vertical roation.
        /// </summary>
        public IEnableable VerticalRotation => verticalRotation;

        private float lockedVerticalAngle = 0f;

        private PlayerSystem inputs;
        private PlayerSystem.GroundMovementActions ground;
        private Vector2 velocity;


        private void Awake()
        {
            //verticalRotation.OnDisabled += () => { lockedVerticalAngle = verticalTarget.localEulerAngles.y; };
        }

        private void OnEnable()
        {
            inputs = new PlayerSystem();
            ground = inputs.GroundMovement;
            ground.CameraMove.performed += HandleMouse;
            ground.CameraMove.canceled += HandleMouseOff;
            inputs.Enable();
        }

        private void OnDisable()
        {
            inputs.Disable();
            ground.CameraMove.canceled -= HandleMouseOff;
            ground.CameraMove.performed -= HandleMouse;
        }

        private void Update()
        {
            if (active.IsEnabled)
            {
                float halfAngle = verticalRoationAngle * 0.5f;
                // Get the current horizontal agnle.
                float horizontalAngle = horizontalTarget.localEulerAngles.x;
                // Add input rotation to the angle.
                horizontalAngle -= velocity.y * rotationSpeed * Time.fixedDeltaTime;
                // Clamp the angle.
                horizontalAngle = (horizontalAngle + 360f) % 360f;
                if (horizontalAngle > halfAngle && horizontalAngle < 180f)
                {
                    horizontalAngle = halfAngle;
                }
                if (horizontalAngle < (360f - halfAngle) && horizontalAngle > 180f)
                {
                    horizontalAngle = -halfAngle;
                }
                Vector3 angle = horizontalTarget.localEulerAngles;
                angle.x = horizontalAngle;
                horizontalTarget.localEulerAngles = angle;

                if (verticalRotation.IsEnabled)
                {
                    angle = verticalTarget.localEulerAngles;
                    angle.y += velocity.x * rotationSpeed * Time.fixedDeltaTime;
                    verticalTarget.localEulerAngles = angle;
                }
                /*
                else if (verticalTarget.localEulerAngles.y != lockedVerticalAngle)
                {
                    angle = verticalTarget.localEulerAngles;
                    angle.y = lockedVerticalAngle;
                    verticalTarget.localEulerAngles = angle;
                }
                */
            }
        }

        private void HandleMouse(InputAction.CallbackContext obj)
        {
            velocity = obj.ReadValue<Vector2>();
        }
        private void HandleMouseOff(InputAction.CallbackContext obj) => velocity = Vector2.zero;

        public void Activate(Object component) => active.Enable(component);
        public void Deactivate(Object component) => active.Disable(component);

        public void LockVerticalRotation(Object component) => verticalRotation.Disable(component);
        public void UnlockVerticalRotation(Object component) => verticalRotation.Enable(component);
    }
}
