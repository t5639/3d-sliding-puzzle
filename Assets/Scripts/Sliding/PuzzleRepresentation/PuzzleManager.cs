namespace ShiftingMaze.Puzzle.Management
{
    using UnityEngine;

    /// <summary>
    /// The matrix for the level.
    /// </summary>
    public class PuzzleManager : PuzzleMatrix
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The tiles in the puzzle.")]
        protected PuzzleTile[] tiles = default;
        /// <inheritdoc/>
        protected override PuzzleTile[] Tiles
        {
            get => tiles;
            set => tiles = value;
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            FinishSetup();
        }
    }
}