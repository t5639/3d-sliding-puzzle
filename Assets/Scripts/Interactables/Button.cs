using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : InteractableBase
{
    bool pressed = false;
    bool interactable = true;

    public MovingPlatform_anim movingPlat;

    public override void OnInteract()
    {
        if (interactable)
        {
            interactable = false;
            pressed = true;

            interactText = "Press E to activate the Button!";

            transform.GetComponent<Animator>().SetBool("pushButton", pressed);

            Action();
        }
    }

    public void ResetButton()
    {
        interactable = true;
        transform.GetComponent<Animator>().SetBool("pushButton", !pressed);

    }

    void Action()
    {
        if (movingPlat != null)
        {
            movingPlat.Animate(true);
        }
    }
}
