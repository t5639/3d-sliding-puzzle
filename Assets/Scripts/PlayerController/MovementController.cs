namespace ShiftingMaze.Player
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using static UnityEngine.InputSystem.InputAction;

    [RequireComponent(typeof(CharacterController))]
    public class MovementController : ControllerBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------
        [Header("Speed")] // ----------------------------------------------------------------------
        [SerializeField, Min(0f), Tooltip("The characters move speed.")]
        private float runSpeed = default;

        [SerializeField, Min(0f), Tooltip("The amcimum movement speed while sprinting.")]
        private float sprintSpeed = default;

        [SerializeField, Min(0f), Tooltip("The speed gain per second while grounded.")]
        private float groundAccelecation = default;

        [SerializeField, Min(0f), Tooltip("The speed gain per second during air time.")]
        private float airAcceleration = default;

        [SerializeField, Min(0f), Tooltip("The boost the character gets on the sprint start.")]
        private float sprintBoost = default;

        [Header("Jump")] // -----------------------------------------------------------------------
        [SerializeField, Min(0f), Tooltip("The intensity of the jump.")]
        private float jumpStrength = 20f;

        [SerializeField, Min(0f), Tooltip("The time a jump can be performed after the input when the ground is hit.")]
        private float jumpGroundDelay = 0.1f;

        [SerializeField, Min(0f), Tooltip("The time a jump can be performed after leaving the ground.")]
        private float jumpAirDelay = 0.1f;

        [Header("Components")] // -----------------------------------------------------------------
        [SerializeField, Tooltip("The script handling the players physics.")]
        private CharacterController body = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Vector2 targetDirection = Vector3.zero;
        private Vector3 velocity = Vector3.zero;
        private Vector3 gravity = Vector3.zero;
        private Vector3 TargetVelocity => (transform.right * targetDirection.x + transform.forward * targetDirection.y) * Speed;
        private float jumpPress = -1f;
        private float fallStart = -1f;
        private float Acceleration => body.isGrounded ? groundAccelecation : airAcceleration;
        private bool isSprinting = false;
        private float Speed => isSprinting ? sprintSpeed : runSpeed;

        private Vector3 move;
        private Vector3 groundNormal;
        /// <summary>
        /// True if the controller is touching the ground.
        /// </summary>
        public bool IsGrounded { get; private set; }

        private PlayerSystem.GroundMovementActions Inputs => controller.Inputs.GroundMovement;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected override void OnEnable()
        {
            base.OnEnable();

            Inputs.HorizontalMovement.performed += Move;
            Inputs.Jump.performed += Jump;
            Inputs.Sprint.performed += StartSprint;
            Inputs.Sprint.canceled += StopSprint;

            Move(Inputs.HorizontalMovement.ReadValue<Vector2>());
        }

        protected override void OnDisable()
        {
            Inputs.Sprint.canceled -= StopSprint;
            Inputs.Sprint.performed -= StartSprint;
            Inputs.Jump.performed -= Jump;
            Inputs.HorizontalMovement.performed -= Move;

            velocity = Vector3.zero;
            gravity = Vector3.zero;
            targetDirection = Vector3.zero;

            base.OnDisable();
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            if (hit.normal.y > 0f)
            {
                if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit groundHit, body.stepOffset))
                {
                    groundNormal = groundHit.normal;
                }
                else
                {
                    groundNormal = hit.normal;
                }
            }
        }

        private void FixedUpdate()
        {
            // Update gravity.
            gravity += Physics.gravity * Time.fixedDeltaTime;
            if (IsGrounded && Time.time - jumpPress <= jumpGroundDelay)
            {
                Jump();
            }

            velocity = Vector3.MoveTowards(velocity, TargetVelocity, Acceleration * Time.fixedDeltaTime);

            groundNormal = Vector3.zero;
            move = velocity + gravity;
            body.Move((move * Time.fixedDeltaTime).SelectY());
            IsGrounded = groundNormal.sqrMagnitude == 0f ? false : Vector3.Angle(Vector3.up, groundNormal) <= body.slopeLimit;
            if (!IsGrounded)
            {
                Vector3 perpendicular = move - groundNormal * Vector3.Dot(move, groundNormal);
                velocity = perpendicular.IgnoreY();
            }
            body.Move((move * Time.fixedDeltaTime).IgnoreY());
            if (!enabled)
            {
                return;
            }
            if (IsGrounded)
            {
                if (fallStart >= 0f)
                {
                    fallStart = -1f;
                }
                if (gravity.y < 0f)
                {
                    gravity = Vector3.zero;
                }
            }
            else if (fallStart < 0f)
            {
                fallStart = Time.time;
            }
        }

        // Move -----------------------------------------------------------------------------------

        /// <summary>
        /// Moves the character in the direction. 
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        public void Move(Vector2 direction)
        {
            targetDirection = Vector2.ClampMagnitude(direction, 1f);
        }
        /// <summary>
        /// Moves the character in the direction. 
        /// </summary>
        /// <param name="context">The context of the input.</param>
        private void Move(CallbackContext context) => Move(context.ReadValue<Vector2>());

        // Jump -----------------------------------------------------------------------------------

        /// <summary>
        /// Applys an upward force.
        /// </summary>
        public void Jump()
        {
            if (enabled)
            {
                if (IsGrounded || gravity.y <= 0f && Time.time - fallStart <= jumpAirDelay)
                {
                    gravity += Vector3.up * jumpStrength;
                    jumpPress = -1f;
                    fallStart = 0f;
                }
                else
                {
                    jumpPress = Time.time;
                }
            }
        }
        /// <summary>
        /// Lets the controller jump.
        /// </summary>
        /// <param name="context">The context of the input.</param>
        private void Jump(CallbackContext context) => Jump();

        // Sprint ---------------------------------------------------------------------------------

        /// <summary>
        /// Changes the characters sprint state.
        /// </summary>
        /// <param name="isSprinting">True if the character should be sprinting.</param>
        public void Sprint(bool isSprinting)
        {
            if (enabled)
            {
                this.isSprinting = isSprinting;
                if (isSprinting)
                {
                    velocity += new Vector3(targetDirection.x, 0f, targetDirection.y) * sprintBoost;
                }
            }
        }

        /// <summary>
        /// Enables the character sprint.
        /// </summary>
        public void StartSprint() => Sprint(true);
        /// <summary>
        /// Enables the character sprint.
        /// </summary>
        /// <param name="context">The context of the input.</param>
        private void StartSprint(CallbackContext context) => StartSprint();

        /// <summary>
        /// Disables the character sprint.
        /// </summary>
        public void StopSprint() => Sprint(false);
        /// <summary>
        /// Disables the character sprint.
        /// </summary>
        /// <param name="context">The context of the input.</param>
        private void StopSprint(CallbackContext context) => StopSprint();
    }
}
