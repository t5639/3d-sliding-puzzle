namespace ShiftingMaze.Player
{
    using UnityEngine;
    using UnityEngine.Events;

    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("States")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("The enabled state of the character.")]
        private Enableable isEnabled = new Enableable(true);
        /// <summary>
        /// The enabled state of the character.
        /// </summary>
        public IEnableable IsEnabled => isEnabled;

        [SerializeField, Tooltip("The controller to default to.")]
        private ControllerBehaviour defaultController = default;

        [Space, SerializeField, Tooltip("Called when the controller was switched.")]
        protected UnityEvent<ControllerBehaviour, ControllerBehaviour> onControllerSwitched = default;
        /// <summary>
        /// Called when the controller was switched.
        /// </summary>
        public event UnityAction<ControllerBehaviour, ControllerBehaviour> OnControllerSwitched
        {
            add => onControllerSwitched.AddListener(value);
            remove => onControllerSwitched.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------
        
        private ControllerBehaviour activeController;

        /// <summary>
        /// The inputs managed by the controller.
        /// </summary>
        public PlayerSystem Inputs { get; private set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            Inputs = new PlayerSystem();
            Cursor.lockState = CursorLockMode.Locked;
            ResetController();
            isEnabled.OnEnabled += EnableActiveController;
            isEnabled.OnDisabled += DisableActiveController;
            Inputs.Enable();
        }

        private void OnDisable()
        {
            Inputs.Disable();
            isEnabled.OnDisabled -= DisableActiveController;
            isEnabled.OnEnabled -= EnableActiveController;
            SetControllerActive(null);
            Cursor.lockState = CursorLockMode.None;
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            hit.gameObject.SendMessage("OnPlayerHit", this, SendMessageOptions.DontRequireReceiver);
        }

        /// <summary>
        /// Sets an controller active.
        /// </summary>
        /// <param name="controller">The controllet to activate.</param>
        public void SetControllerActive(ControllerBehaviour controller)
        {
            if (activeController)
            {
                activeController.enabled = false;
            }
            ControllerBehaviour oldController = activeController;
            activeController = controller;
            if (activeController)
            {
                activeController.enabled = isEnabled.IsEnabled;
            }
            onControllerSwitched?.Invoke(oldController, activeController);
        }

        /// <summary>
        /// Reverts to the default controller.
        /// </summary>
        public void ResetController() => SetControllerActive(defaultController);

        /// <summary>
        /// Enables the currently selected controller.
        /// </summary>
        private void EnableActiveController()
        {
            if (activeController)
            {
                activeController.enabled = true;
            }
        }

        /// <summary>
        /// Disables the currently selected controller.
        /// </summary>
        private void DisableActiveController()
        {
            if (activeController)
            {
                activeController.enabled = false;
            }
        }
    }
}
