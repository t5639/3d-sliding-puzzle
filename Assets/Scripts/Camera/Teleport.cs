using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teleport : MonoBehaviour
{
    public Transform World_Max, World_Min, Table_Max, Table_Min;
    public CameraTransition player_cam, table_cam;
    public RawImage cam2;
    public Transform playerCamPosition;
    public Camera fpsCam;

    public float fadeTime = 2f, fadeOffset  = 3f;
    private float offsetTimer, fadeTimer;
    private bool fade = false;
    private bool block = false;
    // Start is called before the first frame update
    void Start()
    {
      
    }


    private void Update()
    {

        if (Time.realtimeSinceStartup > 4f && !block)
        {
            block = true;
            StartTeleport();

        }

        if (fade)
        {
            offsetTimer -= Time.deltaTime;

            if (offsetTimer <= 0)
            {
                fadeTimer -= Time.deltaTime;
                cam2.color = new Color(cam2.color.r, cam2.color.g, cam2.color.b, map(fadeTimer, 0, fadeTime, 1, 0));

                if (fadeTimer <= 0)
                {
                    fade = false;
                }

            }
        }
    }

    private void OnTeleportEnd()
    {
        player_cam.transform.position = table_cam.transform.position;
        cam2.color = Color.clear;
        player_cam.OnTransitionEnd.RemoveListener(OnTeleportEnd);
    }


    public void StartTeleport()
    {
        player_cam.GetComponent<Camera>().enabled = true;
        fpsCam.enabled = false;

        player_cam.OnTransitionEnd.AddListener(OnTeleportEnd);
        player_cam.transform.position = playerCamPosition.position;
        player_cam.transform.rotation = playerCamPosition.rotation;
        
        

        table_cam.transform.position = map(player_cam.transform.position, World_Min.position, World_Max.position, Table_Min.position, Table_Max.position);
        table_cam.transform.position = new Vector3(table_cam.transform.position.x, Table_Min.position.y, table_cam.transform.position.z);
        table_cam.transform.rotation = player_cam.transform.rotation;

        table_cam.PlayerTransition(table_cam.target, table_cam.bigTarget);
        player_cam.PlayerTransition(player_cam.target, player_cam.bigTarget);

        offsetTimer = fadeOffset;
        fadeTimer = fadeTime;
        fade = true;
    }

    private Vector3 map(Vector3 s, Vector3 a1, Vector3 a2, Vector3 b1, Vector3 b2)
    {
        Vector3 vec = new Vector3();
        vec.x = map(s.x, a1.x, a2.x, b1.x, b2.x);
        vec.y = map(s.y, a1.y, a2.y, b1.y, b2.y);
        vec.z = map(s.z, a1.z, a2.z, b1.z, b2.z);

        return vec;
    }

private float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
}
