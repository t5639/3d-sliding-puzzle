namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Provides methods to move the tiles of a puzzle.
    /// </summary>
    public class PuzzleController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script managing the matrix of the puzzle.")]
        private PuzzleMatrix manager = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Vector2Int controlledField = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            // Register a listener in order to update the controlled field.
            manager.OnMoved += HandleMove;
            manager.OnSynchronized += HandeSynchronization;
            // Get the field to control.
            manager.CallAfterSetup(m => SetControlledField(m.GetPit()));
        }

        private void OnDisable()
        {
            // Unregister the listner.
            manager.OnSynchronized -= HandeSynchronization;
            manager.OnMoved -= HandleMove;
        }

        /// <summary>
        /// Moves the target tile in the given direction.
        /// </summary>
        /// <param name="direction">the direction to move.</param>
        public void Move(CardinalPoint direction)
        {
            manager.Move(controlledField, direction);
        }
        /// <summary>
        /// Moves the target tile in the given direction.
        /// </summary>
        /// <param name="direction">the direction to move.</param>
        public void Move(Vector2 direction)
        {
            if (direction.x * direction.x > direction.y * direction.y)
            {
                if (direction.x >= 0f)
                {
                    Move(CardinalPoint.East);
                }
                else
                {
                    Move(CardinalPoint.West);
                }
            }
            else
            {
                if (direction.y >= 0f)
                {
                    Move(CardinalPoint.North);
                }
                else
                {
                    Move(CardinalPoint.South);
                }
            }
        }

        /// <summary>
        /// Sets the controlled field.
        /// </summary>
        /// <param name="tileCoordinatePair">The tile and coordinates beeing controlled.</param>
        private void SetControlledField((PuzzleTile tile, Vector2Int coordinates) tileCoordinatePair) => controlledField = tileCoordinatePair.coordinates;

        /// <summary>
        /// Updates the coordinates of the controlled tile.
        /// </summary>
        /// <param name="from">The position the tile move away from.</param>
        /// <param name="tile">The tile that moved.</param>
        /// <param name="to">The position the tile moved to.</param>
        private void HandleMove(Vector2Int from, PuzzleTile tile, Vector2Int to)
        {
            // Check if the moved tile is the controlled pit.
            if (tile.Structure is PitStructure && from == controlledField)
            {
                // Update the coordinates.
                controlledField = to;
            }
        }

        /// <summary>
        /// Handles the synchronization of the matrix.
        /// </summary>
        /// <param name="manager">The new layout.</param>
        private void HandeSynchronization(IEnumerable<(PuzzleTile tile, Vector2Int coordinates)> moves) 
            => SetControlledField(manager.GetPit());

#if UNITY_EDITOR

        [ContextMenu("Move/North")]
        private void MoveNorth() => Move(CardinalPoint.North);
        [ContextMenu("Move/East")]
        private void MoveEast() => Move(CardinalPoint.East);
        [ContextMenu("Move/South")]
        private void MoveSout() => Move(CardinalPoint.South);
        [ContextMenu("Move/West")]
        private void MoveWest() => Move(CardinalPoint.West);

#endif
    }
}
