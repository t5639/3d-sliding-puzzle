using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Lever : InteractableInvBase
{
    bool pressed = false;
    public bool placed = false;

    bool open;
    public Animator animator;

    public UnityEvent OnPickup, OnPull, OnPush;

    public override void OnInteract()
    {
        if (!placed)
        {
            if (Inventory.instance.AddItem(this))
            {
                OnPickup.Invoke();
                this.gameObject.SetActive(false);
            }
        }
        else
        {
            pressed = !pressed;

            interactText = "Press E to ..!";

            //interactText += pressed ? "to close" : "to open"; //or public strings for individual texts per lever

            transform.GetComponentInParent<Animator>().SetBool("useLever", pressed);

            Action();
        }
    }

    void Action()
    {
        if (!open)
        {
            OnPush.Invoke();
            animator.SetBool("Open", true);
            open = true;
        }
        else
        {
            OnPull.Invoke();
            animator.SetBool("Open", false);
            open = false;
        }
    }

    /*public override void Equip()
    {
        this.gameObject.SetActive(true);
        Debug.Log("equipped: " + idCode);
    }*/

    /*public override void Unequip()
    {
        this.gameObject.SetActive(false);
    }*/


}
