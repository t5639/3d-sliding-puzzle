namespace ShiftingMaze.Signals
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class SignalSet<T, TNone, TWater, TMagic, TOil, TFire> : ScriptableObject, ISignalCollection<T, TNone, TWater, TMagic, TOil, TFire>
        where TNone : T where TWater : T where TMagic : T where TOil : T where TFire : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value when no signal is active.")]
        protected TNone none = default;
        /// <inheritdoc/>
        public TNone None => none;

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TWater water = default;
        /// <inheritdoc/>
        public TWater Water => water;

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TMagic magic = default;
        /// <inheritdoc/>
        public TMagic Magic => magic;

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TOil oil = default;
        /// <inheritdoc/>
        public TOil Oilt => oil;

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TFire fire = default;
        /// <inheritdoc/>
        public TFire Fire => fire;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public T GetValue(Signal signal)
        {
            switch (signal)
            {
                default: return default;
                case Signal.None: return none;
                case Signal.Water: return water;
                case Signal.Magic: return magic;
                case Signal.Oil: return oil;
                case Signal.Fire: return fire;
            }
        }

        public IEnumerator<KeyValuePair<Signal, T>> GetEnumerator()
        {
            yield return new KeyValuePair<Signal, T>(Signal.None, none);
            yield return new KeyValuePair<Signal, T>(Signal.Water, water);
            yield return new KeyValuePair<Signal, T>(Signal.Magic, magic);
            yield return new KeyValuePair<Signal, T>(Signal.Oil, oil);
            yield return new KeyValuePair<Signal, T>(Signal.Fire, fire);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    public class SignalSet<T> : SignalSet<T, T, T, T, T, T> { }

    [System.Serializable]
    public abstract class SignalCollection<T, TNone, TWater, TMagic, TOil, TFire> : ISignalCollection<T, TNone, TWater, TMagic, TOil, TFire>
        where TNone : T where TWater : T where TMagic : T where TOil : T where TFire : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value when no signal is active.")]
        protected TNone none = default;
        /// <inheritdoc/>
        public TNone None
        {
            get => none;
            set => none = value;
        }

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TWater water = default;
        /// <inheritdoc/>
        public TWater Water
        {
            get => water;
            set => water = value;
        }

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TMagic magic = default;
        /// <inheritdoc/>
        public TMagic Magic
        {
            get => magic;
            set => magic = value;
        }

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TOil oil = default;
        /// <inheritdoc/>
        public TOil Oilt
        {
            get => oil;
            set => oil = value;
        }

        [SerializeField, Tooltip("The value for the water signal.")]
        protected TFire fire = default;
        /// <inheritdoc/>
        public TFire Fire
        {
            get => fire;
            set => fire = value;
        }


        // --- | Constructor | ---------------------------------------------------------------------------------------------

        public SignalCollection(TNone none, TWater water, TMagic magic, TOil oil, TFire fire)
        {
            this.none = none;
            this.water = water;
            this.magic = magic;
            this.oil = oil;
            this.fire = fire;
        }
        public SignalCollection() : this(default, default, default, default, default) { }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public T GetValue(Signal signal)
        {
            switch (signal)
            {
                default: return default;
                case Signal.None: return none;
                case Signal.Water: return water;
                case Signal.Magic: return magic;
                case Signal.Oil: return oil;
                case Signal.Fire: return fire;
            }
        }

        public IEnumerator<KeyValuePair<Signal, T>> GetEnumerator()
        {
            yield return new KeyValuePair<Signal, T>(Signal.None, none);
            yield return new KeyValuePair<Signal, T>(Signal.Water, water);
            yield return new KeyValuePair<Signal, T>(Signal.Magic, magic);
            yield return new KeyValuePair<Signal, T>(Signal.Oil, oil);
            yield return new KeyValuePair<Signal, T>(Signal.Fire, fire);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [System.Serializable]
    public class SignalCollection<T> : SignalCollection<T, T, T, T, T, T>
    {
        // --- | Constructors | --------------------------------------------------------------------------------------------

        public SignalCollection(T none, T water, T magic, T oil, T fire) : base(none, water, magic, oil, fire) { }
        public SignalCollection(T value) : this(value, value, value, value, value) { }
        public SignalCollection() : this(default) { }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the value for a signal.
        /// </summary>
        /// <param name="signal">The signal whos value to change.</param>
        /// <param name="value">The value to apply.</param>
        public void SetValue(Signal signal, T value)
        {
            switch (signal)
            {
                case Signal.None: none = value; break;
                case Signal.Water: water = value; break;
                case Signal.Magic: magic = value; break;
                case Signal.Oil: oil = value; break;
                case Signal.Fire: fire = value; break;
            }
        }
    }

    public interface ISignalCollection<T, TNone, TWater, TMagic, TOil, TFire> : IEnumerable<KeyValuePair<Signal, T>>
        where TNone : T where TWater : T where TMagic : T where TOil : T where TFire : T
    {
        // --- | Properties | ----------------------------------------------------------------------------------------------

        /// <summary>
        /// The value when no signal is active.
        /// </summary>
        TNone None { get; }

        /// <summary>
        /// The value for the water signal.
        /// </summary>
        TWater Water { get; }

        /// <summary>
        /// The value for the water signal.
        /// </summary>
        TMagic Magic { get; }

        /// <summary>
        /// The value for the water signal.
        /// </summary>
        TOil Oilt { get; }

        /// <summary>
        /// The value for the water signal.
        /// </summary>
        TFire Fire { get; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for the signal.
        /// </summary>
        /// <param name="signal">The signal whos value to return.</param>
        /// <returns>the value for the signal.</returns>
        T GetValue(Signal signal);
    }

    public enum Signal { None, Water, Magic, Oil, Fire }
}
