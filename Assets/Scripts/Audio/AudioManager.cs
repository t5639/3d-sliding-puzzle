using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioSource ambienceSource, sfxSource;
    public bool ambienceOnAwake = false;
    public ClipCollection clips;
    public AudioMixerGroup musicMix, ambienceMix, sfxMix;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (ambienceOnAwake)
        {
            PlayAmbience(ambience.dessert_Ambience);
        }
    }

    public void PlayAmbience(ambience amb)
    {
        AudioClip c = clips.ambience[(int)(amb)].clip;
        PlayAmbienceClip(c);
    }
    public void Play(sfx sfx, AudioSource source = null, bool loop = false)
    {
        Debug.Log(source);
        AudioClip c = clips.sfx[(int)(sfx)].clip;
        PlaySFXClip(c, source, loop);
    }
   

    public void PlayAmbienceClip(AudioClip ambience, bool loop = true)
    {
        ambienceSource.clip = ambience;
        ambienceSource.loop = loop;
        ambienceSource.Play();
    }


    public void PlaySFXClip(AudioClip music, AudioSource source = null, bool loop = false)
    {
        if(source == null)
        {
            sfxSource.clip = music;
            sfxSource.loop = loop;
            sfxSource.Play();

        } else
        {
            Debug.Log("play on source");
            source.clip = music;
            source.loop = loop;
            source.Play();

        }
    }
}
