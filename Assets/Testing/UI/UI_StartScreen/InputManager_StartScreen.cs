using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager_StartScreen : MonoBehaviour
{

    [SerializeField] UI_Interaction ui;

    UISystem controls;
    UISystem.StartScreenActions startScreen;


    private void Awake()
    {
        controls = new UISystem();

        startScreen = controls.StartScreen;

        startScreen.Return.performed += _ => ui.SetStartMain();
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
}
