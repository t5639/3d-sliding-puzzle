using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform_anim : MonoBehaviour
{

    public void Animate(bool b)
    {
        transform.GetComponent<Animator>().SetBool("movePlatform", b);
    }

}
