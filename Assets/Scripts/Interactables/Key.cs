using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Key : InteractableInvBase
{
    public int keyId;

    public UnityEvent OnPickUp;

    public override void OnInteract()
    {
        if (Inventory.instance.AddItem(this))
        {
            OnPickUp.Invoke();
            this.gameObject.SetActive(false);
        }
        else
        {
            Debug.Log("not enough space in your inventory");
        }
    }

    /*public override void Equip()
    {
        //hand = image;
        this.gameObject.SetActive(true);
        Debug.Log("equipped: " + idCode);
    }*/






}
