namespace ShiftingMaze.Signals
{
    using UnityEngine;
    using UnityEngine.Events;

    public class SignalReader : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The signal point to listen to.")]
        private SignalPoint point = default;

        [SerializeField, Tooltip("The signal types to listen for.")]
        private Signal[] signals = default;

        [Header("Events")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("Called when the point switches to a registed signal.")]
        private UnityEvent onActive = default;

        [SerializeField, Tooltip("Called when the point switches to a not registed signal.")]
        private UnityEvent onDeactive = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private bool isActive;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            point.GetStartSignal(UpdateInitState);
        }

        private void OnEnable()
        {
            point.OnSignalChange += UpdateState;
        }


        private void OnDisable()
        {
            point.OnSignalChange -= UpdateState;
        }

        /// <summary>
        /// Sets the initial state.
        /// </summary>
        /// <param name="signal">The initial signal type.</param>
        private void UpdateInitState(Signal signal)
        {
            isActive = CheckSignal(signal);
            if (isActive)
            {
                onActive?.Invoke();
            }
            else
            {
                onDeactive?.Invoke();
            }
        }

        /// <summary>
        /// Updates the state.
        /// </summary>
        /// <param name="signal">The current signal.</param>
        private void UpdateState(Signal signal)
        {
            if (isActive != CheckSignal(signal))
            {
                isActive = !isActive;
                if (isActive)
                {
                    onActive?.Invoke();
                }
                else
                {
                    onDeactive?.Invoke();
                }
            }
        }

        /// <summary>
        /// Checks if the signal is part of the registered list.
        /// </summary>
        /// <param name="signal">the signal to check.</param>
        /// <returns>True if the signal is part of the list.</returns>
        private bool CheckSignal(Signal signal)
        {
            foreach (Signal check in signals)
            {
                if (check == signal)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
