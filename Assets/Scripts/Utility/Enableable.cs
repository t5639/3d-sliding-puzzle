namespace ShiftingMaze
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// A class handling the state of an object.
    /// </summary>
    [System.Serializable]
    public sealed class Enableable : IEnableable
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, LockInPlaymode, Tooltip("True if the object is enabled by default.")]
        private bool defaultState = true;

        [Space, SerializeField, Tooltip("Called when the object gets enabled.")]
        private UnityEvent onEnabled = default;
        /// <inheritdoc/>
        public event UnityAction OnEnabled
        {
            add => onEnabled.AddListener(value);
            remove => onEnabled.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called whent the object gets disabled.")]
        private UnityEvent onDisabled = default;
        /// <inheritdoc/>
        public event UnityAction OnDisabled
        {
            add => onDisabled.AddListener(value);
            remove => onDisabled.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the state of the object changed.")]
        private UnityEvent<bool> onStateChanged = default;
        /// <inheritdoc/>
        public event UnityAction<bool> OnStateChanged
        {
            add => onStateChanged.AddListener(value);
            remove => onStateChanged.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private HashSet<int> registeredComponents = new HashSet<int>();
        /// <inheritdoc/>
        public bool IsEnabled => defaultState == (registeredComponents.Count == 0);


        // --- | Constructors | --------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new <see cref="Enableable"/> object.
        /// </summary>
        /// <param name="enabledByDefault">True if the object is enabled by default.</param>
        public Enableable(bool enabledByDefault)
        {
            defaultState = enabledByDefault;
        }
        /// <summary>
        /// Creates a new <see cref="Enableable"/> object.
        /// </summary>
        public Enableable() : this(true) { }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Enable(Object component)
        {
            if (defaultState)
            {
                RemoveComponent(component, onEnabled);
            }
            else
            {
                AddComponent(component, onEnabled);
            }
        }

        /// <inheritdoc/>
        public void Disable(Object component)
        {
            if (defaultState)
            {
                AddComponent(component, onDisabled);
            }
            else
            {
                RemoveComponent(component, onDisabled);
            }
        }

        /// <summary>
        /// Add a component to the hash set.
        /// </summary>
        /// <param name="component">The component to add.</param>
        /// <param name="filled">The event to call when the hash set gets filled.</param>
        private void AddComponent(Object component, UnityEvent filled)
        {
            int id = component.GetInstanceID();
            if (!registeredComponents.Contains(id))
            {
                registeredComponents.Add(id);
                if (registeredComponents.Count == 1)
                {
                    filled?.Invoke();
                    onStateChanged?.Invoke(IsEnabled);
                }
            }
        }

        /// <summary>
        /// Removes a component from the hash set.
        /// </summary>
        /// <param name="compoent">The component to remove.</param>
        /// <param name="emptied">The event to call when the hash set gets emptied.</param>
        private void RemoveComponent(Object compoent, UnityEvent emptied)
        {
            int id = compoent.GetInstanceID();
            if (registeredComponents.Contains(id))
            {
                registeredComponents.Remove(id);
                if (registeredComponents.Count == 0)
                {
                    emptied?.Invoke();
                    onStateChanged?.Invoke(IsEnabled);
                }
            }
        }
    }

    public interface IEnableable
    {
        /// <summary>
        /// True if the object is enabled.
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Called when the object gets enabled.
        /// </summary>
        event UnityAction OnEnabled;
        /// <summary>
        /// Called whent the object gets disabled.
        /// </summary>
        event UnityAction OnDisabled;
        /// <summary>
        /// Called when the state of the object changed.
        /// </summary>
        event UnityAction<bool> OnStateChanged;

        /// <summary>
        /// Enables the object.
        /// </summary>
        /// <param name="component">The component enabling the object</param>
        void Enable(Object component);
        /// <summary>
        /// Disables the object.
        /// </summary>
        /// <param name="component">The component disabling the object.</param>
        void Disable(Object component);
    }
}
