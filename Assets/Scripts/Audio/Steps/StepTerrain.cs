using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "stepTerrain", menuName = "Audio/Step Terrain")]
public class StepTerrain : ScriptableObject
{
    public enum tagNames {texture_terrain, sand_terrain, gravel_terrain, water__terrain, stone_terrain , grass_terrain , dirt_terrain, ladder_terrain };
    public tagNames tagName;
    public StepSample[] StepSamples;
    public Color colorOnMap;
    [Range(0,1)]
    public float volumeMin = 1, volumeMax = 1;
    [Range(-1, 2)]
    public float pitchMin = 1, pitchMax = 1;

    public StepSample GetClip()
    {
        float weight = Random.value - 1;
        int r = Random.Range(0, StepSamples.Length);
        StepSample c = null;
        while (c == null)
        {
            if (StepSamples[r].weight >= weight)
            {
                c = StepSamples[r];
            }
            else
            {
                r = Random.Range(0, StepSamples.Length);
            }
        }

        c.volume = Random.Range(volumeMin, volumeMax);
        c.pitch = Random.Range(pitchMin, pitchMax);

        return c;
    }
}

[System.Serializable]
public class StepSample
{
    public AudioClip Sample;
    [Range(-1, 0)]
    public float weight = 1;

    [System.NonSerialized]
    public float volume, pitch;


    StepSample()
    {
        weight = 1;
    }
    //public float volume = 1, pitch = 1;
}
