using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateByHand : InteractableBase
{
    Vector3 mPrevPos = Vector3.zero;
    Vector3 mPosDelta = Vector3.zero;
    bool active;

    public List<SpinObject> rotationObjects = new List<SpinObject>();

    public void OnInteract(float rotation)
    {
        if (rotation < 20 && rotation > -20)
        {
            rotation /= 5;
            Quaternion rot = Quaternion.Euler(0, rotation, 0);
            transform.rotation *= rot;

            if (rotation < 0)
            {
                SpinObject(-0.4f);
            } else
            {
                SpinObject(0.4f);
            }
        }

    }


    public void SpinObject(float rotation)
    {

        for (int i = 0; i < rotationObjects.Count; i++)
        {
            if (rotationObjects[i].spinable)
            {
                rotationObjects[i].currentRot += rotation;
                rotationObjects[i].obj.transform.rotation = Quaternion.Euler(0, rotationObjects[i].currentRot, 0);
            }

            if (rotationObjects[i].obj.transform.rotation.eulerAngles.y < rotationObjects[i].minRot)
            {
                rotationObjects[i].currentRot = rotationObjects[i].minRot;
                rotationObjects[i].obj.transform.rotation = Quaternion.Euler(0, rotationObjects[i].minRot, 0);
            }

            if (rotationObjects[i].obj.transform.rotation.eulerAngles.y > rotationObjects[i].maxRot)
            {
                rotationObjects[i].currentRot = rotationObjects[i].maxRot;
                rotationObjects[i].obj.transform.rotation = Quaternion.Euler(0, rotationObjects[i].maxRot, 0);
            }
        }
    }
}

[System.Serializable]
public class SpinObject
{
    public GameObject obj;
    public float minRot, maxRot, currentRot;
    public bool spinable;
}
