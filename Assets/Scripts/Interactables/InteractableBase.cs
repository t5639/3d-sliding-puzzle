using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBase : MonoBehaviour
{
    public string idCode;

    public Sprite image;
    public Sprite actionImage;

    public string interactText = "Press F to X";

    public virtual void OnInteract()
    {

    }
}
