using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [ContextMenu("Open")]
    public void Open() => Set(true);

    [ContextMenu("Close")]
    public void Close() => Set(false);

    public void Set(bool isOpen)
    {
        animator.SetBool("Open", isOpen);
    }
}
