using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LeverStation : InteractableBase
{

    public GameObject lever_obj;
    bool full  = false;

    public UnityEvent OnStickIn;

    public void OnInteract(InteractableInvBase item)
    {
        if (!full)
        {
            OnStickIn.Invoke();
            lever_obj.SetActive(true);
            Inventory.instance.RemoveItem(item);
            full = true;
            Destroy(this);
        }
    
    }

}
