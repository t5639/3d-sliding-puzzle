public enum music 
{
	empty = 0,
	bassline = 1,
	clarinet_calm = 2,
	drums = 3,
	flute_calm = 4,
	flute_loud = 5,
	guitar_calm = 6,
	violin_calm = 7,
	pad_calm = 8,
	flute_calmSingle = 9,
	flute_loudSingle = 10,

}
