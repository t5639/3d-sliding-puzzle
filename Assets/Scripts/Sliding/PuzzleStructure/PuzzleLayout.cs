namespace ShiftingMaze.Puzzle.Strcuture
{
    using System.Collections.Generic;
    using UnityEngine;

    // TODO: Find a way to select the different tiles in the puzzle matrix.

    /// <summary>
    /// The matrix for the puzzles.
    /// </summary>
    [CreateAssetMenu(fileName = "Puzzle", menuName = "SlidingPuzzle")]
    public class PuzzleLayout : ScriptableObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, LockInPlaymode, Tooltip("The structures of the tiles in the puzzle.")]
        private List<TileStructure> structures = default;
        /// <summary>
        /// The structures of the tiles in the puzzle.
        /// </summary>
        public IEnumerable<TileStructure> Structures => structures;

        [SerializeField, Min(0), LockInPlaymode, Tooltip("The amount of tiles along the horizontal axis.")]
        private int width = default;
        /// <summary>
        /// The amount of tiles along the horizontal axis.
        /// </summary>
        public int Width => width;

        [SerializeField, Min(0), LockInPlaymode, Tooltip("The amount of tiles along the vertical axis.")]
        private int height = default;
        /// <summary>
        /// The amount of tiles along the vertical axis.
        /// </summary>
        public int Height => height;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public IEnumerable<(TileStructure structure, Vector2Int cooridnates)> EachStructureWithCoordinates()
        {
            Vector2Int coordinates = Vector2Int.zero;
            for (; coordinates.y < height; coordinates.y++)
            {
                for (; coordinates.x < width; coordinates.x++)
                {
                    yield return (structures[coordinates.x + coordinates.y * width], coordinates);
                }
                coordinates.x = 0;
            }
        }
    }
}
