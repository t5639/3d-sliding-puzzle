using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepTexture : MonoBehaviour
{
    public float tileXsize = 40, tileZsize = 40;
    [System.NonSerialized]
    public float xMin, xMax, zMin, zMax;
    [SerializeField]
    private Texture2D texture;
    public bool transformIsLeftDownCorner = false;

    private void Awake()
    {
        if (!transformIsLeftDownCorner)
        {
            Vector3 mid = transform.position;
            xMin = mid.x - tileXsize / 2;
            xMax = mid.x + tileXsize / 2;
            zMin = mid.z - tileZsize / 2;
            xMax = mid.z + tileZsize / 2;
        }
        else
        {
            Vector3 mid = transform.position;
            xMin = mid.x;
            xMax = mid.x + tileXsize;
            zMin = mid.z;
            xMax = mid.z + tileZsize;
        }
    }

    public Color GetTextureColorAt(int x, int z)
    {
        int y = z;
        return texture.GetPixel(x,y);
    }
}
