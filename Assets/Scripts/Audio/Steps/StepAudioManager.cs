using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class StepAudioManager : MonoBehaviour
{
    public static StepAudioManager Instance;
    public StepTerrain[] StepTerrains;

    public Transform Player;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public StepSample GetStep(Transform source)
    {
        RaycastHit hit;
        StepTerrain t = null;

        if (Physics.Raycast(source.position, source.TransformDirection(Vector3.down), out hit,  Mathf.Infinity))
        {
            StepTerrain.tagNames tag;

            if (hit.transform.gameObject.tag != "Untagged")
            {
                 tag = (StepTerrain.tagNames)System.Enum.Parse(typeof(StepTerrain.tagNames), hit.transform.gameObject.tag);
            } else
            {
                tag = StepTerrain.tagNames.sand_terrain;
                Debug.LogError(hit.transform.gameObject +" is not tagged for being stepped on");
            }

            if (tag == StepTerrain.tagNames.texture_terrain)
            {
                StepTexture tex = hit.transform.gameObject.GetComponent<StepTexture>();
                t = GetTerrainFromTexture(tex);
            }
            else
            {
                t = GetTagTerrain(tag);
            }
        }
        else
        {
            Debug.LogError("No Terrain Found");
        }

        return t.GetClip();
    }

    public StepSample GetStep(StepTerrain.tagNames terrainTag)
    {
        return GetTagTerrain(terrainTag).GetClip();
    }

        public StepTerrain GetTagTerrain(StepTerrain.tagNames tag)
    {
        foreach (StepTerrain sT in StepTerrains)
        {
            if (sT.tagName == tag)
            {
                return sT;
            }
        }
        Debug.LogError("No Terrain with tag "+tag.ToString());
        return null;
    }

    public StepTerrain GetTerrainFromTexture(StepTexture tex)
    {

        int x = (int)map(Player.position.x, tex.xMin , tex.xMax, 0, tex.tileXsize);
        int z = (int)map(Player.position.z, tex.zMin , tex.zMax, 0, tex.tileZsize);

        Color c = tex.GetTextureColorAt(x,z);


        foreach(StepTerrain sT in StepTerrains)
        {
            if (sT.colorOnMap ==  c)
            {
                return sT;
            }
        }

        Debug.LogWarning("NoTerrainFound in Texture");
        return StepTerrains[0];
    }



    private float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

}
