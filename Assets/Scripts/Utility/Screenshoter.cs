using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screenshoter : MonoBehaviour
{
    public string filename;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        ScreenCapture.CaptureScreenshot("Assets\\Materials\\Textures\\" + filename +".png", 1);
        this.enabled = false;
    }
}
