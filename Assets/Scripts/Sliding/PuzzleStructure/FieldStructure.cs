namespace ShiftingMaze.Puzzle.Strcuture
{
    using UnityEngine;

    /// <summary>
    /// The class for all puzzle fields.
    /// </summary>
    public class FieldStructure : BorderedStructure
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The connection to the other fields.")]
        private CardinalConnections connections = default;
        /// <summary>
        /// The connection to the other fields.
        /// </summary>
        public CardinalConnections Connections => connections;
    }
}
