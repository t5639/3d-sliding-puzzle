namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using UnityEngine;

    public class PuzzlePrefabInstantiater : PuzzleTileBuilder
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The size of a tile.")]
        protected Vector2 tileSize = default;
        /// <inheritdoc/>
        public override Vector2 TileSize => tileSize;

        [SerializeField, Tooltip("The vertical offset of the tiles from the border tiles.")]
        protected float tileGroundOffset = 0f;

        [Header("Tile Prefabs")] // ---------------------------------------------------------------
        [SerializeField, Tooltip("The tile with a straint connection.")]
        private GameObject iTiele = default;
        [SerializeField, Tooltip("The tile with a curved connection.")]
        private GameObject lTile = default;
        [SerializeField, Tooltip("The tile with 3 connections.")]
        private GameObject tTile = default;
        [SerializeField, Tooltip("The tile with all 3 connections.")]
        private GameObject xTile = default;
        [SerializeField, Tooltip("The tile with only one conenction.")]
        private GameObject uTile = default;
        [SerializeField, Tooltip("The tile without connections.")]
        private GameObject oTile = default;

        [Header("Border Prefabs")] // ---------------------------------------------------------------
        [SerializeField, Tooltip("The border tile with no connections.")]
        private GameObject cleanWall = default;
        [SerializeField, Tooltip("The border tile with a horizontal connection.")]
        private GameObject horizontalWall = default;
        [SerializeField, Tooltip("The border tile with a vertical connection.")]
        private GameObject verticalWall = default;
        [SerializeField, Tooltip("The border tile with a north to east connection.")]
        private GameObject topRightWall = default;
        [SerializeField, Tooltip("The border tile with a north to west connection.")]
        private GameObject topLeftWall = default;
        [SerializeField, Tooltip("The border tile with a south to east connection.")]
        private GameObject bottomRightWall = default;
        [SerializeField, Tooltip("The border tile with a south to west connection.")]
        private GameObject bottomLeftWall = default;
        [SerializeField, Tooltip("The corner tile with no connections.")]
        private GameObject cleanCorner = default;
        [SerializeField, Tooltip("The corner tile with connections.")]
        private GameObject drainCorner = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override PuzzleTile BuildTile(TileStructure structure, CardinalPoints borders)
        {
            // Create parent for the tile.
            PuzzleTile tile = PuzzleTile.AddComponent(new GameObject(), structure);

            GameObject prefab = null;
            float rotation = 0f;
            float offset = 0f;

            // Check if the tile has connections.
            if (structure is BorderedStructure)
            {
                // Check if the tile is not a border tile.
                if (borders == CardinalPoints.None)
                {
                    // Get the correct tile.
                    (prefab, rotation) = GetTilePrefab(((BorderedStructure)structure).Entries);
                    offset = tileGroundOffset;
                }
                else
                {
                    // Get the correct border tile.
                    (prefab, rotation) = GetBorderPrefab(((BorderedStructure)structure).Entries, borders);
                }
            }
            else if (borders != CardinalPoints.None)
            {
                // Check if the tile is a border tile.
                (prefab, rotation) = GetBorderPrefab(CardinalPoints.None, borders);
            }
            if (prefab)
            {
                // Instantiate the tile inside the parent, with the correct offset and roation.
                Instantiate(prefab, tile.transform.position + new Vector3(tileSize.x, offset, tileSize.y) * 0.5f, Quaternion.Euler(0f, rotation, 0f), tile.transform);
            }
            return tile;
        }

        /// <summary>
        /// Looks the correct prefab up.
        /// </summary>
        /// <param name="points">The entries of the tile.</param>
        /// <returns>The prefab and its rotation in degrees.</returns>
        private (GameObject prefab, float rotation) GetTilePrefab(CardinalPoints points)
        {
            switch ((int)points)
            {
                default: return (oTile,  0f);       // - NONE -
                case  1: return (uTile,  0f);       // - NORTH -
                case  2: return (uTile,  90f);      // - EAST -
                case  3: return (lTile,  0f);       // north + east
                case  4: return (uTile,  180f);     // - SOUTH -
                case  5: return (iTiele, 0f);       // north + south
                case  6: return (lTile,  90f);      // east + south
                case  7: return (tTile, -90f);      // north + east + south
                case  8: return (uTile, -90f);      // - WEST -
                case  9: return (lTile, -90f);      // north + west
                case 10: return (iTiele, 90f);      // east + west
                case 11: return (tTile,  180f);     // north + east + west
                case 12: return (lTile,  180f);     // south + west
                case 13: return (tTile,  90f);      // north + south + west
                case 14: return (tTile,  0f);       // east + south + west
                case 15: return (xTile,  0f);       // north + east + south + west
            }
        }

        /// <summary>
        /// Get the correct border prefab.
        /// </summary>
        /// <param name="connections">The connections of the border.</param>
        /// <param name="borders">The sides the tile is touching the border.</param>
        /// <returns>The prefab and its rotation.</returns>
        private (GameObject prefab, float rotation) GetBorderPrefab(CardinalPoints connections, CardinalPoints borders)
        {
            GameObject prefab; float rotation;
            switch (borders)
            {
                case CardinalPoints.North:
                    rotation = 180f;
                    prefab = GetWall(connections);
                    break;
                case CardinalPoints.East:
                    rotation = -90f;
                    prefab = GetWall(CardinalPointUtilities.RotatateClockwise(connections, -1));
                    break;
                case CardinalPoints.South:
                    rotation = 0f;
                    prefab = GetWall(CardinalPointUtilities.RotatateClockwise(connections, 2));
                    break;
                case CardinalPoints.West:
                    rotation = 90f;
                    prefab = GetWall(CardinalPointUtilities.RotatateClockwise(connections, 1));
                    break;
                case CardinalPoints.North | CardinalPoints.East:
                    rotation = -90f;
                    prefab = GetCorner(borders, connections);
                    break;
                case CardinalPoints.East | CardinalPoints.South:
                    rotation = 0f;
                    prefab = GetCorner(borders, connections);
                    break;
                case CardinalPoints.South | CardinalPoints.West:
                    rotation = 90f;
                    prefab = GetCorner(borders, connections);
                    break;
                case CardinalPoints.North | CardinalPoints.West:
                    rotation = 180f;
                    prefab = GetCorner(borders, connections);
                    break;
                default:
                    prefab = null;
                    rotation = 0f;
                    break;
            }
            return (prefab, rotation);
        }

        /// <summary>
        /// Returns the correct wall prefab.
        /// </summary>
        /// <param name="connections">The connections of the wall.</param>
        /// <returns>The wall prefab.</returns>
        private GameObject GetWall(CardinalPoints connections)
        {
            switch ((int)connections)
            {
                case 0: return cleanWall;
                case 3: return bottomLeftWall;
                case 5: return verticalWall;
                case 9: return bottomRightWall;
                case 6: return topLeftWall;
                case 10: return horizontalWall;
                case 12: return topRightWall;
                default: return null;
            }
        }

        /// <summary>
        /// Returns the correct corner prefab.
        /// </summary>
        /// <param name="borders">The borders of the corner.</param>
        /// <param name="connections">The connections of the border.</param>
        /// <returns>The corner prefab.</returns>
        private GameObject GetCorner(CardinalPoints borders, CardinalPoints connections)
        {
            if (connections == CardinalPoints.None)
            {
                return cleanCorner;
            }
            if (CardinalPointUtilities.RotatateClockwise(borders, 2) == connections)
            {
                return drainCorner;
            }
            return null;
        }
    }
}
