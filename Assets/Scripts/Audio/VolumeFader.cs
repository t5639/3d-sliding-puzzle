using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeFader : MonoBehaviour
{
    private float fadeTime;
    private AudioSource source;

    private float fadeTimer = -1;
    private bool fadeIn = false, fadeOut = false;
    private float vol = 0;

    private void Awake()
    {
        source = this.gameObject.GetComponent<AudioSource>();
    }


    private void Update()
    {
        if (fadeIn || fadeOut)
        {
            fadeTimer -= Time.deltaTime;
            vol = fadeOut ? 1 / fadeTime * fadeTimer : 1 / fadeTime * (fadeTime - fadeTimer);
            source.volume = vol;
            if(fadeTimer <= 0)
            {
                source.volume = fadeOut ? 0 : 1;
                if (fadeOut)
                {
                    source.Stop();
                    fadeOut = false;
                }
                else
                {
                    fadeIn = false;

                }
            }
        }
    }

    public void FadeIn(float fadeTime)
    {
        this.fadeTime = fadeTime;
        fadeIn = true;
        fadeTimer = fadeTime;
        vol = 0;
        source.volume = vol;
        source.Play();
    }

    public void FadeOut(float fadeTime)
    {
        this.fadeTime = fadeTime;
        fadeOut = true;
        fadeTimer = fadeTime;
        vol = 1;
        source.volume = vol;
    }



}
