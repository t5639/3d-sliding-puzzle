// GENERATED AUTOMATICALLY FROM 'Assets/Testing/InputSystems/PlayerSystem.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerSystem : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerSystem()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerSystem"",
    ""maps"": [
        {
            ""name"": ""GroundMovement"",
            ""id"": ""c7a8c2fd-7306-4820-be10-9f033d5fc792"",
            ""actions"": [
                {
                    ""name"": ""HorizontalMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""b16e1dca-2c4b-4a4b-9b40-939f8b6f71af"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""db4442bb-fc87-461d-80a8-da55964d0384"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseX"",
                    ""type"": ""PassThrough"",
                    ""id"": ""48122302-87a2-42dd-b7cf-805d93ed3e9b"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseY"",
                    ""type"": ""PassThrough"",
                    ""id"": ""111f3a8e-20d3-464b-8c43-43f6421570aa"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CameraMove"",
                    ""type"": ""Value"",
                    ""id"": ""01e532d4-a515-4b22-86b5-3f92c5055d95"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Sprint"",
                    ""type"": ""Button"",
                    ""id"": ""669aa5fb-a58c-4a8e-a829-3550ac8bc7f9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Zoom"",
                    ""type"": ""Button"",
                    ""id"": ""967f5f04-6192-40b5-b139-7c20951c4f55"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""27b472fc-3ac7-47ba-ad34-26d70869619e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""4171f905-de68-4148-9a66-e5cbaa5b6b9e"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""4dc21ec5-512c-498d-99d1-08e248806969"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""866e4efa-a064-42eb-b28a-86477ee2c831"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""204313ad-aa46-4cfc-86d5-589b11a6a7f4"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""1d76abd5-d176-4db7-b98f-d1493f905697"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""9b4e6b49-54f4-42d2-b04e-70b60204b2dc"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone"",
                    ""groups"": """",
                    ""action"": ""HorizontalMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4d54ae9c-dc5b-429d-9639-05dbdc05f020"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c1e34580-da85-4c6b-ad6a-a8c3b16dec12"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""44a652b9-dd92-40e6-a9e4-c2b6220d562d"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""01fc50a2-a31f-47e0-9e24-95f1d8bff262"",
                    ""path"": ""<Gamepad>/rightStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""21aed440-1f9d-4fe7-8c34-62f332646c88"",
                    ""path"": ""<Mouse>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""35608a83-0cae-41b3-ad85-520179b52e66"",
                    ""path"": ""<Gamepad>/leftStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf1606c0-8e8b-417f-9aeb-b862e75ae9db"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=0.3,y=0.3)"",
                    ""groups"": """",
                    ""action"": ""CameraMove"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b7455a3f-7fae-462c-a8e1-2b110d9d8f78"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone,ScaleVector2(x=5,y=5)"",
                    ""groups"": """",
                    ""action"": ""CameraMove"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d6c05bea-a8b8-4ecc-8b48-082c251d1f26"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7960cd1c-b8ee-4a3c-ab4f-258cef486690"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e69972e2-b5b3-4d03-8122-56594f90948a"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Zoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f098fb01-e40b-423a-8a9a-d84815cc0a74"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Zoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""98da9025-6222-444b-86e6-8279afacd7e4"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a952549-2cfc-4b9d-b8bb-00358bad6ad4"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Interaction"",
            ""id"": ""a417caec-f08d-4152-8ab7-891412206ef5"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""a1d79773-c133-4179-98fd-353a8af60977"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SwitchInventory"",
                    ""type"": ""Button"",
                    ""id"": ""c91d3ac5-4fb3-4f3b-b7fc-1efbf7fa14fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Detection"",
                    ""type"": ""PassThrough"",
                    ""id"": ""1e467cfc-3720-4100-86d8-503a75c14aeb"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""03eed22f-d281-491b-81d7-fbe7be7029cf"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e976ced-0335-4a28-ba09-dcfd9c110cd9"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d38ce142-d97d-47b2-80ea-2e62c5c180e0"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchInventory"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a447cd3c-971a-4b99-a02d-c88ef0bb3eb1"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchInventory"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""FocusInteraction"",
            ""id"": ""57a7d4c7-40b1-45a8-8d3d-aa51d3b95cd4"",
            ""actions"": [
                {
                    ""name"": ""MousePos"",
                    ""type"": ""PassThrough"",
                    ""id"": ""c4be06d2-82d6-4b90-ad5e-00c96344379b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectPlate"",
                    ""type"": ""Button"",
                    ""id"": ""4cb21a07-b2aa-4b2f-b262-3df4c90d0ba5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Scroll"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ca4266cc-369e-4aae-996e-3673220c9c4f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotatebyHand"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ee4357dc-2bcf-493e-be1c-95fdd3b5a4cf"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotatePressed"",
                    ""type"": ""Button"",
                    ""id"": ""662ca1ac-c3fb-485f-9ab2-f597fb066f70"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FocusOut"",
                    ""type"": ""Button"",
                    ""id"": ""5f74726f-e9bb-4c8a-972d-13e0d39a410a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""28f9b74f-43b9-4237-aadf-1b6646324885"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Scroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fa786bac-71c2-4539-9ece-16f6904254f2"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePos"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dcb48774-34ec-49c4-a72c-4f6627d3e17e"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RotatebyHand"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d1d59451-04b6-4fa7-9c63-36e7b3ac6046"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Press(behavior=2)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RotatePressed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e0a0389a-9a07-44f5-a272-f4d20a845fb6"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FocusOut"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""efe9cc1a-eaf7-4947-b0cb-9b7152f927c9"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FocusOut"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6b6e5256-ccf0-40ea-a7b8-11b73fc3c876"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectPlate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PauseMenu"",
            ""id"": ""c98e4cf6-535c-4bce-8b9a-3c9250456e95"",
            ""actions"": [
                {
                    ""name"": ""EndPause"",
                    ""type"": ""Button"",
                    ""id"": ""99810748-d6db-4924-b085-8dc7b2c6fea2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""93b436b5-0dbd-4bf0-9bbb-29d8383e14a2"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EndPause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""StoneTable"",
            ""id"": ""7c11846c-1f0b-4114-8cf6-7b112f8d9908"",
            ""actions"": [
                {
                    ""name"": ""Up"",
                    ""type"": ""Button"",
                    ""id"": ""a3e201bb-4ea6-47fd-98c8-627593eade33"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Button"",
                    ""id"": ""84c5d3f2-c552-446d-ae92-f88e55184a4f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""5a53d17c-810f-4d8e-a1e8-0f82d306788f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Left"",
                    ""type"": ""Button"",
                    ""id"": ""67327c5e-2dcd-4bd2-ba60-4434c034e9d6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a9008916-7072-4ce3-a3e1-62d21226ca86"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cf6b8d2a-4a8a-4ce0-bce8-f19aa82dbb68"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5c404cdc-ef2b-46e6-a42a-d61b7c930c8e"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d71e2ca8-441d-417a-abdd-e93a028752e3"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fb845a71-22f8-4dff-a3f0-e441969b2867"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97fce58a-c3e7-432c-acde-781ee153d1e7"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""595fe645-9d42-4a93-8bab-1edeeee42869"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""09ce45bf-18d1-44cb-8013-4b9918c9c088"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25401c1a-ed60-41b0-b596-3b8d53b53646"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""81773621-045f-4bfe-91c5-9995b1bc9589"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""12ae92f4-4bc0-4fd3-9d61-22891b869b7a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e6b4e65-3dae-4e34-9c7d-bac14beb4928"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // GroundMovement
        m_GroundMovement = asset.FindActionMap("GroundMovement", throwIfNotFound: true);
        m_GroundMovement_HorizontalMovement = m_GroundMovement.FindAction("HorizontalMovement", throwIfNotFound: true);
        m_GroundMovement_Jump = m_GroundMovement.FindAction("Jump", throwIfNotFound: true);
        m_GroundMovement_MouseX = m_GroundMovement.FindAction("MouseX", throwIfNotFound: true);
        m_GroundMovement_MouseY = m_GroundMovement.FindAction("MouseY", throwIfNotFound: true);
        m_GroundMovement_CameraMove = m_GroundMovement.FindAction("CameraMove", throwIfNotFound: true);
        m_GroundMovement_Sprint = m_GroundMovement.FindAction("Sprint", throwIfNotFound: true);
        m_GroundMovement_Zoom = m_GroundMovement.FindAction("Zoom", throwIfNotFound: true);
        m_GroundMovement_Pause = m_GroundMovement.FindAction("Pause", throwIfNotFound: true);
        // Interaction
        m_Interaction = asset.FindActionMap("Interaction", throwIfNotFound: true);
        m_Interaction_Interact = m_Interaction.FindAction("Interact", throwIfNotFound: true);
        m_Interaction_SwitchInventory = m_Interaction.FindAction("SwitchInventory", throwIfNotFound: true);
        m_Interaction_Detection = m_Interaction.FindAction("Detection", throwIfNotFound: true);
        // FocusInteraction
        m_FocusInteraction = asset.FindActionMap("FocusInteraction", throwIfNotFound: true);
        m_FocusInteraction_MousePos = m_FocusInteraction.FindAction("MousePos", throwIfNotFound: true);
        m_FocusInteraction_SelectPlate = m_FocusInteraction.FindAction("SelectPlate", throwIfNotFound: true);
        m_FocusInteraction_Scroll = m_FocusInteraction.FindAction("Scroll", throwIfNotFound: true);
        m_FocusInteraction_RotatebyHand = m_FocusInteraction.FindAction("RotatebyHand", throwIfNotFound: true);
        m_FocusInteraction_RotatePressed = m_FocusInteraction.FindAction("RotatePressed", throwIfNotFound: true);
        m_FocusInteraction_FocusOut = m_FocusInteraction.FindAction("FocusOut", throwIfNotFound: true);
        // PauseMenu
        m_PauseMenu = asset.FindActionMap("PauseMenu", throwIfNotFound: true);
        m_PauseMenu_EndPause = m_PauseMenu.FindAction("EndPause", throwIfNotFound: true);
        // StoneTable
        m_StoneTable = asset.FindActionMap("StoneTable", throwIfNotFound: true);
        m_StoneTable_Up = m_StoneTable.FindAction("Up", throwIfNotFound: true);
        m_StoneTable_Right = m_StoneTable.FindAction("Right", throwIfNotFound: true);
        m_StoneTable_Down = m_StoneTable.FindAction("Down", throwIfNotFound: true);
        m_StoneTable_Left = m_StoneTable.FindAction("Left", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // GroundMovement
    private readonly InputActionMap m_GroundMovement;
    private IGroundMovementActions m_GroundMovementActionsCallbackInterface;
    private readonly InputAction m_GroundMovement_HorizontalMovement;
    private readonly InputAction m_GroundMovement_Jump;
    private readonly InputAction m_GroundMovement_MouseX;
    private readonly InputAction m_GroundMovement_MouseY;
    private readonly InputAction m_GroundMovement_CameraMove;
    private readonly InputAction m_GroundMovement_Sprint;
    private readonly InputAction m_GroundMovement_Zoom;
    private readonly InputAction m_GroundMovement_Pause;
    public struct GroundMovementActions
    {
        private @PlayerSystem m_Wrapper;
        public GroundMovementActions(@PlayerSystem wrapper) { m_Wrapper = wrapper; }
        public InputAction @HorizontalMovement => m_Wrapper.m_GroundMovement_HorizontalMovement;
        public InputAction @Jump => m_Wrapper.m_GroundMovement_Jump;
        public InputAction @MouseX => m_Wrapper.m_GroundMovement_MouseX;
        public InputAction @MouseY => m_Wrapper.m_GroundMovement_MouseY;
        public InputAction @CameraMove => m_Wrapper.m_GroundMovement_CameraMove;
        public InputAction @Sprint => m_Wrapper.m_GroundMovement_Sprint;
        public InputAction @Zoom => m_Wrapper.m_GroundMovement_Zoom;
        public InputAction @Pause => m_Wrapper.m_GroundMovement_Pause;
        public InputActionMap Get() { return m_Wrapper.m_GroundMovement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GroundMovementActions set) { return set.Get(); }
        public void SetCallbacks(IGroundMovementActions instance)
        {
            if (m_Wrapper.m_GroundMovementActionsCallbackInterface != null)
            {
                @HorizontalMovement.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnHorizontalMovement;
                @HorizontalMovement.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnHorizontalMovement;
                @HorizontalMovement.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnHorizontalMovement;
                @Jump.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnJump;
                @MouseX.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnMouseX;
                @MouseX.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnMouseX;
                @MouseX.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnMouseX;
                @MouseY.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnMouseY;
                @MouseY.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnMouseY;
                @MouseY.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnMouseY;
                @CameraMove.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnCameraMove;
                @CameraMove.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnCameraMove;
                @CameraMove.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnCameraMove;
                @Sprint.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnSprint;
                @Sprint.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnSprint;
                @Sprint.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnSprint;
                @Zoom.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnZoom;
                @Zoom.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnZoom;
                @Zoom.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnZoom;
                @Pause.started -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_GroundMovementActionsCallbackInterface.OnPause;
            }
            m_Wrapper.m_GroundMovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                @HorizontalMovement.started += instance.OnHorizontalMovement;
                @HorizontalMovement.performed += instance.OnHorizontalMovement;
                @HorizontalMovement.canceled += instance.OnHorizontalMovement;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @MouseX.started += instance.OnMouseX;
                @MouseX.performed += instance.OnMouseX;
                @MouseX.canceled += instance.OnMouseX;
                @MouseY.started += instance.OnMouseY;
                @MouseY.performed += instance.OnMouseY;
                @MouseY.canceled += instance.OnMouseY;
                @CameraMove.started += instance.OnCameraMove;
                @CameraMove.performed += instance.OnCameraMove;
                @CameraMove.canceled += instance.OnCameraMove;
                @Sprint.started += instance.OnSprint;
                @Sprint.performed += instance.OnSprint;
                @Sprint.canceled += instance.OnSprint;
                @Zoom.started += instance.OnZoom;
                @Zoom.performed += instance.OnZoom;
                @Zoom.canceled += instance.OnZoom;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
            }
        }
    }
    public GroundMovementActions @GroundMovement => new GroundMovementActions(this);

    // Interaction
    private readonly InputActionMap m_Interaction;
    private IInteractionActions m_InteractionActionsCallbackInterface;
    private readonly InputAction m_Interaction_Interact;
    private readonly InputAction m_Interaction_SwitchInventory;
    private readonly InputAction m_Interaction_Detection;
    public struct InteractionActions
    {
        private @PlayerSystem m_Wrapper;
        public InteractionActions(@PlayerSystem wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Interaction_Interact;
        public InputAction @SwitchInventory => m_Wrapper.m_Interaction_SwitchInventory;
        public InputAction @Detection => m_Wrapper.m_Interaction_Detection;
        public InputActionMap Get() { return m_Wrapper.m_Interaction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InteractionActions set) { return set.Get(); }
        public void SetCallbacks(IInteractionActions instance)
        {
            if (m_Wrapper.m_InteractionActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @SwitchInventory.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnSwitchInventory;
                @SwitchInventory.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnSwitchInventory;
                @SwitchInventory.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnSwitchInventory;
                @Detection.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnDetection;
                @Detection.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnDetection;
                @Detection.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnDetection;
            }
            m_Wrapper.m_InteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @SwitchInventory.started += instance.OnSwitchInventory;
                @SwitchInventory.performed += instance.OnSwitchInventory;
                @SwitchInventory.canceled += instance.OnSwitchInventory;
                @Detection.started += instance.OnDetection;
                @Detection.performed += instance.OnDetection;
                @Detection.canceled += instance.OnDetection;
            }
        }
    }
    public InteractionActions @Interaction => new InteractionActions(this);

    // FocusInteraction
    private readonly InputActionMap m_FocusInteraction;
    private IFocusInteractionActions m_FocusInteractionActionsCallbackInterface;
    private readonly InputAction m_FocusInteraction_MousePos;
    private readonly InputAction m_FocusInteraction_SelectPlate;
    private readonly InputAction m_FocusInteraction_Scroll;
    private readonly InputAction m_FocusInteraction_RotatebyHand;
    private readonly InputAction m_FocusInteraction_RotatePressed;
    private readonly InputAction m_FocusInteraction_FocusOut;
    public struct FocusInteractionActions
    {
        private @PlayerSystem m_Wrapper;
        public FocusInteractionActions(@PlayerSystem wrapper) { m_Wrapper = wrapper; }
        public InputAction @MousePos => m_Wrapper.m_FocusInteraction_MousePos;
        public InputAction @SelectPlate => m_Wrapper.m_FocusInteraction_SelectPlate;
        public InputAction @Scroll => m_Wrapper.m_FocusInteraction_Scroll;
        public InputAction @RotatebyHand => m_Wrapper.m_FocusInteraction_RotatebyHand;
        public InputAction @RotatePressed => m_Wrapper.m_FocusInteraction_RotatePressed;
        public InputAction @FocusOut => m_Wrapper.m_FocusInteraction_FocusOut;
        public InputActionMap Get() { return m_Wrapper.m_FocusInteraction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(FocusInteractionActions set) { return set.Get(); }
        public void SetCallbacks(IFocusInteractionActions instance)
        {
            if (m_Wrapper.m_FocusInteractionActionsCallbackInterface != null)
            {
                @MousePos.started -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnMousePos;
                @MousePos.performed -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnMousePos;
                @MousePos.canceled -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnMousePos;
                @SelectPlate.started -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnSelectPlate;
                @SelectPlate.performed -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnSelectPlate;
                @SelectPlate.canceled -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnSelectPlate;
                @Scroll.started -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnScroll;
                @Scroll.performed -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnScroll;
                @Scroll.canceled -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnScroll;
                @RotatebyHand.started -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnRotatebyHand;
                @RotatebyHand.performed -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnRotatebyHand;
                @RotatebyHand.canceled -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnRotatebyHand;
                @RotatePressed.started -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnRotatePressed;
                @RotatePressed.performed -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnRotatePressed;
                @RotatePressed.canceled -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnRotatePressed;
                @FocusOut.started -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnFocusOut;
                @FocusOut.performed -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnFocusOut;
                @FocusOut.canceled -= m_Wrapper.m_FocusInteractionActionsCallbackInterface.OnFocusOut;
            }
            m_Wrapper.m_FocusInteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MousePos.started += instance.OnMousePos;
                @MousePos.performed += instance.OnMousePos;
                @MousePos.canceled += instance.OnMousePos;
                @SelectPlate.started += instance.OnSelectPlate;
                @SelectPlate.performed += instance.OnSelectPlate;
                @SelectPlate.canceled += instance.OnSelectPlate;
                @Scroll.started += instance.OnScroll;
                @Scroll.performed += instance.OnScroll;
                @Scroll.canceled += instance.OnScroll;
                @RotatebyHand.started += instance.OnRotatebyHand;
                @RotatebyHand.performed += instance.OnRotatebyHand;
                @RotatebyHand.canceled += instance.OnRotatebyHand;
                @RotatePressed.started += instance.OnRotatePressed;
                @RotatePressed.performed += instance.OnRotatePressed;
                @RotatePressed.canceled += instance.OnRotatePressed;
                @FocusOut.started += instance.OnFocusOut;
                @FocusOut.performed += instance.OnFocusOut;
                @FocusOut.canceled += instance.OnFocusOut;
            }
        }
    }
    public FocusInteractionActions @FocusInteraction => new FocusInteractionActions(this);

    // PauseMenu
    private readonly InputActionMap m_PauseMenu;
    private IPauseMenuActions m_PauseMenuActionsCallbackInterface;
    private readonly InputAction m_PauseMenu_EndPause;
    public struct PauseMenuActions
    {
        private @PlayerSystem m_Wrapper;
        public PauseMenuActions(@PlayerSystem wrapper) { m_Wrapper = wrapper; }
        public InputAction @EndPause => m_Wrapper.m_PauseMenu_EndPause;
        public InputActionMap Get() { return m_Wrapper.m_PauseMenu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PauseMenuActions set) { return set.Get(); }
        public void SetCallbacks(IPauseMenuActions instance)
        {
            if (m_Wrapper.m_PauseMenuActionsCallbackInterface != null)
            {
                @EndPause.started -= m_Wrapper.m_PauseMenuActionsCallbackInterface.OnEndPause;
                @EndPause.performed -= m_Wrapper.m_PauseMenuActionsCallbackInterface.OnEndPause;
                @EndPause.canceled -= m_Wrapper.m_PauseMenuActionsCallbackInterface.OnEndPause;
            }
            m_Wrapper.m_PauseMenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @EndPause.started += instance.OnEndPause;
                @EndPause.performed += instance.OnEndPause;
                @EndPause.canceled += instance.OnEndPause;
            }
        }
    }
    public PauseMenuActions @PauseMenu => new PauseMenuActions(this);

    // StoneTable
    private readonly InputActionMap m_StoneTable;
    private IStoneTableActions m_StoneTableActionsCallbackInterface;
    private readonly InputAction m_StoneTable_Up;
    private readonly InputAction m_StoneTable_Right;
    private readonly InputAction m_StoneTable_Down;
    private readonly InputAction m_StoneTable_Left;
    public struct StoneTableActions
    {
        private @PlayerSystem m_Wrapper;
        public StoneTableActions(@PlayerSystem wrapper) { m_Wrapper = wrapper; }
        public InputAction @Up => m_Wrapper.m_StoneTable_Up;
        public InputAction @Right => m_Wrapper.m_StoneTable_Right;
        public InputAction @Down => m_Wrapper.m_StoneTable_Down;
        public InputAction @Left => m_Wrapper.m_StoneTable_Left;
        public InputActionMap Get() { return m_Wrapper.m_StoneTable; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(StoneTableActions set) { return set.Get(); }
        public void SetCallbacks(IStoneTableActions instance)
        {
            if (m_Wrapper.m_StoneTableActionsCallbackInterface != null)
            {
                @Up.started -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnUp;
                @Up.performed -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnUp;
                @Up.canceled -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnUp;
                @Right.started -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnRight;
                @Down.started -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnDown;
                @Down.performed -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnDown;
                @Down.canceled -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnDown;
                @Left.started -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnLeft;
                @Left.performed -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnLeft;
                @Left.canceled -= m_Wrapper.m_StoneTableActionsCallbackInterface.OnLeft;
            }
            m_Wrapper.m_StoneTableActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Up.started += instance.OnUp;
                @Up.performed += instance.OnUp;
                @Up.canceled += instance.OnUp;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Down.started += instance.OnDown;
                @Down.performed += instance.OnDown;
                @Down.canceled += instance.OnDown;
                @Left.started += instance.OnLeft;
                @Left.performed += instance.OnLeft;
                @Left.canceled += instance.OnLeft;
            }
        }
    }
    public StoneTableActions @StoneTable => new StoneTableActions(this);
    public interface IGroundMovementActions
    {
        void OnHorizontalMovement(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnMouseX(InputAction.CallbackContext context);
        void OnMouseY(InputAction.CallbackContext context);
        void OnCameraMove(InputAction.CallbackContext context);
        void OnSprint(InputAction.CallbackContext context);
        void OnZoom(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
    }
    public interface IInteractionActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnSwitchInventory(InputAction.CallbackContext context);
        void OnDetection(InputAction.CallbackContext context);
    }
    public interface IFocusInteractionActions
    {
        void OnMousePos(InputAction.CallbackContext context);
        void OnSelectPlate(InputAction.CallbackContext context);
        void OnScroll(InputAction.CallbackContext context);
        void OnRotatebyHand(InputAction.CallbackContext context);
        void OnRotatePressed(InputAction.CallbackContext context);
        void OnFocusOut(InputAction.CallbackContext context);
    }
    public interface IPauseMenuActions
    {
        void OnEndPause(InputAction.CallbackContext context);
    }
    public interface IStoneTableActions
    {
        void OnUp(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
        void OnLeft(InputAction.CallbackContext context);
    }
}
