namespace ShiftingMaze.Editor
{
    using ShiftingMaze.Puzzle.Strcuture;
    using System.Collections.Generic;
    using System;
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(PuzzleLayout))]
    public class PuzzleMatrixDrawer : Editor
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        SerializedProperty structures;
        SerializedProperty width;
        SerializedProperty height;

        int arrayWidth, arrayHeight;
        float scroll = 0f;

        private List<Node> nodes;
        private Rect nodeContainer;
        private bool hasPit = false;
        private bool hasStart = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            GetProperties();
            UpdateArraySize();
            AssignNodes();
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            // Draw the width and height into the inspector.
            EditorGUILayout.PropertyField(width);
            EditorGUILayout.PropertyField(height);

            // Check if the dimensions have changed.
            if (EditorGUI.EndChangeCheck())
            {
                UpdateArraySize(width.intValue, height.intValue);
                serializedObject.ApplyModifiedProperties();
            }

            // Create the area for the nodes.
            nodeContainer = EditorGUILayout.GetControlRect(false, arrayWidth > 0f ? arrayHeight * Node.SIZE.y : 0f);
            // Draw the nodes.
            UpdateNodes(nodeContainer, Event.current);

            float maxWidth = arrayWidth * Node.SIZE.x;
            scroll = GUILayout.HorizontalScrollbar(scroll, nodeContainer.width, 0f, nodeContainer.width >= maxWidth ? 0f : maxWidth);
        }

        /// <summary>
        /// Change the size of the grid.
        /// </summary>
        /// <param name="width">The new width of the grid.</param>
        /// <param name="height">The new height of the grid.</param>
        private void UpdateArraySize(int width, int height)
        {
            bool isCleard = arrayWidth > 0 && arrayHeight > 0 && (width == 0 || height == 0);
            bool isNew = width > 0 && height > 0 && (arrayWidth == 0 || arrayHeight == 0);

            int updatestate = (width * height).CompareTo(arrayWidth * arrayHeight) + (isNew ? 1 : 0) - (isCleard ? 1 : 0);
            if (updatestate != 0)
            {
                if (updatestate > 0)
                { 
                    AddNewTiles(width, height);
                }
                else
                {
                    RemoveExistingTiles(width, height);
                }
            }
            
            arrayWidth = width;
            arrayHeight = height;
        }

        /// <summary>
        /// Update the size of the array by adding tiles.
        /// </summary>
        /// <param name="width">The new width of the grid.</param>
        /// <param name="height">The new height of the grid.</param>
        private void AddNewTiles(int width, int height)
        {
            int arrayWidth = this.arrayWidth;
            int arrayHeight = this.arrayHeight;
            this.arrayWidth = width;
            this.arrayHeight = height;

            // Iterate over the grid.
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Check if the position needs to be updated.
                    if (x > arrayWidth - 1 || y > arrayHeight - 1)
                    {
                        // Create a new node for the position.
                        Node node = new Node(x, y, serializedObject.targetObject, MoveNode);
                        // Add a listener for the tile.
                        node.OnTileUpdated += UpdateArrayField;
                        // Add the node to the node list.
                        nodes.Insert(x + y * width, node);

                        // Increase the array size. 
                        structures.InsertArrayElementAtIndex(x + y * width);
                        // Apply the change.
                        serializedObject.ApplyModifiedProperties();

                        // Update the content of the array.
                        UpdateArrayField(node.Structure, x, y);
                    }
                }
            }
            // Apply the changes.
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Resizes the grid by removing tiles from it.
        /// </summary>
        /// <param name="width">The new width.</param>
        /// <param name="height">The new height.</param>
        private void RemoveExistingTiles(int width, int height)
        {
            int arrayWidth = this.arrayWidth;
            int arrayHeight = this.arrayHeight;
            this.arrayWidth = width;
            this.arrayHeight = height;

            // Iterate over all positions.
            for (int y = arrayHeight - 1; y >= 0; y--)
            {
                for (int x = arrayWidth - 1; x >= 0; x--)
                {
                    // Check if a node in the removed area.
                    if (x > width - 1 || y > height - 1)
                    {
                        // Clear the nodes content.
                        nodes[x + y * arrayWidth].EmptyNode();
                        // Remove the node from the node list.
                        nodes.RemoveAt(x + y * arrayWidth);
                        // Move the tile in the array to the last position.
                        structures.MoveArrayElement(x + y * arrayWidth, structures.arraySize - 1);
                        // Decrease the array size and remove the last position.
                        structures.arraySize--;
                        // Apply the changes.
                        serializedObject.ApplyModifiedProperties();
                    }
                }
            }
        }

        /// <summary>
        /// Update the content of the array at the given coordinates.
        /// </summary>
        /// <param name="tile">The new content at the coordinates.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        private void UpdateArrayField(TileStructure tile, int x, int y)
        {
            // Get the element at the given coordinates and replace it.
            structures.GetArrayElementAtIndex(x + y * arrayWidth).objectReferenceValue = tile;
            // Apply the changes.
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Get the propertie managed by the custom inspector.
        /// </summary>
        private void GetProperties()
        {
            structures = serializedObject.FindProperty("structures");
            width = serializedObject.FindProperty("width");
            height = serializedObject.FindProperty("height");
        }

        /// <summary>
        /// Update the know array size.
        /// </summary>
        private void UpdateArraySize()
        {
            arrayWidth = width.intValue;
            arrayHeight = height.intValue;
        }

        /// <summary>
        /// Create nodes for all positions.
        /// </summary>
        private void AssignNodes()
        {
            nodes = new List<Node>();
            TileStructure tile;
            // Iterate over all positions.
            for (int y = 0; y < arrayHeight; y++)
            {
                for (int x = 0; x < arrayWidth; x++)
                {
                    // Get the content of the position.
                    tile = structures.GetArrayElementAtIndex(x + y * arrayWidth).objectReferenceValue as TileStructure;
                    // Create a node with the content.
                    Node node = new Node(tile, x, y, serializedObject.targetObject, MoveNode);
                    // Add the listener for the content update.
                    node.OnTileUpdated += UpdateArrayField;
                    // Add the node to the list.
                    nodes.Add(node);
                }
            }
        }

        // Move the node to a new position.
        private void MoveNode(CardinalPoint direction, int x, int y)
        {
            // Check if there is enought space.
            bool isInside;
            switch (direction)
            {
                default: isInside = false; break;
                case CardinalPoint.North: isInside = y < arrayHeight - 1; break;
                case CardinalPoint.East: isInside = x < arrayWidth - 1; break;
                case CardinalPoint.South: isInside = y >= 1; break;
                case CardinalPoint.West: isInside = x >= 1; break;
            }
            if (isInside)
            {
                // Get the direction to move.
                Vector2Int dir = direction.ToVector2Int();
                // Calculate the index of the targeted position.
                int originIndex = x + y * arrayWidth;
                // Get the index of the position to move to.
                int targetIndex = x + dir.x + (y + dir.y) * arrayWidth;

                // Switch the content of the two positions.
                TileStructure tile = nodes[originIndex].Structure;
                nodes[originIndex].AssignTile(nodes[targetIndex].Structure);
                nodes[targetIndex].AssignTile(tile);
            }
        }

        /// <summary>
        /// Updates all nodes.
        /// </summary>
        /// <param name="container">The container wrapping the nodes.</param>
        /// <param name="e">The event to handle.</param>
        private void UpdateNodes(Rect container, Event e)
        {
            // Iterate ofer all nodes.
            foreach (Node node in nodes)
            {
                // Draw the node.
                node.Draw(container.position + new Vector2(node.x, arrayHeight - node.y - 1) * Node.SIZE + Vector2.left * scroll);
                // Process the nodes events.
                if (e != null && node.ProcessEvent(e))
                {
                    e = null;
                }
            }
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// A class holding all the information of one position in the grid.
        /// </summary>
        private class Node
        {
            // Variables --------------------------------------------------------------------------

            /// <summary>
            /// The x coordinate of the node.
            /// </summary>
            public int x { get; private set; }
            /// <summary>
            /// The y coordinate of the node.
            /// </summary>
            public int y { get; private set; }

            private GUIStyle style;
            /// <summary>
            /// The tile stored in the node.
            /// </summary>
            public TileStructure Structure { get; protected set; }
            private UnityEngine.Object parent;

            protected Rect rect;

            private Connection[] connections = new Connection[]
            {
                new Connection(CardinalPoint.North, SIZE.y * 0.5f),
                new Connection(CardinalPoint.East, SIZE.x * 0.5f),
                new Connection(CardinalPoint.South, SIZE.y * 0.5f),
                new Connection(CardinalPoint.West, SIZE.x * 0.5f)
            };

            /// <summary>
            /// Called when the content of the node changes.
            /// </summary>
            public event Action<TileStructure, int, int> OnTileUpdated;

            private Action<CardinalPoint, int, int> move;

            /// <summary>
            /// The size of a node.
            /// </summary>
            public static readonly Vector2 SIZE = Vector2.one * 50f;

            private static readonly (GUIStyle moveable, GUIStyle inmoveable) fieldStyle = TileBaseStyles("node2.png");
            private static readonly (GUIStyle moveable, GUIStyle inmoveable) pitStyle = TileBaseStyles("node1.png");
            private static readonly (GUIStyle moveable, GUIStyle inmoveable) emptyStyle = TileBaseStyles("node0.png");
            private static readonly (GUIStyle moveable, GUIStyle inmoveable) startStyle = TileBaseStyles("node3.png");
            private static readonly (GUIStyle moveable, GUIStyle inmoveable) endStyle = TileBaseStyles("node6.png");


            // Constructors -----------------------------------------------------------------------

            /// <summary>
            /// Creates a new node.
            /// </summary>
            /// <param name="tile">The tile stored in the node.</param>
            /// <param name="x">The x coordinate of the node.</param>
            /// <param name="y">The y coordinate of the node.</param>
            /// <param name="parent">The parent object storing the tile.</param>
            /// <param name="move">The action to perfrom to move the tile.</param>
            public Node(TileStructure tile, int x, int y, UnityEngine.Object parent, Action<CardinalPoint, int, int> move)
            {
                Structure = tile;
                this.parent = parent;
                this.x = x;
                this.y = y;
                this.move = move;

                UpdateStyle();
            }
            /// <summary>
            /// Creates a new node.
            /// </summary>
            /// <param name="x">The x coordinate of the node.</param>
            /// <param name="y">The y coordinate of the node.</param>
            /// <param name="parent">The parent object storing the tile.</param>
            /// <param name="tileType">The type of the tile.</param>
            /// <param name="move">The action to perfrom to move the tile.</param>
            public Node(int x, int y, UnityEngine.Object parent, Type tileType, Action<CardinalPoint, int, int> move) 
                : this(GetTile(tileType, parent, x, y), x, y, parent, move) { }
            /// <summary>
            /// Creates a new node.
            /// </summary>
            /// <param name="x">The x coordinate of the node.</param>
            /// <param name="y">The y coordinate of the node.</param>
            /// <param name="parent">The parent object storing the tile.</param>
            /// <param name="move">The action to perfrom to move the tile.</param>
            public Node(int x, int y, UnityEngine.Object parent, Action<CardinalPoint, int, int> move) 
                : this(x, y, parent, null, move) { }


            // Methods ----------------------------------------------------------------------------

            /// <summary>
            /// Draw the node.
            /// </summary>
            /// <param name="position">The position to draw the node at.</param>
            public void Draw(Vector2 position)
            {
                rect = new Rect(position, SIZE);
                GUI.Box(rect, $"[{x}/{y}]", style);

                if (Structure is BorderedStructure)
                {
                    foreach (Connection connection in connections)
                    { 
                        connection.Draw(position + SIZE * 0.5f, Structure as BorderedStructure);
                    }
                }
            }

            /// <summary>
            /// Process the nodes events.
            /// </summary>
            /// <param name="e">The event to handle.</param>
            /// <returns>True if the event was used by the node.</returns>
            public bool ProcessEvent(Event e)
            {
                // Check if any PuzzleField events need handling. 
                if (Structure is BorderedStructure)
                {
                    // Iterate over the connections and handle their events.
                    foreach (Connection connection in connections)
                    {
                        if (connection.ProcessEvent(e, rect.position + SIZE * 0.5f))
                        {
                            return true;
                        }
                    }
                }
                // Check the type of event.
                switch (e.type)
                {
                    case EventType.MouseDown:
                        if (e.button == 1)
                        {
                            if (rect.Contains(e.mousePosition))
                            {
                                ProvessContextMenu(e.mousePosition);
                                return true;
                            }
                        }
                        break;
                }
                return false;
            }

            /// <summary>
            /// Create a context menu.
            /// </summary>
            /// <param name="mousePosition">the mouse position.</param>
            protected void ProvessContextMenu(Vector2 mousePosition)
            {
                GenericMenu genericMenu = new GenericMenu();
                genericMenu.AddItem(new GUIContent("Edit Type/Empty"), Structure == null, () => EmptyNode(true));
                genericMenu.AddItem(new GUIContent("Edit Type/Field"), Structure is FieldStructure, () => UpdateTile<FieldStructure>());
                genericMenu.AddItem(new GUIContent("Edit Type/Pit"), Structure is PitStructure, () => UpdateTile<PitStructure>());
                genericMenu.AddItem(new GUIContent("Edit Type/Start"), Structure is StartStructure, () => UpdateTile<StartStructure>());

                genericMenu.AddItem(new GUIContent("Move/North"), false, () => move?.Invoke(CardinalPoint.North, x, y));
                genericMenu.AddItem(new GUIContent("Move/East"), false, () => move?.Invoke(CardinalPoint.East, x, y));
                genericMenu.AddItem(new GUIContent("Move/South"), false, () => move?.Invoke(CardinalPoint.South, x, y));
                genericMenu.AddItem(new GUIContent("Move/West"), false, () => move?.Invoke(CardinalPoint.West, x, y));
                genericMenu.ShowAsContext();
            }

            /// <summary>
            /// Clear the nodes content.
            /// </summary>
            /// <param name="updateStyle">True if the style should be updated.</param>
            private void EmptyNode(bool updateStyle)
            {
                if (Structure != null)
                {
                    AssetDatabase.RemoveObjectFromAsset(Structure);
                    AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Structure));
                    AssetDatabase.SaveAssets();
                    Structure = null;
                    if (updateStyle)
                    {
                        UpdateStyle();
                    }
                    OnTileUpdated?.Invoke(null, x, y);
                }
            }
            /// <summary>
            /// Clear the nodes content.
            /// </summary>
            public void EmptyNode() => EmptyNode(true);

            /// <summary>
            /// Update the tiles type.
            /// </summary>
            /// <typeparam name="T">The type of the new tile.</typeparam>
            private void UpdateTile<T>()
            {
                // Remove the current content.
                EmptyNode(false);
                // Create a new tile.
                Structure = GetTile(typeof(T), parent, x, y);
                // Update the style.
                UpdateStyle();
                // Ibvoke the event to signal that the content has changed.
                OnTileUpdated?.Invoke(Structure, x, y);
            }

            /// <summary>
            /// Create a new tile.
            /// </summary>
            /// <param name="tileType">The type of the new tile.</param>
            /// <param name="parent">The parent element storing the tiles data.</param>
            /// <param name="x">The x position of the tile.</param>
            /// <param name="y">The y position of the tile.</param>
            /// <returns>The new tile.</returns>
            private static TileStructure GetTile(Type tileType, UnityEngine.Object parent, int x, int y)
            {
                if (tileType == null)
                {
                    return null;
                }
                // Create the tile.
                TileStructure tile = CreateInstance(tileType) as TileStructure;
                // Change the tiles name.
                tile.name = GetName(tileType, parent, x, y);
                // Add the tiles data to the parent.
                AssetDatabase.AddObjectToAsset(tile, parent);
                // Save the change.
                AssetDatabase.SaveAssets();

                // Signal that the object have changed.
                EditorUtility.SetDirty(tile);
                EditorUtility.SetDirty(parent);

                // Return the new tile.
                return tile;
            }

            /// <summary>
            /// Get the name of the node/tile.
            /// </summary>
            /// <param name="tileType">The type of the tile.</param>
            /// <param name="parent">The parent of the tile.</param>
            /// <param name="x">The x coordinate of the tile.</param>
            /// <param name="y">The y coordinate of the tile.</param>
            /// <returns>The name of the node/tile.</returns>
            private static string GetName(Type tileType, UnityEngine.Object parent, int x, int y) 
                => $"{parent.name}_{tileType.Name}[{x}/{y}]";
            /// <summary>
            /// Get the name of the node/tile.
            /// </summary>
            /// <returns>The name of the node/tile.</returns>
            private string GetName() => GetName(Structure.GetType(), parent, x, y);

            /// <summary>
            /// Updates the used style for the node.
            /// </summary>
            private void UpdateStyle()
            {
                if (Structure == null)
                {
                    style = emptyStyle.inmoveable;
                }
                else if (Structure is FieldStructure)
                {
                    style = (Structure as FieldStructure).Moveable ? fieldStyle.moveable : fieldStyle.inmoveable;
                }
                else if (Structure is PitStructure)
                {
                    style = pitStyle.moveable;
                }
                else if (Structure is StartStructure)
                {
                    style = (Structure as StartStructure).Moveable ? startStyle.moveable : startStyle.inmoveable;
                }
                else
                {
                    style = new GUIStyle();
                }
            }

            /// <summary>
            /// Create a <see cref="GUIStyle"/> for the node.
            /// </summary>
            /// <param name="texturePath">The path to the texture to use as background.</param>
            /// <returns>The style with the given texture.</returns>
            private static (GUIStyle, GUIStyle) TileBaseStyles(string texturePath)
            {
                return (TileBaseStyle("builtin skins/lightskin/images/" + texturePath), TileBaseStyle("builtin skins/darkskin/images/" + texturePath));
            }
            private static GUIStyle TileBaseStyle(string texturePath)
            {
                GUIStyle style = new GUIStyle();
                style.alignment = TextAnchor.MiddleCenter;
                style.normal.background = EditorGUIUtility.Load(texturePath) as Texture2D;
                return style;
            }

            /// <summary>
            /// Assigns a tile to the node.
            /// </summary>
            /// <param name="tile">The new content of the node.</param>
            public void AssignTile(TileStructure tile)
            {
                Structure = tile;
                if (tile != null)
                {
                    tile.name = GetName();
                    EditorUtility.SetDirty(tile);
                }
                UpdateStyle();
                EditorUtility.SetDirty(parent);
                OnTileUpdated?.Invoke(tile, x, y);
            }

            private class Connection
            {
                Rect rect;
                CardinalPoints direction;
                GUIStyle on = TileBaseStyle("builtin skins/darkskin/images/node6.png");
                GUIStyle off = TileBaseStyle("builtin skins/darkskin/images/node0.png");

                public Connection(CardinalPoint direction, float offset)
                {
                    switch (direction)
                    {
                        case CardinalPoint.North: this.direction = CardinalPoints.North; break;
                        case CardinalPoint.East: this.direction = CardinalPoints.East; break;
                        case CardinalPoint.South: this.direction = CardinalPoints.South; break;
                        case CardinalPoint.West: this.direction = CardinalPoints.West; break;
                    }
                    rect = new Rect
                    (
                        direction == CardinalPoint.East ? offset - 15f : direction == CardinalPoint.West ? 5f - offset : - 10f,
                        direction == CardinalPoint.North ? 5f - offset : direction == CardinalPoint.South ? offset - 15f : - 10f,
                        direction == CardinalPoint.East || direction == CardinalPoint.West ? 10f : 20f,
                        direction == CardinalPoint.North || direction == CardinalPoint.South ? 10f : 20f
                    );
                }

                public void Draw(Vector2 position, BorderedStructure field)
                {
                    GUI.Box(new Rect(position + rect.position, rect.size), "", field.Entries.HasFlag(direction) ? on : off);
                }

                public bool ProcessEvent(Event e, Vector2 position)
                {
                    return false;
                    switch (e.type)
                    {
                        case EventType.MouseDown:
                            if (e.button == 1)
                            {
                                if (rect.Contains(e.mousePosition - position))
                                {
                                    ProvessContextMenu(e.mousePosition);
                                    return true;
                                }
                            }
                            break;
                    }
                    return false;
                }

                private void ProvessContextMenu(Vector2 mousePosition)
                {
                    GenericMenu genericMenu = new GenericMenu();
                    genericMenu.AddItem(new GUIContent("Activate"), false, () => { });
                    genericMenu.AddItem(new GUIContent("Deactivate"), false, () => { });
                    genericMenu.ShowAsContext();
                }
            }
        }
    }
}
