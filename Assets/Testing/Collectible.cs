using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : InteractableBase
{

    public override void OnInteract()
    {
        Inventory.instance.AddCollectible(this);
    }
}
