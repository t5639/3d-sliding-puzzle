using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace bugiflo {

    public class Options_Controls : MonoBehaviour
    {
        public Slider sensitSlider;
        public bool controlMode;
        public CameraRotationController cam;

        public Interact interact;
        public TMP_Text text;
        public TMP_Text text2;

        public void SetAll()
        {
            SetSensitivity();
            SetPuzzleControl();
        }

        public void SetSensitivity()
        {
            if (cam != null)
            {
                cam.rotationSpeed = sensitSlider.value;
            }
        }

        public void ChangePuzzleControl()
        {
            controlMode = !controlMode;

            SetPuzzleControl();
        }

        public void SetPuzzleControl()
        {
            if (interact != null)
            {
                interact.puzzleMode = controlMode;
            }

            text.text = "";
            text2.text = "move Stone";
            if (controlMode)
            {
                text.text = "X";
                text2.text = "move Hole";
            }
        }



    }
}
