using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Lock : InteractableBase
{
    public int lockId;
    int tableCount;
    public GameObject tableFull;

    public UnityEvent OnUnlock;

    public override void OnInteract()
    {
        int keyId = Inventory.instance.GetEquipped().GetComponent<Key>().keyId;

        if (keyId == lockId)
        {
            OnUnlock.Invoke();
            if (keyId >= 30) //stone ids are >= 30
            {
                AddPlate();
            }
            else
            {
                Unlock();
            }
        } //else wrong key
    }

    void Unlock()
    {
        Debug.Log("unlocked the door");
        Inventory.instance.RemoveItem(Inventory.instance.GetEquipped().GetComponent<Key>());
    }

    void AddPlate()
    {
        tableCount++;
        lockId++;

        Debug.Log("Animation abspielen, Funken fliegen?");

        if (tableCount == 1)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else if (tableCount == 2)
        {
            transform.GetChild(1).gameObject.SetActive(true);
        }
        else if (tableCount == 3)
        {
            transform.gameObject.SetActive(false);
            tableFull.gameObject.SetActive(true);

        }

        Inventory.instance.RemoveItem(Inventory.instance.GetEquipped().GetComponent<Key>());

        
    }


}
