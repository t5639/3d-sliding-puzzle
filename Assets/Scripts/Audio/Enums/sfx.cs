public enum sfx 
{
	lever_Pull = 0,
	lever_Push = 1,
	grab_Metall = 2,
	putdown_Metall = 3,
	grab_Stone = 4,
	putdown_Stone = 5,
	putdown_smallWood = 6,
	Door_open = 7,
	move_Tile1 = 8,
	move_Tile2 = 9,
	move_TileImpact = 10,

}
