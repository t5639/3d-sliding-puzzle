namespace ShiftingMaze.Player
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.InputSystem;

    public class ClimbingController : ControllerBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The body representation of the object.")]
        private CharacterController body = default;

        [SerializeField, Tooltip("The motion of the step.")]
        private AnimationCurve climbMotion = new AnimationCurve(new Keyframe(), new Keyframe(0.2f, 1f));

        [SerializeField, Tooltip("The mottion to perform for the step onto the climbable.")]
        private AnimationCurve mountMotion = new AnimationCurve(new Keyframe(), new Keyframe(0.05f, 1f));

        [SerializeField, Tooltip("The motion to perform to get off the climbable.")]
        private AnimationCurve dismountMotion = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Climbable.Agent agent;
        private (Vector3 position, Vector3 normal) prev, next;
        private float time = 0f;
        private float stepDuration = 1f;
        private float climbDirection = 0f;

        private const float BODY_DISTANCE = 0.001f;

        private PlayerSystem.GroundMovementActions Ground => controller.Inputs.GroundMovement;

        private Coroutine activeMove;


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // Monobehaviour --------------------------------------------------------------------------

        protected override void OnEnable()
        {
            base.OnEnable();

            stepDuration = climbMotion[climbMotion.length - 1].time;

            Ground.HorizontalMovement.performed += Move;
            Move(Ground.HorizontalMovement.ReadValue<Vector2>());
        }

        protected override void OnDisable()
        {
            Ground.HorizontalMovement.performed -= Move;

            climbDirection = 0f;

            agent = null;
            StopActiveMove();

            base.OnDisable();
        }

        // Mount ----------------------------------------------------------------------------------

        /// <summary>
        /// Mounts the player to a ladder.
        /// </summary>
        /// <param name="climbable">The object to climb on.</param>
        public void Mount(Climbable climbable)
        {
            if (agent == null)
            {
                agent = climbable.GetAgent(transform.position, GetOnWall);
                controller.SetControllerActive(this);
            }
        }

        // Input handling -------------------------------------------------------------------------

        /// <summary>
        /// Moves the character in a direction.
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        private void Move(Vector2 direction)
        {
            climbDirection = direction.y;
            if (activeMove == null)
            {
                GetNextMove();
            }
        }
        /// <summary>
        /// Moves the character in a direction.
        /// </summary>
        /// <param name="ctx">The input context.</param>
        private void Move(InputAction.CallbackContext ctx) => Move(ctx.ReadValue<Vector2>());

        // Next move ------------------------------------------------------------------------------

        /// <summary>
        /// Starts the next move.
        /// </summary>
        private void GetNextMove()
        {
            if (climbDirection > 0f)
            {
                agent.MoveUp(SetNextMove, GetOffWall);
            }
            else if (climbDirection < 0f)
            {
                agent.MoveDown(SetNextMove, GetOffWall);
            }
            else
            {
                time = 0f;
                activeMove = null;
            }
        }

        /// <summary>
        /// Cancels the active movement.
        /// </summary>
        private void StopActiveMove()
        {
            if (activeMove != null)
            {
                StopCoroutine(activeMove);
                activeMove = null;
            }
        }

        // Wall interactios -----------------------------------------------------------------------

        /// <summary>
        /// Moves the character to the climbable.
        /// </summary>
        /// <param name="orientation">The orientation of the climbable.</param>
        private void GetOnWall((Vector3 position, Vector3 normal) orientation)
        {
            StopActiveMove();
            prev = (transform.position, -transform.forward);
            next = (orientation.position + orientation.normal * (body.radius + BODY_DISTANCE), orientation.normal);
            activeMove = StartCoroutine(DoMoveOnClimbable());
        }
        /// <summary>
        /// Moves the character off the climbable.
        /// </summary>
        /// <param name="orientation">The orientation of the climbable.</param>
        private void GetOffWall((Vector3 position, Vector3 normal) orientation)
        {
            StopActiveMove();
            next = orientation;
            activeMove = StartCoroutine(DoMoveOffClimbable());
        }

        /// <summary>
        /// Moves the character to the next point.
        /// </summary>
        /// <param name="orientation">The orientation of the next point.</param>
        private void SetNextMove((Vector3 position, Vector3 normal) orientation)
        {
            next = (orientation.position + orientation.normal * (body.radius + BODY_DISTANCE), orientation.normal);
            time -= stepDuration;
            activeMove = StartCoroutine(DoMove());
        }

        // Movement Updates -----------------------------------------------------------------------

        /// <summary>
        /// Moves the character to the climbable over time.
        /// </summary>
        private IEnumerator DoMoveOnClimbable()
        {
            float time = 0f;
            float speed = mountMotion[mountMotion.length - 1].time;
            float distance = Vector3.Distance(prev.position, next.position);
            float duration = speed * distance;
            while (time < duration)
            {
                yield return new WaitForFixedUpdate();
                time += Time.fixedDeltaTime;
                UpdatePosition(prev, next, mountMotion, time / distance);
            }
            prev = next;
            GetNextMove();
        }

        /// <summary>
        /// Moves the character off the climbable over time.
        /// </summary>
        private IEnumerator DoMoveOffClimbable()
        {
            float time = 0f;
            float speed = dismountMotion[dismountMotion.length - 1].time;
            float distance = Vector3.Distance(prev.position, next.position);
            float duration = speed * distance;
            while (time < duration)
            {
                yield return new WaitForFixedUpdate();
                time += Time.fixedDeltaTime;
                UpdatePosition(prev, next, dismountMotion, time / distance);
            }
            prev = next = (Vector3.zero, Vector3.zero);
            activeMove = null;
            controller.ResetController();
        }

        /// <summary>
        /// Moves the climbable to the next position over time.
        /// </summary>
        private IEnumerator DoMove()
        {
            while (time < stepDuration)
            {
                yield return new WaitForFixedUpdate();
                time += Time.fixedDeltaTime;
                UpdatePosition(prev, next, climbMotion, time);
            }
            prev = next;
            GetNextMove();
        }

        // Utility --------------------------------------------------------------------------------

        /// <summary>
        /// Moves the character to the desirec position.
        /// </summary>
        /// <param name="prev">The previous orientation.</param>
        /// <param name="next">The target orientation.</param>
        /// <param name="motion">The motion to perform between the orientations.</param>
        /// <param name="time">The current time since the start of the move.</param>
        private void UpdatePosition((Vector3 position, Vector3 normal) prev, (Vector3 position, Vector3 normal) next, AnimationCurve motion, float time)
        {
            float lerp = motion.Evaluate(time);
            transform.position = Vector3.Lerp(prev.position, next.position, lerp);
            transform.LookAt(transform.position + transform.forward.SelectY() + Vector3.Lerp(-prev.normal, -next.normal, lerp).IgnoreY());
            /*Vector3 euler = transform.eulerAngles;
            euler.y = Mathf.LerpAngle
            (
                Vector3.SignedAngle(Vector3.forward, -prev.normal, Vector3.up),
                Vector3.SignedAngle(Vector3.forward, -next.normal, Vector3.up),
                lerp
            );
            transform.eulerAngles = euler;
            transform.rotation = Quaternion.Euler(euler);
            */
        }
    }
}
