namespace ShiftingMaze.Puzzle.Management
{
    using UnityEngine;

    public class PuzzleBuilder : PuzzleMatrix
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script managing the building of the tiles.")]
        private PuzzleTileBuilder tileBuilder = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Vector2 tileSize;
        private PuzzleTile[] tiles;
        /// <inheritdoc/>
        protected override PuzzleTile[] Tiles
        {
            get => tiles;
            set => tiles = value;
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            tileSize = new Vector2(size.x / layout.Width, size.y / layout.Height);
            Vector3 scale = new Vector3(size.x / (tileBuilder.TileSize.x * layout.Width), 1f, size.y / (tileBuilder.TileSize.y * layout.Height));
            scale.y = (scale.x + scale.z) * 0.5f;
            tiles = new PuzzleTile[layout.Width * layout.Height];
            foreach (var structureWithCoordinates in layout.EachStructureWithCoordinates())
            {
                int index = structureWithCoordinates.cooridnates.x + structureWithCoordinates.cooridnates.y * (int)layout.Width;
                tiles[index] = tileBuilder.BuildTile(structureWithCoordinates.structure, GetBorders(structureWithCoordinates.cooridnates));
                tiles[index].transform.SetParent(Container);
                tiles[index].transform.localPosition = CoordinatesToPosition(structureWithCoordinates.cooridnates);
                tiles[index].transform.localScale = scale;
                tiles[index].transform.localRotation = Quaternion.identity;
                if (Container.rotation != Quaternion.identity)
                {
                    tiles[index].GetComponentInChildren<TileSignals>()?.UpdatePointPositions();
                }
            }
            FinishSetup();
        }

        private Vector3 CoordinatesToPosition(Vector2Int coordinates)
        {
            return new Vector3(coordinates.x * tileSize.x, 0f, coordinates.y * tileSize.y);
        }

        private CardinalPoints GetBorders(Vector2Int cooridinates)
        {
            CardinalPoints borders = CardinalPoints.None;

            if (cooridinates.x == 0)
            {
                borders |= CardinalPoints.West;
            }
            if (cooridinates.y == 0)
            {
                borders |= CardinalPoints.South;
            }
            if (cooridinates.x == layout.Width - 1)
            {
                borders |= CardinalPoints.East;
            }
            if (cooridinates.y == layout.Height - 1)
            {
                borders |= CardinalPoints.North;
            }

            return borders;
        }
    }
}
