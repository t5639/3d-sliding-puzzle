using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    [Header("Basics")]
    public static Inventory instance;
    public List<InteractableInvBase> itemList;
    int capacity = 5;
    int collectibles = 0;
    int equipped = -1;

    [Header("UI")]
    GameObject collContainer; //collectibles

    [Header("ItemPrefabs")]
    public GameObject itemCamera;
    GameObject itemInHand;

    public GameObject leverPre;
    public GameObject brokenTablePre1;
    public GameObject brokenTablePre2;
    public GameObject brokenTablePre3;

    public UnityEvent OnItemSwitch, OnHandSwitch;

    private void Awake()
    {
        instance = this;
        itemList = new List<InteractableInvBase>();
    }


    public bool AddItem(InteractableInvBase item)
    {
        if (itemList.Count < capacity)
        {
            itemList.Add(item);
            UpdateUI();
            return true;
        } else
        {
            Debug.Log("Too many items");
            return false;
        }

    }

    public void RemoveItem(InteractableInvBase item)
    {
        if (itemList.Contains(item))
        {
            itemList.Remove(item);
            Destroy(item.gameObject);
        }

        SwitchEquipped();
    }

    public void SwitchEquipped()
    {
        if (equipped >= itemList.Count - 1)
        {
            OnHandSwitch.Invoke();
            equipped = -1;
        }
        else
        {
            OnItemSwitch.Invoke();
            equipped++;
        }

        UpdateUI();
    }

    public InteractableInvBase GetEquipped()
    {
        if (equipped == -1)
        {
            return null;
        }

        return itemList[equipped];
    }

    void UpdateUI()
    {
        Inventory_UI.instance.setImages();
        if (itemCamera.transform.childCount != 0)
        {
            itemInHand.transform.GetComponent<Animator>().SetBool("itemEntry", false);
        }

        //handheld item
        if (GetEquipped() != null)
        {
            if (GetEquipped().GetComponent<Lever>() != null)
            {
                itemInHand = Instantiate(leverPre, itemCamera.transform);
            }
            if (GetEquipped().GetComponent<Key>() != null)
            {
                if (GetEquipped().GetComponent<Key>().keyId == 30)
                {
                    itemInHand = Instantiate(brokenTablePre1, itemCamera.transform);
                }
                else if (GetEquipped().GetComponent<Key>().keyId == 31)
                {
                    itemInHand = Instantiate(brokenTablePre2, itemCamera.transform);
                }
                else if (GetEquipped().GetComponent<Key>().keyId == 32)
                {
                    itemInHand = Instantiate(brokenTablePre3, itemCamera.transform);
                }
                else
                {
                    itemInHand = Instantiate(leverPre, itemCamera.transform);
                }
            }
            //other handheld items

            itemInHand.transform.GetComponent<Animator>().SetBool("itemEntry", true);
        }
    }


    public void AddCollectible(Collectible collectible)
    {
        collectibles++;
        UpdateUIColl();
    }

    void UpdateUIColl()
    {
        for (int i = 0; i < collectibles; i++)
        {
            collContainer.transform.GetChild(i).gameObject.SetActive(true);
        }
    }

}
