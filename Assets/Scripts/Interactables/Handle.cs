using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handle : InteractableInvBase
{
    float rotSpeed = 3;
    public MovingPlatform movingPlatform;

    public void OnInteract(float scrollInput)
    {

        if (scrollInput < 0)
        {
            ScrollDown();
        }
        else if (scrollInput > 0)
        {
            ScrollUp();
        }
    }


    public void ScrollUp()
    {
        Quaternion rot = Quaternion.Euler(0, rotSpeed, 0);
        transform.rotation *= rot;

        if (movingPlatform != null)
        {
            movingPlatform.moveUp();
        }
    }

    public void ScrollDown()
    {
        Quaternion rot = Quaternion.Euler(0, -rotSpeed, 0);
        transform.rotation *= rot;

        if (movingPlatform != null)
        {
            movingPlatform.moveDown();
        }

    }


    private void OnExit()
    {
        enabled = false;

    }
}
