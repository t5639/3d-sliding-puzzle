using ShiftingMaze;
using ShiftingMaze.Puzzle.Management;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Interact : MonoBehaviour
{
    Transform cam;

    [SerializeField] float range = 5f;

    FocusInteraction focus;
    Handle handle;
    RotateByHand rotatebyHand;
    PuzzleController table;


    Vector3 mousePosition = Vector3.zero;

    public GameObject haircrossOut;
    public GameObject haircrossIn;
    public GameObject haircrossOutanim;
    public GameObject haircrossInanim;
    public GameObject actionImage;

    public UnityEvent OnMoveTile;

    RectTransform crossScale;

    private void Awake()
    {
        cam = Camera.main.transform;

        crossScale = haircrossIn.GetComponent<RectTransform>();
    }

    public void CastRay ()
    {
        RaycastHit hit;

        if(Physics.Raycast(cam.position, cam.forward, out hit, range))
        {
            if (hit.collider.GetComponent<InteractableBase>() != null)
            {
                haircrossInanim.SetActive(true);
                haircrossOutanim.SetActive(true);

                haircrossInanim.GetComponent<Animator>().SetTrigger("click");
                haircrossOutanim.GetComponent<Animator>().SetTrigger("click");
            }

            //lever
            if (hit.collider.GetComponent<Lever>() != null)
            {
                hit.collider.GetComponent<Lever>().OnInteract();
            }

            if (hit.collider.GetComponent<LeverStation>() != null)
            {
                InteractableInvBase item = Inventory.instance.GetEquipped();
                if (item is Lever)
                {
                    hit.collider.GetComponent<LeverStation>().OnInteract(item);
                }
            }

            //Button
            if (hit.collider.GetComponent<Button>() != null)
            {
                hit.collider.GetComponent<Button>().OnInteract();
            }

            //Focus Interaction
            if (hit.collider.GetComponent<FocusInteraction>() != null)
            {
                hit.collider.GetComponent<FocusInteraction>().OnInteract();
                focus = hit.collider.GetComponent<FocusInteraction>();
                haircrossIn.SetActive(false);
                haircrossOut.SetActive(false);

                //Handle
                if (hit.collider.transform.parent.GetComponentInChildren<Handle>() != null)
                {
                    handle = hit.collider.transform.parent.GetComponentInChildren<Handle>();
                }

                //Stonetable
                if (hit.collider.transform.parent.GetComponentInChildren<PuzzleController>() != null)
                {               
                    InputManager.instance.EnableStoneTable();
                    table = hit.collider.transform.parent.GetComponentInChildren<PuzzleController>();
                }
            }

            //Key and Lock
            if (hit.collider.GetComponent<Key>() != null)
            {
                hit.collider.GetComponent<Key>().OnInteract();
            }

            if (hit.collider.GetComponent<Lock>() != null)
            {
                InteractableInvBase item = Inventory.instance.GetEquipped();
                if (item is Key)
                {
                    hit.collider.GetComponent<Lock>().OnInteract();
                }
            }

            //











        }
    }

    //casts while Game Mode and detect all interactibles
    

    public void CastRayDetection()
    {
        RaycastHit hit;

        if (crossScale.sizeDelta.x != 50)
        {
            crossScale.sizeDelta = new Vector2(50, 50);
        }

        //if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePosition), out hit, range))
        if (Physics.Raycast(cam.position, cam.forward, out hit, range))
        {
            if (hit.collider.GetComponent<InteractableBase>() != null)
            {

                if (actionImage.GetComponent<Image>().sprite == null)
                {
                    crossScale.sizeDelta = new Vector2(70, 70);
                    actionImage.GetComponent<Image>().sprite = hit.collider.GetComponent<InteractableBase>().actionImage;
                    
                    haircrossOut.SetActive(true);
                    haircrossIn.SetActive(true);

                    actionImage.SetActive(true);
                }

            }
            else
            {
                actionImage.GetComponent<Image>().sprite = null;
                haircrossOut.SetActive(false);
                actionImage.SetActive(false);
            }
        } else
        {
            actionImage.GetComponent<Image>().sprite = null;
            haircrossOut.SetActive(false);
            actionImage.SetActive(false);
        }
    }




    //receiving information from Inputmanager
    public void ReceiveScroll(float scrollInput)
    {
        if (handle != null)
        {
            handle.OnInteract(scrollInput);
        }
    }

    public void ReceiveRotation(float rotation, float active)
    {
        if (rotatebyHand != null)
        {
            if (active == 1)
            {
                rotatebyHand.OnInteract(rotation);
            }
        }
    }
    public void SelectRotationPlate(Vector2 mouseInput)
    {
        RaycastHit hit;
        mousePosition = mouseInput;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePosition), out hit, range))
        {
            if (hit.collider.GetComponent<RotateByHand>() != null)
            {
                rotatebyHand = hit.collider.GetComponent<RotateByHand>();
            }
        }
    }


    //Lay focus off of interactible
    public void Unfocus()
    {
        if (focus != null)
        {
            focus.gameObject.SetActive(true);
            focus.OnInteract();

            if (table.gameObject.GetComponentInChildren<PuzzleManager>() != null)
            {
                table.gameObject.GetComponentInChildren<PuzzleManager>().ApplyChanges();
            }

            InputManager.instance.DisableStoneTable();
            table = null;
            
            handle = null;
            rotatebyHand = null;
        }
    }


    public bool puzzleMode = true;

    public void TableUp()
    {
        OnMoveTile.Invoke();

        if (puzzleMode)
        {
            table.Move(CardinalPoint.North);
        }
        else
        {
            table.Move(CardinalPoint.South);
        }
    }

    public void TableEast()
    {
        OnMoveTile.Invoke();

        if (puzzleMode)
        {
            table.Move(CardinalPoint.East);
        }
        else
        {
            table.Move(CardinalPoint.West);
        }
    }

    public void TableSouth()
    {
        OnMoveTile.Invoke();

        if (puzzleMode)
        {
            table.Move(CardinalPoint.South);
        }
        else
        {
            table.Move(CardinalPoint.North);
        }
    }

    public void TableWest()
    {
        OnMoveTile.Invoke();

        if (puzzleMode)
        {
            table.Move(CardinalPoint.West);
        }
        else
        {
            table.Move(CardinalPoint.East);
        }
    }



}
