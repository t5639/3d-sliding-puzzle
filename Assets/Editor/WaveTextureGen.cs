namespace ShiftingMaze.Editor
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

    public class WaveTextureGen : EditorWindow
    {
        private int cellCount = 4;
        private int size = 8;
        private Texture3D texture;
        private int seed = 1;

        [MenuItem("Window/Wave Generation")]
        static void Init()
        {
            WaveTextureGen window = (WaveTextureGen)GetWindow(typeof(WaveTextureGen), true, "Wave Generation");
            window.Show();
        }

        void OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            cellCount = EditorGUILayout.IntField($"Cell Count ({ Mathf.Pow(2, cellCount) })", cellCount);
            size = EditorGUILayout.IntField($"Texture Size ({ Mathf.Pow(2, size) })", size);
            EditorGUILayout.BeginHorizontal();
            seed = EditorGUILayout.IntField($"Random seed", seed);
            if (GUILayout.Button("Random"))
            {
                seed = Random.Range(int.MinValue, int.MaxValue);
            }
            EditorGUILayout.EndHorizontal();
            if (EditorGUI.EndChangeCheck())
            {
                texture = null;
            }

            if (GUILayout.Button("Generate"))
            {
                texture = MakeTexture();
            }

            if (texture)
            {
                texture.name = EditorGUILayout.TextField("Name", texture.name);

                if (GUILayout.Button("Save Texture"))
                {
                    AssetDatabase.CreateAsset(texture, $"Assets/{ texture.name }.asset");
                }
            }
        }

        private Texture3D MakeTexture()
        {
            int size = (int)Mathf.Pow(2, this.size);
            Texture3D texture = new Texture3D(size, size, size, TextureFormat.RGBAFloat, false);
            texture.name = "Wave";
            int cellCount = (int)Mathf.Pow(2, this.cellCount);
            int cellSize = size / cellCount;
            Vector3Int[,,] points = new Vector3Int[cellCount, cellCount, cellCount];
            float[,,] pixels = new float[size, size, size];
            Random.InitState(seed);

            foreach (var cell in GetCellCoorinates(cellCount))
            {
                points[cell.x, cell.y, cell.z] = new Vector3Int(Random.Range(0, cellSize), Random.Range(0, cellSize), Random.Range(0, cellSize));
            }

            float maxDistance = 0f;

            foreach (var cell in GetCellCoorinates(cellCount))
            {
                foreach (var pixel in GetPixelsInCell(cell, cellSize))
                {
                    float closest = float.PositiveInfinity;
                    foreach (var neighbour in GetNeighbourCells(cell, cellCount))
                    {
                        float distance = Vector3.Distance(pixel, neighbour * cellSize + points[neighbour.x, neighbour.y, neighbour.z]);
                        if (distance < closest)
                        {
                            closest = distance;
                        }
                    }
                    pixels[pixel.x, pixel.y, pixel.z] = closest; 
                    if (maxDistance < closest)
                    {
                        maxDistance = closest;
                    }
                }
            }

            float multiplyer = 1f / maxDistance;

            foreach (var pixel in GetPixelCoorinates(size))
            {
                texture.SetPixel(pixel.x, pixel.y, pixel.z, Color.white * pixels[pixel.x, pixel.y, pixel.z] * multiplyer);
            }
            texture.Apply();

            return texture;
        }

        private IEnumerable<Vector3Int> GetPixelCoorinates(int size)
        {
            for (int z = 0; z < size; z++)
            {
                for (int y = 0; y < size; y++)
                {
                    for (int x = 0; x < size; x++)
                    {
                        yield return new Vector3Int(x, y, z);
                    }
                }
            }
        }
        private IEnumerable<Vector3Int> GetCellCoorinates(int cellCount)
        {
            for (int z = 0; z < cellCount; z++)
            {
                for (int y = 0; y < cellCount; y++)
                {
                    for (int x = 0; x < cellCount; x++)
                    {
                        yield return new Vector3Int(x, y, z);
                    }
                }
            }
        }

        private IEnumerable<Vector3Int> GetNeighbourCells(Vector3Int cell, int cellCount)
        {
            for (int z = -1; z <= 1; z++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    for (int x = -1; x <= 1; x++)
                    {
                        yield return new Vector3Int((cell.x + cellCount + x) % cellCount, (cell.y + cellCount + y) % cellCount, (cell.z + cellCount + z) % cellCount);
                    }
                }
            }
        }

        private IEnumerable<Vector3Int> GetPixelsInCell(Vector3Int cell, int cellSize)
        {
            for (int z = 0; z < cellSize; z++)
            {
                for (int y = 0; y < cellSize; y++)
                {
                    for (int x = 0; x < cellSize; x++)
                    {
                        yield return cell * cellSize + new Vector3Int(x, y, z);
                    }
                }
            }
        }
    }
}
