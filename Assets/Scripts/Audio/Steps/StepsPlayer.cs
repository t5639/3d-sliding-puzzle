using ShiftingMaze.Player;
using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepsPlayer : MonoBehaviour
{
    public static StepsPlayer Instance;

    public AudioSource StepSource1, StepSource2;
    private bool stepSourceToggle = true;
    private MovementController player;
    public double stepLength;
    Vector3 lastPosition;
    int stepsTaken;
    private bool movingOnGround = true, movingOnLadder = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<MovementController>();
        if(player == null)
        {
            Debug.LogError("Playersteps is not child of PlayerController");
        }
        Instance = this;
        lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (movingOnGround)
        {
            transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
            if (Vector3.Distance(lastPosition, transform.position) > stepLength)
            {
                PlayCurrentStep();

                stepsTaken++;
                lastPosition = transform.position;
            }

        }

        else if (movingOnLadder)
        {
            transform.position = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
            if (Vector3.Distance(lastPosition, transform.position) > stepLength / 2)
            {
                PlayStep(StepTerrain.tagNames.ladder_terrain);

                stepsTaken++;
                lastPosition = transform.position;
            }

        }


        if (player.IsGrounded)
        {
            if (!movingOnGround)
            {
                lastPosition = transform.position;
                if (!movingOnLadder)
                {
                    PlayCurrentStep(1.5f); // Play Land
                }
            }
            movingOnLadder = false;
            movingOnGround = true;
        }
        else if (movingOnLadder)
        {
            if (!movingOnLadder)
            {
                lastPosition = transform.position;
            }
            movingOnLadder = false;
            movingOnGround = true;
        }
        else
        {
            if (movingOnGround)
            {
                PlayCurrentStep(1.2f);
            }
            movingOnLadder = false;
            movingOnGround = false;

        }

        


    }


    private void  PlayCurrentStep(float volumeMultiplicator = 1)
    {
        AudioSource source = stepSourceToggle ? StepSource1 : StepSource2;
        StepSample st = StepAudioManager.Instance.GetStep(source.gameObject.transform);

        source.clip = st.Sample;
        source.volume = st.volume * volumeMultiplicator;
        source.pitch = st.pitch;

        source.Play();
        stepSourceToggle = !stepSourceToggle;
    }

    private void PlayStep(StepTerrain.tagNames terrainTag, float volumeMultiplicator = 1)
    {
        AudioSource source = stepSourceToggle ? StepSource1 : StepSource2;
        StepSample st = StepAudioManager.Instance.GetStep(terrainTag);

        source.clip = st.Sample;
        source.volume = st.volume * volumeMultiplicator;
        source.pitch = st.pitch;

        source.Play();
        stepSourceToggle = !stepSourceToggle;
    }
}
