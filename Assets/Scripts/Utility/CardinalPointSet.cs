namespace ShiftingMaze
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /**
     * All <see cref="CardinalPointCollection{T}"/>s should be under the "Set/CardinalPoint" asset menu.
     */

    /// <summary>
    /// A <see cref="ScriptableObject"/> providing values for all 4 <see cref="CardinalPoint"/>s.
    /// </summary>
    /// <typeparam name="T">The base type of all values.</typeparam>
    /// <typeparam name="TNorth">The type for the north value.</typeparam>
    /// <typeparam name="TEast">The type for the east value.</typeparam>
    /// <typeparam name="TSouth">The type for the south value.</typeparam>
    /// <typeparam name="TWest">The type for the west value.</typeparam>
    public class CardinalPointSet<T, TNorth, TEast, TSouth, TWest> : ScriptableObject, ICardinalPointCollection<T, TNorth, TEast, TSouth, TWest>
        where TNorth : T where TEast : T where TSouth : T where TWest : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the north direction.")]
        protected TNorth north = default;
        /// <inheritdoc/>
        public TNorth North => north;

        [SerializeField, Tooltip("The value for the east direction.")]
        protected TEast east = default;
        /// <inheritdoc/>
        public TEast East => east;

        [SerializeField, Tooltip("The value for the south direction.")]
        protected TSouth south = default;
        /// <inheritdoc/>
        public TSouth South => south;

        [SerializeField, Tooltip("The value for the west direction.")]
        protected TWest west = default;
        /// <inheritdoc/>
        public TWest West => west;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public T GetValue(CardinalPoint direction)
        {
            switch (direction)
            {
                default: return default;
                case CardinalPoint.North: return north;
                case CardinalPoint.East: return east;
                case CardinalPoint.South: return south;
                case CardinalPoint.West: return west;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<CardinalPoint, T>> GetEnumerator()
        {
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.North, north);
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.East, east);
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.South, south);
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.West, west);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    /// <summary>
    /// A <see cref="ScriptableObject"/> providing values for all 4 <see cref="CardinalPoint"/>s.
    /// </summary>
    /// <typeparam name="T">The type of all values.</typeparam>
    public class CardinalPointSet<T> : CardinalPointSet<T, T, T, T, T> { }

    /// <summary>
    /// A class providing values for all 4 <see cref="CardinalPoint"/>s.
    /// </summary>
    /// <typeparam name="T">The base type of all values.</typeparam>
    /// <typeparam name="TNorth">The type for the north value.</typeparam>
    /// <typeparam name="TEast">The type for the east value.</typeparam>
    /// <typeparam name="TSouth">The type for the south value.</typeparam>
    /// <typeparam name="TWest">The type for the west value.</typeparam>
    [System.Serializable]
    public class CardinalPointCollection<T, TNorth, TEast, TSouth, TWest> : ICardinalPointCollection<T, TNorth, TEast, TSouth, TWest>
        where TNorth : T where TEast : T where TSouth : T where TWest : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the north direction.")]
        protected TNorth north = default;
        /// <inheritdoc/>
        public TNorth North
        {
            get => north;
            set => north = value;
        }

        [SerializeField, Tooltip("The value for the east direction.")]
        protected TEast east = default;
        /// <inheritdoc/>
        public TEast East
        {
            get => east;
            set => east = value;
        }

        [SerializeField, Tooltip("The value for the south direction.")]
        protected TSouth south = default;
        /// <inheritdoc/>
        public TSouth South
        {
            get => south;
            set => south = value;
        }

        [SerializeField, Tooltip("The value for the west direction.")]
        protected TWest west = default;
        /// <inheritdoc/>
        public TWest West
        {
            get => west;
            set => west = value;
        }


        // --- | Constructors | --------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new collection for values for each <see cref="CardinalPoint"/>.
        /// </summary>
        /// <param name="north">The value for the north point.</param>
        /// <param name="east">The value for the east point.</param>
        /// <param name="south">The value for the south point.</param>
        /// <param name="west">The value for the west point.</param>
        public CardinalPointCollection(TNorth north, TEast east, TSouth south, TWest west)
        {
            this.north = north;
            this.east = east;
            this.south = south;
            this.west = west;
        }
        /// <summary>
        /// Creates a new collection for values for each <see cref="CardinalPoint"/>.
        /// </summary>
        public CardinalPointCollection() : this(default, default, default, default) { }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public T GetValue(CardinalPoint direction)
        {
            switch (direction)
            {
                default: return default;
                case CardinalPoint.North: return north;
                case CardinalPoint.East: return east;
                case CardinalPoint.South: return south;
                case CardinalPoint.West: return west;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<CardinalPoint, T>> GetEnumerator()
        {
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.North, north);
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.East, east);
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.South, south);
            yield return new KeyValuePair<CardinalPoint, T>(CardinalPoint.West, west);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    /// <summary>
    /// A class providing values for all 4 <see cref="CardinalPoint"/>s.
    /// </summary>
    /// <typeparam name="T">The type of the values.</typeparam>
    [System.Serializable]
    public class CardinalPointCollection<T> : CardinalPointCollection<T, T, T, T, T>
    {
        /// <summary>
        /// Creates a new collection for values for each <see cref="CardinalPoint"/>.
        /// </summary>
        /// <param name="value">The value for the directions.</param>
        public CardinalPointCollection(T value) : base(value, value, value, value) { }
        /// <summary>
        /// Creates a new collection for values for each <see cref="CardinalPoint"/>.
        /// </summary>
        public CardinalPointCollection() : this(default) { }

        /// <summary>
        /// Sets the value of the given <see cref="CardinalPoint"/>.
        /// </summary>
        /// <param name="direction">the direction whos value to change.</param>
        /// <param name="value">The value to apply.</param>
        public void SetValue(CardinalPoint direction, T value)
        {
            switch (direction)
            {
                case CardinalPoint.North: north = value; break;
                case CardinalPoint.East: east = value; break;
                case CardinalPoint.South: south = value; break;
                case CardinalPoint.West: west = value; break;
            }
        }
    }

    /// <summary>
    /// A interface providing values for all 4 <see cref="CardinalPoint"/>s.
    /// </summary>
    /// <typeparam name="T">The base type of all values.</typeparam>
    /// <typeparam name="TNorth">The type for the north value.</typeparam>
    /// <typeparam name="TEast">The type for the east value.</typeparam>
    /// <typeparam name="TSouth">The type for the south value.</typeparam>
    /// <typeparam name="TWest">The type for the west value.</typeparam>
    public interface ICardinalPointCollection<T, TNorth, TEast, TSouth, TWest> : IEnumerable<KeyValuePair<CardinalPoint, T>>
        where TNorth : T where TEast : T where TSouth : T where TWest : T
    {
        /// <summary>
        /// Returns the value for the given direction.
        /// </summary>
        /// <param name="direction">The direction whos value to return.</param>
        /// <returns>The value for the given <see cref="CardinalPoint"/>.</returns>
        T GetValue(CardinalPoint direction);

        /// <summary>
        /// The value for the north direction.
        /// </summary>
        TNorth North { get; }
        /// <summary>
        /// The value for the east direction.
        /// </summary>
        TEast East { get; }
        /// <summary>
        /// The value for the south direction.
        /// </summary>
        TSouth South { get; }
        /// <summary>
        /// The value for the west direction.
        /// </summary>
        TWest West { get; } 
    }
}
