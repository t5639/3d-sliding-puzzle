namespace ShiftingMaze.Camera
{
    using System.Collections;
    using UnityEngine;
    using static UnityEngine.InputSystem.InputAction;

    public class CameraZoom : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The camera to target.")]
        private Camera target = default;

        [SerializeField, Range(1e-05f, 179f), Tooltip("The field of view at the maximized zoom state.")]
        private float maxZoom = 40f;

        [SerializeField, Range(1e-05f, 179f), Tooltip("The field of view at the minimized zoom state")]
        private float minZoom = 60f;

        [SerializeField, Min(0f), Tooltip("The speed of the zoom.")]
        private float acceleration = 30f;
        [SerializeField, Min(0f), Tooltip("The speed of the zoom.")]
        private float maxSpeed = 30f;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private float zoom;
        private Coroutine zoomLoop = null;
        private bool IsZooming => zoomLoop != null;
        private float targetZoom = 0f;
        private int zoomDirection = 0;

        private PlayerSystem inputs;
        private PlayerSystem.GroundMovementActions ground;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            inputs = new PlayerSystem();
            ground = inputs.GroundMovement;
            ground.Zoom.performed += ZoomIn;
            ground.Zoom.canceled += ZoomOut;
            inputs.Enable();

            SetTargetZoom();
        }

        private void OnDisable()
        {
            inputs.Disable();
            ground.Zoom.canceled -= ZoomOut;
            ground.Zoom.performed -= ZoomIn;
        }

        public void Zoom(float zoom)
        {
            this.zoom = Mathf.Clamp01(zoom);
            SetTargetZoom();
            if (!IsZooming)
            {
                zoomLoop = StartCoroutine(DoZoom());
            }
        }
        private void Zoom(CallbackContext context) => Zoom(context.ReadValue<float>());

        public void ZoomIn() => Zoom(1f);
        private void ZoomIn(CallbackContext context) => ZoomIn();
        public void ZoomOut() => Zoom(0f);
        private void ZoomOut(CallbackContext context) => ZoomOut();

        private IEnumerator DoZoom()
        {
            float speed = 0f;
            float min;
            float max;
            while (target.fieldOfView != targetZoom)
            {
                zoomDirection =  targetZoom > target.fieldOfView ? 1 : -1;
                speed = Mathf.Clamp(speed + acceleration * zoomDirection * Time.deltaTime, -maxSpeed, maxSpeed);
                target.fieldOfView += speed;
                if (zoomDirection > 0)
                {
                    if (target.fieldOfView > targetZoom)
                    {
                        target.fieldOfView = targetZoom;
                    }
                }
                else
                {
                    if (target.fieldOfView < targetZoom)
                    {
                        target.fieldOfView = targetZoom;
                    }
                }
                yield return null;
            }
            zoomDirection = 0;
            zoomLoop = null;
        }

        private void SetTargetZoom()
        {
            targetZoom = Mathf.Lerp(minZoom, maxZoom, zoom);
        }
    }
}
