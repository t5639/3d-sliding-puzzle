using ShiftingMaze.Player;
using ShiftingMaze.Camera;
using bugiflo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputManager : MonoBehaviour
{
    public static InputManager instance;

    [SerializeField] Interact interact;
    [SerializeField] Inventory inventory;
    [SerializeField] UI_Interaction ui;
    [SerializeField] PlayerController player;
    [SerializeField] CameraRotationController camera0;
    [SerializeField] CameraZoom camera1;

    PlayerSystem controls;
    PlayerSystem.GroundMovementActions groundMovement;
    PlayerSystem.InteractionActions interaction;
    PlayerSystem.FocusInteractionActions focusInteraction;
    PlayerSystem.PauseMenuActions pauseMenu;
    PlayerSystem.StoneTableActions stoneTable;

    Vector2 mouseInput;
    Vector2 mousePosition;

    float scrollInput;
    float rotationbyHand;
    float rotationPressed;

    private void Awake()
    {
        instance = this;

        controls = new PlayerSystem();
        groundMovement = controls.GroundMovement;

        //player controller
        /*groundMovement.HorizontalMovement.performed += ctx => horizontalInput = ctx.ReadValue<Vector2>();

        groundMovement.MouseX.performed += ctx => mouseInput.x = ctx.ReadValue<float>();
        groundMovement.MouseY.performed += ctx => mouseInput.y = ctx.ReadValue<float>();*/

        //pause game
        groundMovement.Pause.performed += _ => ui.PauseGame();

        //interaction

        interaction = controls.Interaction;
        interaction.Interact.performed += _ => interact.CastRay();

        interaction.SwitchInventory.performed += _ => inventory.SwitchEquipped();

        interaction.Detection.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();

        //focus Interaction
        focusInteraction = controls.FocusInteraction;

        focusInteraction.FocusOut.performed += _ => interact.Unfocus();

        focusInteraction.MousePos.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();

        focusInteraction.Scroll.performed += ctx => scrollInput = ctx.ReadValue<float>();

        focusInteraction.RotatebyHand.performed += ctx => rotationbyHand = ctx.ReadValue<float>();
        focusInteraction.RotatePressed.performed += ctx => rotationPressed = ctx.ReadValue<float>();

        focusInteraction.SelectPlate.performed += _ => interact.SelectRotationPlate(mousePosition);

        //pause Menu
        pauseMenu = controls.PauseMenu;

        pauseMenu.EndPause.performed += _ => ui.EndPauseMenu();


        //Stone Table System 1
        stoneTable = controls.StoneTable;

        stoneTable.Up.performed += _ => interact.TableUp();
        stoneTable.Right.performed += _ => interact.TableEast();
        stoneTable.Down.performed += _ => interact.TableSouth();
        stoneTable.Left.performed += _ => interact.TableWest();


    }

    private void Start()
    {
        focusInteraction.Disable();
        pauseMenu.Disable();
        stoneTable.Disable();
    }

    private void Update()
    {
        if (groundMovement.enabled)
        {
            interact.CastRayDetection();
        }
        
        if (focusInteraction.enabled)
        {
            interact.ReceiveScroll(scrollInput);
            interact.ReceiveRotation(rotationbyHand, rotationPressed);
        }
    }


    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    public void DisableGroundMovement()
    {
        groundMovement.Disable();
        interaction.Disable();

        player.enabled = false;
        //player.IsEnabled.Disable(this);
        camera0.Deactivate(this);
        camera1.enabled = false;
    }

    public void EnableGroundMovement()
    {
        groundMovement.Enable();
        interaction.Enable();

        player.enabled = true;
        //player.IsEnabled.Enable(this);
        camera0.Activate(this);
        camera1.enabled = true;
        /*mouseLook.enabled = true;
        movement.enabled = true;*/
    }


    public void DisableFocus()
    {
        focusInteraction.Disable();
        SetCursorHairCross();
    }

    public void EnableFocus()
    {
        focusInteraction.Enable();
        SetCursorStandard();
    }

    public void EnablePauseMenu()
    {
        pauseMenu.Enable();
        SetCursorStandard();
    }

    public void DisablePauseMenu()
    {
        pauseMenu.Disable();
        SetCursorHairCross();
    }

    bool stone = false;
    public void EnableStoneTable()
    {
        stoneTable.Enable();
        stone = true;
    }

    public void DisableStoneTable()
    {
        stoneTable.Disable();
        stone = false;
    }


    //visuals

    public void SetCursorHairCross()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void SetCursorStandard()
    {
        if (!stone)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public bool IsPauseMenuEnabled()
    {
        return pauseMenu.enabled;
    }

}
