using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusInteraction : InteractableBase
{
    public Transform cam;
    public Transform target;
    public Transform camOrigin;

    float timeElapsed;
    float lerpDuration = 1;

    bool focused = false;
    bool interactable = true;

    public GameObject hairCross;

    public override void OnInteract()
    {
        hairCross.SetActive(false);
        if (interactable)
        {
            timeElapsed = 0;
            interactable = false;

            if (!focused)
            {
                InputManager.instance.DisableGroundMovement();

                focused = !focused;
                StartCoroutine(FocusOnInteraction(cam.position, target.position, cam.rotation, target.rotation));
            }
            else
            {
                focused = !focused;

                InputManager.instance.DisableFocus();

                StartCoroutine(FocusOnInteraction(cam.position, camOrigin.position, cam.rotation, camOrigin.rotation));
            }

        }
    }


    public IEnumerator FocusOnInteraction(Vector3 posCam, Vector3 posTarget, Quaternion rotCam, Quaternion rotTarget)
    {

        while (timeElapsed < lerpDuration)
        {
            cam.position = Vector3.Lerp(posCam, posTarget, timeElapsed / (lerpDuration));
            cam.rotation = Quaternion.Slerp(rotCam, rotTarget, timeElapsed / (lerpDuration));

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        gameObject.SetActive(!focused);
        interactable = true;

        if (!focused)
        {
            InputManager.instance.EnableGroundMovement();
            hairCross.SetActive(true);

            cam.position = camOrigin.position;
            cam.rotation = camOrigin.rotation;
        }
        else
        {
            InputManager.instance.EnableFocus();

            cam.position = posTarget;
            cam.rotation = rotTarget;
        }

        yield return null;
    }

}
