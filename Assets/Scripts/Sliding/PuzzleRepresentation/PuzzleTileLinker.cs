namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Signals;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Links the signal of the tiles.
    /// </summary>
    public class PuzzleTileLinker : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script managing the tiles.")]
        private PuzzleMatrix manager = default;

        [SerializeField, Tooltip("The connections from outside the puzzle, with the puzzle.")]
        private BorderConnection[] borderConnections = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private TileSignals[,] tiles;
        private Dictionary<Vector2Int, TileSignals> tileStorage = new Dictionary<Vector2Int, TileSignals>();
        private Dictionary<Vector2Int, CardinalPointCollection<SignalPoint>> outsideConnections;


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // MonoBehavour ---------------------------------------------------------------------------

        private void Awake()
        {
            outsideConnections = new Dictionary<Vector2Int, CardinalPointCollection<SignalPoint>>();
            foreach (var connection in borderConnections)
            {
                Vector2Int coordinate;
                switch (connection.direction)
                {
                    case CardinalPoint.North: coordinate = new Vector2Int(connection.coordinate, manager.Height - 1); break;
                    case CardinalPoint.West: coordinate = new Vector2Int(0, connection.coordinate); break;
                    case CardinalPoint.South: coordinate = new Vector2Int(connection.coordinate, 0); break;
                    case CardinalPoint.East: coordinate = new Vector2Int(manager.Width - 1, connection.coordinate); break;
                    default: coordinate = default; break;
                }
                if (!outsideConnections.ContainsKey(coordinate))
                {
                    outsideConnections.Add(coordinate, new CardinalPointCollection<SignalPoint>());
                }
                outsideConnections[coordinate].SetValue(connection.direction, connection.point);
            }
            manager.CallAfterSetup(m => UpdateGrid(m.GetAllTilesWithCoordinates()));
        }

        // Setup ----------------------------------------------------------------------------------

        /// <summary>
        /// Updates all connection of the tiles in the grid.
        /// </summary>
        /// <param name="tiles">The tiles to update.</param>
        public void UpdateGrid(IEnumerable<(PuzzleTile tile, Vector2Int coorinate)> tiles)
        {
            // Check if there were already linked connections.
            if (this.tiles != null)
            {
                // Disconnect all tiles.
                foreach (var connection in GetTileConnectedCoordinates())
                {
                    TryBreackingConnection(connection.x1, connection.y1, connection.x2, connection.y2);
                }
                foreach (var borderCooridnate in outsideConnections)
                {
                    TryBreakingOutsideConnection(borderCooridnate.Key.x, borderCooridnate.Key.y);
                }
            }

            // Create a new tile array.
            this.tiles = new TileSignals[manager.Width, manager.Height]; 
            // Assign all tiles.
            foreach (var pair in tiles)
            {
                this.tiles[pair.coorinate.x, pair.coorinate.y] = pair.tile.GetComponentInChildren<TileSignals>();
            }

            // Link the tiles.
            foreach (var connection in GetTileConnectedCoordinates())
            {
                TryLinkingConnection(connection.x1, connection.y1, connection.x2, connection.y2);
            }
            foreach (var borderCooridnate in outsideConnections)
            {
                TryLinkingOutsideConnection(borderCooridnate.Key.x, borderCooridnate.Key.y);
            }
        }

        // Grid Iterations ------------------------------------------------------------------------

        /// <summary>
        /// Iterates over all the tiles and their neighbours.
        /// </summary>
        /// <returns>The positions in the grid.</returns>
        private IEnumerable<(int x1, int y1, int x2, int y2)> GetTileConnectedCoordinates()
        {
            for (int y = 0; y < manager.Height - 1; y++)
            {
                for (int x = 0; x < manager.Width - 1; x++)
                {
                    yield return (x, y, x + 1, y);
                    yield return (x, y, x, y + 1);
                }
                yield return (manager.Width - 1, y, manager.Width - 1, y + 1);
            }
            for (int x = 0; x < manager.Width - 1; x++)
            {
                yield return (x, manager.Height - 1, x + 1, manager.Height - 1);
            }
        }

        // Signle Tile Connection / Disconnection -------------------------------------------------

        /// <summary>
        /// Disconnects a tile befor moving.
        /// </summary>
        /// <param name="previous">The tiles previous position.</param>
        /// <param name="tile">The moved tile.</param>
        /// <param name="current">The new position of the tile.</param>
        public void DisconnectTile(Vector2Int previous, PuzzleTile tile, Vector2Int current)
        {
            DisconnectFromNeighbouts(previous.x, previous.y);
        }

        /// <summary>
        /// Connects a tile after moving.
        /// </summary>
        /// <param name="previous">The tiles previous position.</param>
        /// <param name="tile">The moved tile.</param>
        /// <param name="current">The new position of the tile.</param>
        public void ReconnectTile(Vector2Int previous, PuzzleTile tile, Vector2Int current)
        {
            tileStorage[current] = tiles[current.x, current.y];
            if (tileStorage.ContainsKey(previous))
            {
                tiles[current.x, current.y] = tileStorage[previous];
                tileStorage.Remove(previous);
            }
            else
            {
                tiles[current.x, current.y] = tiles[previous.x, previous.y];
            }

            ConnectToNeighbours(current.x, current.y);
        }

        /// <summary>
        /// Disconnects a tile from all its neighbours.
        /// </summary>
        /// <param name="x">The x coordinate of the tile.</param>
        /// <param name="y">The y coordinate of the tile.</param>
        private void DisconnectFromNeighbouts(int x, int y)
        {
            TryBreackingConnection(x, y, x + 1, y);
            TryBreackingConnection(x, y, x - 1, y);
            TryBreackingConnection(x, y, x, y + 1);
            TryBreackingConnection(x, y, x, y - 1);
            TryBreakingOutsideConnection(x, y);
        }

        /// <summary>
        /// Connects a tile to all its neighbours.
        /// </summary>
        /// <param name="x">The x coordinate of the tile.</param>
        /// <param name="y">The y coordinate of the tile.</param>
        private void ConnectToNeighbours(int x, int y)
        {
            TryLinkingConnection(x, y, x + 1, y);
            TryLinkingConnection(x, y, x - 1, y);
            TryLinkingConnection(x, y, x, y + 1);
            TryLinkingConnection(x, y, x, y - 1);
            TryLinkingOutsideConnection(x, y);
        }

        /// <summary>
        /// Trys to connect two tiles.
        /// </summary>
        /// <param name="x1">The x coordinate of the first tile.</param>
        /// <param name="y1">The y coordinate of the first tile.</param>
        /// <param name="x2">The x coordinate of the second tile.</param>
        /// <param name="y2">The y coordinate of the second tile.</param>
        private void TryLinkingConnection(int x1, int y1, int x2, int y2)
        {
            if (CoordinateCheck(x1, y1) && CoordinateCheck(x2, y2))
            {
                CardinalPoint direction = CardinalPointUtilities.Vector2ToPoint(new Vector2(x2- x1, y2 - y1));
                tiles[x1, y1]?.Points.GetValue(direction)?.AddConnection(tiles[x2, y2]?.Points.GetValue(direction.Invert()));
            }
        }

        /// <summary>
        /// Trys to disconnect two tiles.
        /// </summary>
        /// <param name="x1">The x coordinate of the first tile.</param>
        /// <param name="y1">The y coordinate of the first tile.</param>
        /// <param name="x2">The x coordinate of the second tile.</param>
        /// <param name="y2">The y coordinate of the second tile.</param>
        private void TryBreackingConnection(int x1, int y1, int x2, int y2)
        {
            if (CoordinateCheck(x1, y1) && CoordinateCheck(x2, y2))
            {
                CardinalPoint direction = CardinalPointUtilities.Vector2ToPoint(new Vector2(x2 - x1, y2 - y1));
                tiles[x1, y1]?.Points.GetValue(direction)?.RemoveConnection(tiles[x2, y2]?.Points.GetValue(direction.Invert()));
            }
        }

        /// <summary>
        /// Try to connect the tile with the outside world.
        /// </summary>
        /// <param name="x">The x coordinate of the tile.</param>
        /// <param name="y">The y coordinate of the tile.</param>
        private void TryLinkingOutsideConnection(int x, int y)
        {
            Vector2Int coordinates = new Vector2Int(x, y);
            if (outsideConnections.ContainsKey(coordinates))
            {
                CardinalPointCollection<SignalPoint> connection = outsideConnections[coordinates];
                foreach (var direction in connection)
                {
                    if (direction.Value)
                    {
                        tiles[x, y]?.Points.GetValue(direction.Key)?.AddConnection(direction.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Try to disconnect the tile with the outside world.
        /// </summary>
        /// <param name="x">The x coordinate of the tile.</param>
        /// <param name="y">The y coordinate of the tile.</param>
        private void TryBreakingOutsideConnection(int x, int y)
        {
            Vector2Int coordinates = new Vector2Int(x, y);
            if (outsideConnections.ContainsKey(coordinates))
            {
                CardinalPointCollection<SignalPoint> connection = outsideConnections[coordinates];
                foreach (var direction in connection)
                {
                    if (direction.Value)
                    {
                        tiles[x, y]?.Points.GetValue(direction.Key)?.RemoveConnection(direction.Value);
                    }
                }
            }
        }

        // Coordinates Check ----------------------------------------------------------------------

        /// <summary>
        /// Checks if the coordinates are part of the grid.
        /// </summary>
        /// <param name="x">The x coodinate to check.</param>
        /// <param name="y">The y coodinate to check.</param>
        /// <returns>True if the coordinates are in the grid.</returns>
        private bool CoordinateCheck(int x, int y)
        {
            return x >= 0 && x < manager.Width && y >= 0 && y < manager.Height;
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// A class holding the information about the outside connections.
        /// </summary>
        [System.Serializable]
        private class BorderConnection
        {
            [Tooltip("The coordinates of the tile to connect to.")]
            public int coordinate;

            [Tooltip("The point to connect to the puzzle.")]
            public SignalPoint point;

            [Tooltip("The direction of the connection (from the perspective of the outside point).")]
            public CardinalPoint direction;
        }
    }
}
