using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class UI_Interaction : MonoBehaviour
{
    /*
     * 
     * PAUSE MENU
     * 
     * 
     * 
     */

    [Header("Pause")]
    public GameObject pauseScreen;
    public GameObject pauseMainScreen;
    public GameObject pauseOptionsScreen;
    public GameObject pauseDiaryScreen;
    public GameObject pauseExitScreen;


    public UnityEvent OnMenuOpen, OnMenuClosed, OnSubmenu, OnReturnSubmenu;

    //selection
    public GameObject mainButton, optionsButton, diaryButton, exitButton;

    public void PauseGame()
    {
        OnMenuOpen.Invoke();
        //set active of PauseScreen
        pauseScreen.SetActive(true);
        SetPauseMain();

        //Disable/Enable interactions
        InputManager.instance.DisableGroundMovement();
        InputManager.instance.EnablePauseMenu();
    }

    //Button Click Events - Pause Menu
    public void EndPauseMenu()
    {
        OnMenuClosed.Invoke();
        pauseScreen.SetActive(false);
        pauseMainScreen.SetActive(false);
        pauseOptionsScreen.SetActive(false);
        pauseDiaryScreen.SetActive(false);
        pauseExitScreen.SetActive(false);

        InputManager.instance.EnableGroundMovement();
        InputManager.instance.DisablePauseMenu();
    }

    public void SetPauseMain()
    {
        if (InputManager.instance.IsPauseMenuEnabled())
        {
            OnReturnSubmenu.Invoke();
        }

        pauseMainScreen.SetActive(true);
        pauseOptionsScreen.SetActive(false);
        pauseDiaryScreen.SetActive(false);
        pauseExitScreen.SetActive(false);

        EventSystem.current.SetSelectedGameObject(mainButton);
    }

    public void SetPauseOptions()
    {
        OnSubmenu.Invoke();

        pauseMainScreen.SetActive(false);
        pauseOptionsScreen.SetActive(true);

        EventSystem.current.SetSelectedGameObject(optionsButton);
    }

    public void SetPauseDiary()
    {
        OnSubmenu.Invoke();

        pauseMainScreen.SetActive(false);
        pauseDiaryScreen.SetActive(true);

        EventSystem.current.SetSelectedGameObject(diaryButton);
    }

    public void SetPauseExit()
    {
        OnSubmenu.Invoke();

        pauseMainScreen.SetActive(false);
        pauseExitScreen.SetActive(true);

        EventSystem.current.SetSelectedGameObject(exitButton);
    }


    //Save or Straight to Menu
    public void SetMainMenuScene()
    {
        Debug.Log("load Main Menu Scene");
    }



    /*
     * 
     * START MENU
     * 
     * 
     * 
     */

    [Header("Start")]
    public GameObject startMainScreen;
    public GameObject startOptionsScreen;
    public GameObject controlStyle;

    //selection
    public GameObject mainFirstButton, optionsFirstButton, creditsFirstButton;

    bool _controlStyle = false;

    public void SetStartMain()
    {
        startMainScreen.SetActive(true);
        startOptionsScreen.SetActive(false);

        EventSystem.current.SetSelectedGameObject(mainFirstButton);
    }

    public void SetStartOptions()
    {
        startMainScreen.SetActive(false);
        startOptionsScreen.SetActive(true);

        EventSystem.current.SetSelectedGameObject(optionsFirstButton);
    }


    public void QuitGame()
    {
        Application.Quit();
    }

    public void NewGame()
    {
        Debug.Log("Szenen-Wechsel, neues Spiel");
        SceneManager.LoadScene(1);
    }

    public void ContinueGame()
    {
        Debug.Log("Szenen-Wechsel, Spiel laden");
    }

    public void setControlStyle()
    {
        _controlStyle = !_controlStyle;

        if (_controlStyle)
        {
            controlStyle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "X";
            controlStyle.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "You are playing with the hole moving";
            //change PlayStyle
        } else
        {
            controlStyle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
            controlStyle.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "You are playing with the block moving";
            //change PlayStyle
        }
    }


    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == "StartScreen")
        {
            SetStartMain();
        }
    }


}
