using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Inventory_UI : MonoBehaviour
{
    public GameObject images;
    public static Inventory_UI instance;
    public Sprite noItem;
    bool equipped = false;

    [Header("coordinates")]
    Vector3 coord11 = new Vector3(-370, 143, 10);
    Vector3 coord12 = new Vector3(-325, 143, 10);
    Vector3 coord13 = new Vector3(-277, 143, 10);

    Vector3 coord21 = new Vector3(-348, 97, 10);
    Vector3 coord22 = new Vector3(-301, 97, 10);

    Vector3 coord115 = new Vector3(-348, 143, 10);
    Vector3 coord125 = new Vector3(-301, 143, 10);

    Vector3 coord215 = new Vector3(-325, 97, 10);



    private void Awake()
    {
        instance = this;
    }

    //set UI for Inventory
    public void setImages()
    {

        int nonActive_length = Inventory.instance.itemList.Count;

        //set all images false
        for (int i = 0; i < nonActive_length; i++)
        {
            images.transform.GetChild(i).gameObject.SetActive(false);
        }

        //set active item image
        if (Inventory.instance.GetEquipped() == null)
        {
            images.transform.GetChild(5).GetComponent<Image>().sprite = noItem;
            equipped = false;
        }
        else
        {
            images.transform.GetChild(5).GetComponent<Image>().sprite = Inventory.instance.GetEquipped().image;

            equipped = true;
        }
       
        //set non-active item images
        for (int i = 0, j = 0; i < nonActive_length; i++)
        {
            if (Inventory.instance.itemList[i] != Inventory.instance.GetEquipped())
            {
                images.transform.GetChild(j).GetComponent<Image>().sprite = Inventory.instance.itemList[i].image;
                images.transform.GetChild(j).gameObject.SetActive(true);
                j++;
            }
        }
        if (equipped)
        {
            setPositions(nonActive_length-1);
        } else
        {
            setPositions(nonActive_length);
        }
    }

    //set Positions of images, depending on amount of items
    void setPositions(int i)
    {

        switch(i)
        {
            case 1:
                images.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = coord12;
                break;
            case 2:
                images.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = coord115;
                images.transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = coord125;
                break;
            case 3:
                setTop();
                break;
            case 4:
                setTop();
                images.transform.GetChild(3).GetComponent<RectTransform>().anchoredPosition = coord215;
                break;
            case 5:
                setTop();
                images.transform.GetChild(3).GetComponent<RectTransform>().anchoredPosition = coord21;
                images.transform.GetChild(4).GetComponent<RectTransform>().anchoredPosition = coord22;
                break;
        }
    }

    void setTop()
    {
        images.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = coord11;
        images.transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = coord12;
        images.transform.GetChild(2).GetComponent<RectTransform>().anchoredPosition = coord13;
    }







}