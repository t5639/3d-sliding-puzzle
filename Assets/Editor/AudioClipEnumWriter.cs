using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ClipCollection))]
public class AudioClipEnumWriter : Editor
{
    ClipCollection clips;

    private void OnEnable()
    {
        clips = (ClipCollection)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Save"))
        {
            EdiorMethods.WriteToEnum(clips.path, "ambience", clips.ToStringList(clips.ambience));
            EdiorMethods.WriteToEnum(clips.path, "sfx", clips.ToStringList(clips.sfx));
            EdiorMethods.WriteToEnum(clips.path, "music", clips.ToStringList(clips.music));
        }
    }
}