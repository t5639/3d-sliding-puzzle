namespace ShiftingMaze.Testing
{
    using ShiftingMaze.Puzzle.Management;
    using UnityEngine;

    public class PuzzleMoveController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Tooltip("The controller managing the puzzle.")]
        public PuzzleController controller;
        [Tooltip("The script moving the tiles.")]
        public TileMover mover;
        [Tooltip("The moves to perform.")]
        public CardinalPoint[] inputs;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        int index = 0;
        int count = 0;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Starts the movement chain.
        /// </summary>
        [ContextMenu("Start")]
        public void StartMoves()
        {
            controller.Move(inputs[index]);
            mover.OnMoveEnd += OnEnd;
        }

        /// <summary>
        /// Called when the prevous move was finished.
        /// </summary>
        /// <param name="from">The previous coorinates of the tile.</param>
        /// <param name="tile">The tile moved.</param>
        /// <param name="to">Ther new coordinates of the tile.</param>
        private void OnEnd(Vector2Int from, PuzzleTile tile, Vector2Int to)
        {
            if (++count % 2 == 0)
            {
                index++;
                if (index < inputs.Length)
                {
                    controller.Move(inputs[index]);
                }
                else
                {
                    mover.OnMoveEnd -= OnEnd;
                }
            }
        }
    }
}
