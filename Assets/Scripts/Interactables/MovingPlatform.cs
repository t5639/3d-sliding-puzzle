using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    
    public float min;
    public float max;
    public float current;
    public float value = 0.02f;
    public string axis;


    public void moveUp()
    {
        if (current < max)
        {
            ChangePosition(value);
            current += value;
        }
    }

    public void moveDown()
    {
        if (current > min)
        {
            ChangePosition(-value);
            current -= value;
        }
    }

    public void moveDown(float value2)
    {
        if (current > min)
        {
            ChangePosition(value2);
            current -= value2;
        }
    }

    void ChangePosition(float v)
    {
        switch(axis)
        {
            case "x":
                transform.position = new Vector3(transform.position.x + v, transform.position.y, transform.position.z);
                break;
            case "y":
                transform.position = new Vector3(transform.position.x, transform.position.y + v, transform.position.z);
                break;
            case "z":
                transform.position = new Vector3(transform.position.x + v, transform.position.y, transform.position.z + v);
                break;
        }
    }


}
