namespace ShiftingMaze.Puzzle.Strcuture
{
    using UnityEngine;

    /// <summary>
    /// The base class for all puzzle tiles.
    /// </summary>
    public class TileStructure : ScriptableObject { }

    public abstract class BorderedStructure : TileStructure
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("True if the field can be moved.")]
        private bool moveable = true;
        /// <summary>
        /// True if the field can be moved.
        /// </summary>
        public bool Moveable => moveable && !MoveLock;

        [SerializeField, Tooltip("The entry points of the tile.")]
        private CardinalPoints entries = default;
        /// <summary>
        /// The entry points of the tile.
        /// </summary>
        public CardinalPoints Entries => entries;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// True if the tiles movement is locked.
        /// </summary>
        [field:System.NonSerialized]
        public bool MoveLock { get; set; } = false;
    }
}
