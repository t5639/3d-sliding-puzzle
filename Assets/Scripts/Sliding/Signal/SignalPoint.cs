namespace ShiftingMaze.Signals
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// An achor point for the signals.
    /// </summary>
    public sealed class SignalPoint : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("Signal")] // --------------------------------------------------------
        [SerializeField, LockInPlaymode, Tooltip("The base signal of this point.")]
        private Signal emittingSignal = default;

        [Space, SerializeField, Tooltip("Called with the initial signal value of the point.")]
        private UnityEvent<Signal> onStartSignal = default;

        [SerializeField, Tooltip("Called when the signal changes.")]
        private UnityEvent<Signal> onSignalChange = default;
        /// <summary>
        /// Called when the signal changes.
        /// </summary>
        public event UnityAction<Signal> OnSignalChange
        {
            add => onSignalChange.AddListener(value);
            remove => onSignalChange.RemoveListener(value);
        }

        [Header("Connections")] // ----------------------------------------------------------
        [SerializeField, Tooltip("The connected points")]
        private List<SignalPoint> connectedPoints = default;

        [Space, SerializeField, Tooltip("Called when a new connection was added.")]
        private UnityEvent<ISignalConnection> onConnectionAdded = default;
        /// <summary>
        /// Called when a new connection was added.
        /// </summary>
        public event UnityAction<ISignalConnection> OnConnectionAdded
        {
            add => onConnectionAdded.AddListener(value);
            remove => onConnectionAdded.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when a connection was removed.")]
        private UnityEvent<ISignalConnection> onConnectionRemoved = default;
        /// <summary>
        /// Called when a connection was removed.
        /// </summary>
        public event UnityAction<ISignalConnection> OOnConnectionRemoved
        {
            add => onConnectionRemoved.AddListener(value);
            remove => onConnectionRemoved.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Signal signal = default;
        /// <summary>
        /// The current signal type in the point.
        /// </summary>
        public Signal Signal => signal;

        private Dictionary<SignalPoint, Connection> connections;

        private float sourceDistance = 0f;
        private static HashSet<SignalPoint> pointsInStartup = new HashSet<SignalPoint>();


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                foreach (var connection in connections)
                {
                    Gizmos.color = GetSignalColor(signal);
                    Gizmos.DrawRay(transform.position, connection.Value.Vector * connection.Value.Distance * 0.5f);
                }
                if (sourceDistance == 0f && signal != default)
                {
                    Gizmos.color = SetAlpha(GetSignalColor(signal), Time.time % 1f);
                    Gizmos.DrawWireSphere(transform.position, Time.time % 1f * 0.1f);
                    Gizmos.color = SetAlpha(Gizmos.color, 1f);
                    Gizmos.DrawWireSphere(transform.position, Time.time % 1f * 0.1f + 0.1f);
                    Gizmos.color = SetAlpha(Gizmos.color, 1f - Time.time % 1f);
                    Gizmos.DrawWireSphere(transform.position, Time.time % 1f * 0.1f + 0.2f);
                }
            }
            else
            {
                Gizmos.color = Color.white;
                if (connectedPoints != null)
                {
                    for (int i = 0; i < connectedPoints.Count; i++)
                    {
                        if (connectedPoints[i])
                        {
                            Vector3 direction = connectedPoints[i].transform.position - transform.position;
                            Gizmos.DrawRay(transform.position, direction * 0.5f);
                        }
                    }
                }
            }
        }

        private Color GetSignalColor(Signal signal)
        {
            switch (signal)
            {
                default: return new Color(0f, 0f, 0f, 0f);
                case Signal.None: return Color.white;
                case Signal.Water: return Color.blue;
                case Signal.Magic: return Color.cyan;
                case Signal.Oil: return Color.black;
                case Signal.Fire: return Color.red;
            }
        }

        private Color SetAlpha(Color color, float alpha)
        {
            Color other = color;
            other.a = alpha;
            return other;
        }

#endif
        private void Awake()
        {
            pointsInStartup.Add(this);
            GetConnections();
        }

        private void Start()
        {
            if (emittingSignal != default)
            {
                SetSignal(this);
            }
            foreach (var point in pointsInStartup)
            {
                point.onStartSignal?.Invoke(point.signal);
                point.onStartSignal.RemoveAllListeners();
            }
            pointsInStartup.Clear();
        }

        /// <summary>
        /// Changes the points signal.
        /// </summary>
        /// <param name="source">The source influencing this point.</param>
        private void SetSignal(SignalPoint source)
        {
            // Create a queue to store the points that need editing.
            Queue<Connection> pointsToUpdate = new Queue<Connection>();
            // Create dictionary to store the chaned points.
            Dictionary<SignalPoint, Signal> changedPoints = new Dictionary<SignalPoint, Signal>();
            // Add the current point to the changes.
            changedPoints.Add(this, signal);

            // Change the points signal.
            SetSignal(source, ref pointsToUpdate);

            // Update the points that need editing.
            while (pointsToUpdate.Count > 0)
            {
                // Get the next connection.
                Connection connection = pointsToUpdate.Dequeue();
                // Check if the connections target isn't already part of the changed points.
                if (!changedPoints.ContainsKey(connection.Target))
                {
                    // Add the point.
                    changedPoints.Add(connection.Target, connection.Target.Signal);
                }
                // Update the connection.
                UpdateConnection(connection, ref pointsToUpdate);
            }

            if (pointsInStartup.Count == 0)
            {
                // Iterate over all points with potential changes.
                foreach (var point in changedPoints)
                {
                    // Only invoke event if the points signal has changed over the duration of the update.
                    if (point.Key.signal != point.Value)
                    {
                        point.Key.onSignalChange?.Invoke(point.Key.signal);
                    }
                }
            }
        }
        /// <summary>
        /// Update the points signal.
        /// </summary>
        /// <param name="source">The point influencing this one.</param>
        /// <param name="pointsToUpdate">The queue to add points that need to be updated.</param>
        private void SetSignal(SignalPoint source, ref Queue<Connection> pointsToUpdate)
        {
            // Check if a signal reset is performed.
            if (source == null)
            {
                // Create variable to store the point with the closest source.
                SignalPoint closestSource = this;
                // Create variable to store the distance to the closest source.
                float distanceToClosestSource = sourceDistance;
                // Iterate over all connections to find the point with the closest source.
                foreach (var connection in connections)
                {
                    // Calculate the distance to the connected points source.
                    float distanceToOtherSource = connection.Key.sourceDistance + connection.Value.Distance;
                    // Check if the source of the connected point is closer.
                    if (distanceToOtherSource < distanceToClosestSource)
                    {
                        // Update the closest point.
                        closestSource = connection.Key;
                        // Update the distance to the closest points source.
                        distanceToClosestSource = distanceToOtherSource;
                    }
                }
                // Reset the signal.
                signal = default;
                // Update the signal before updating the connections.
                SetSignal(closestSource, ref pointsToUpdate);
                return;
            }
            else if (source == this)
            {
                // Check if the signal that the point is emitting is stronger than the current s�gnal.
                if (signal <= emittingSignal)
                {
                    // Update the signal.
                    signal = emittingSignal;
                    // Update the source distance.
                    sourceDistance = 0f;
                }
            }
            else if (!connections.ContainsKey(source))
            {
                // Return, the other point is not connected to this point.
                return;
            }
            else if (source.signal <= emittingSignal)
            {
                // Update the signal.
                signal = emittingSignal;
                // Update the source distance.
                sourceDistance = 0f;
            }
            else
            {
                // Check if the signal is changing.
                if (signal < source.signal)
                {
                    // Update the signal.
                    signal = source.signal;
                    // Update the source distance.
                    sourceDistance = source.sourceDistance + connections[source].Distance;
                }
                else if (signal == source.signal)
                {
                    SignalFlowDirection flow = connections[source].GetFlowDirection();
                    // Check which source is closer.
                    if (flow == SignalFlowDirection.CloserTarget)
                    {
                        // Update the source distance.
                        sourceDistance = source.sourceDistance + connections[source].Distance;
                    }
                }
            }
            UpdateConnections(ref pointsToUpdate);
        }

        /// <summary>
        /// Checks all connections and add the ones that need to be updated to the queue.
        /// </summary>
        /// <param name="pointsToUpdate">The queue holding the points that need to be updated.</param>
        private void UpdateConnections(ref Queue<Connection> pointsToUpdate)
        {
            // Iterate over all connections to see if changes need to be applied.
            foreach (var connection in connections)
            {
                if (connection.Key.signal < signal)
                {
                    if (!pointsToUpdate.Contains(connection.Value))
                    {
                        // Add the connected point to the ones to update, this points signal
                        // is stronger and overriding the other ones.
                        pointsToUpdate.Enqueue(connection.Value);
                    }
                }
                else
                {
                    SignalFlowDirection flow = connection.Value.GetFlowDirection();
                    // Check if the connected point has the same signal.
                    if (connection.Key.signal == signal)
                    {
                        // Check which source is closer.
                        if (flow == SignalFlowDirection.CloserOrigin)
                        {
                            if (!pointsToUpdate.Contains(connection.Value))
                            {
                                // Add the connected point to the ones to update.
                                pointsToUpdate.Enqueue(connection.Value);
                            }
                        }
                    }
                    else if (connection.Key.signal > signal)
                    {
                        // Check if the this signal flows to the other point.
                        if (flow > 0)
                        {
                            if (!pointsToUpdate.Contains(connection.Value))
                            {
                                // Add connected point to the ones to update.
                                // A weaker signal canot flow to a point with a stronger one.
                                pointsToUpdate.Enqueue(connection.Value);
                            }
                        }
                    }
                } 
            }
        }
        /// <summary>
        /// Updates a connection by setting the signal of the target.
        /// </summary>
        /// <param name="connection">The connection to update.</param>
        /// <param name="pointsToUpdate">The queue holding the points that need an update.</param>
        private static void UpdateConnection(Connection connection, ref Queue<Connection> pointsToUpdate)
        {
            connection.Target.SetSignal(connection.Origin, ref pointsToUpdate);
        }

        /// <summary>
        /// Adds a connection between two points.
        /// </summary>
        /// <param name="point">The point to connect to.</param>
        public void AddConnection(SignalPoint point)
        {
            if (connections == null)
            {
                connectedPoints.Add(point);
            }
            else if (point && point != this && !connections.ContainsKey(point))
            {
                connections.Add(point, new Connection(this, point));
#if (UNITY_EDITOR)
                connectedPoints.Add(point);
#endif
                point.AddConnection(this);
                if (signal > point.signal || signal == point.signal && (sourceDistance < point.sourceDistance || sourceDistance == point.sourceDistance && ClosestSource() < point.ClosestSource()))
                {
                    point.SetSignal(this);
                }
                onConnectionAdded?.Invoke(connections[point]);
            }
        }

        /// <summary>
        /// Removes a connection between this and another point.
        /// </summary>
        /// <param name="point">The point to disconnect from.</param>
        public void RemoveConnection(SignalPoint point)
        {
            if (connections == null)
            {
                connectedPoints.Remove(point);
            }
            else if (point && connections.ContainsKey(point))
            {
                Connection connection = connections[point];
                connections.Remove(point);
#if (UNITY_EDITOR)
                connectedPoints.Remove(point);
#endif
                point.RemoveConnection(this);
                if (signal < point.signal || signal == point.signal && (sourceDistance > point.sourceDistance || sourceDistance == point.sourceDistance && ClosestSource() > point.ClosestSource()))
                {
                    SetSignal(null);
                }
                OnSignalChange -= connection.UpdateSignal;
                onConnectionRemoved?.Invoke(connection);
            }
        }

        /// <summary>
        /// Find the closes source in the points neighbours.
        /// </summary>
        /// <returns>The distance to the soource.</returns>
        private float ClosestSource()
        {
            float source = sourceDistance;
            foreach (var connection in connections ??= GetConnections())
            {
                if (connection.Value.Target.sourceDistance < source)
                {
                    source = connection.Value.Distance;
                }
            }
            return source;
        }

        /// <summary>
        /// Calls the given action with the initial signal type after setup.
        /// </summary>
        /// <param name="callback">Teh action to call after setup.</param>
        public void GetStartSignal(UnityAction<Signal> callback)
        {
            if (pointsInStartup.Count == 0)
            {
                callback?.Invoke(signal);
            }
            else
            {
                onStartSignal.AddListener(callback);
            }
        }

        /// <summary>
        /// Sets the connected points and returns them.
        /// </summary>
        /// <returns>The connections between the points.</returns>
        private Dictionary<SignalPoint, Connection> GetConnections()
        {
            if (connections == null)
            {
                connections = new Dictionary<SignalPoint, Connection>();
                foreach (SignalPoint point in connectedPoints)
                {
                    connections.Add(point, new Connection(this, point));
                    onConnectionAdded?.Invoke(connections[point]);
                }
#if (!UNITY_EDITOR)
                connectedPoints = null;
#endif
            }
            return connections;
        }

        /// <summary>
        /// Updates the position of the point.
        /// </summary>
        /// <param name="updateConnectedPoints">True if the connected points should also be updated.</param>
        public void UpdatePosition(bool updateConnectedPoints)
        {
            if (updateConnectedPoints)
            {
                HashSet<SignalPoint> updatedPoints = new HashSet<SignalPoint>();
                UpdatePosition(ref updatedPoints);
                return;
            }
            foreach (var connection in connections)
            {
                connection.Value.UpdateVector();
            }
        }
        /// <summary>
        /// Updates the position of the point.
        /// </summary>
        /// <param name="updatedPoints">All the points that have been updated.</param>
        private void UpdatePosition(ref HashSet<SignalPoint> updatedPoints)
        {
            foreach (var connection in connections)
            {
                if (connection.Value.UpdateVector() && !updatedPoints.Contains(connection.Value.Target))
                {
                    connection.Value.Target.UpdatePosition(ref updatedPoints);
                }
            }
        }

#if (UNITY_EDITOR)

        [ContextMenu("Emit/Nothing")]
        private void EmitNothing() => EmitSignal(Signal.None);
        [ContextMenu("Emit/Water")]
        private void EmitWater() => EmitSignal(Signal.Water);
        [ContextMenu("Emit/Magic")]
        private void EmitMagic() => EmitSignal(Signal.Magic);
        [ContextMenu("Emit/Oil")]
        private void EmitOil() => EmitSignal(Signal.Oil);
        [ContextMenu("Emit/Fire")]
        private void EmitFire() => EmitSignal(Signal.Fire);

        private void EmitSignal(Signal signal)
        {
            emittingSignal = signal;
            SetSignal(this);
        }

#endif

        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Stores information about a connection between to points.
        /// </summary>
        private class Connection : ISignalConnection
        {
            // Variables --------------------------------------------------------------------------

            /// <inheritdoc/>
            public Vector3 Vector { get; private set; }
            /// <inheritdoc/>
            public float Distance { get; private set; }
            /// <inheritdoc/>
            public SignalPoint Origin { get; private set; }
            /// <inheritdoc/>
            public SignalPoint Target { get; private set; }
            /// <inheritdoc/>
            public Signal Signal => Origin.signal;
            /// <inheritdoc/>
            public event System.Action<ISignalConnection> OnChange;

            // Constructors -----------------------------------------------------------------------

            /// <summary>
            /// Creates a new instance to store the connection data between two points.
            /// </summary>
            /// <param name="origin">The start of the connection.</param>
            /// <param name="target">The end of the connection.</param>
            public Connection(SignalPoint origin, SignalPoint target)
            {
                Origin = origin;
                Target = target;
                UpdateVector();
                Origin.OnSignalChange += UpdateSignal;
            }

            // Methods ----------------------------------------------------------------------------

            /// <summary>
            /// Tells the connection that the signal has changed.
            /// </summary>
            /// <param name="signal">The new signal type.</param>
            public void UpdateSignal(Signal signal)
            {
                OnChange?.Invoke(this);
            }

            /// <inheritdoc/>
            public SignalFlowDirection GetFlowDirection()
            {
                // Get the difference in distance between the two points and their sources.
                float deltaSourceDistance = Mathf.Abs(Origin.sourceDistance - Target.sourceDistance);
                // Check if the difference is the same as the distance,
                // meaning that one points signal flows to the other.
                if (Mathf.Abs(deltaSourceDistance - Distance) <= 0.0001f)
                {
                    // Check which direction the signal is flowning.
                    if (Distance == 0f)
                    {
                        return SignalFlowDirection.None;
                    }
                    else if (Origin.sourceDistance < Target.sourceDistance)
                    {
                        return SignalFlowDirection.Out;
                    }
                    else
                    {
                        return SignalFlowDirection.In;
                    }
                }
                else if (deltaSourceDistance < Distance)
                {
                    // Return none, the signal is not reaching the other point.
                    return SignalFlowDirection.None;
                }
                else
                {
                    if (Origin.sourceDistance < Target.sourceDistance)
                    {
                        // Return over, the points signal is flowing through the other point.
                        return SignalFlowDirection.CloserOrigin;
                    }
                    else
                    {
                        // Return through, the other points signal is flowing throught the point.
                        return SignalFlowDirection.CloserTarget;
                    }
                }
            }

            /// <summary>
            /// Updates the vector values.
            /// </summary>
            /// <returns>True if there were any changes.</returns>
            public bool UpdateVector()
            {
                if (Target.transform.position == Origin.transform.position)
                {
                    if (Distance != 0f || Vector != Vector3.zero)
                    {
                        Distance = 0f;
                        Vector = Vector3.zero;
                        OnChange?.Invoke(this);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                Vector3 delta = Target.transform.position - Origin.transform.position;
                if (Distance != delta.magnitude || Vector != delta.normalized)
                {
                    Distance = delta.magnitude;
                    Vector = delta.normalized;
                    OnChange?.Invoke(this);
                    return true;
                }
                return false;
            }
        }

        public interface ISignalConnection
        {
            /// <summary>
            /// The direction between the two points (normalized).
            /// </summary>
            Vector3 Vector { get; }
            /// <summary>
            /// The distance between the connected points.
            /// </summary>
            float Distance { get; }
            /// <summary>
            /// The origin point of the connection.
            /// </summary>
            SignalPoint Origin { get; }
            /// <summary>
            /// The target point of the connection.
            /// </summary>
            SignalPoint Target { get; }

            /// <summary>
            /// Called when the connection changed.
            /// </summary>
            event System.Action<ISignalConnection> OnChange;

            /// <summary>
            /// The signal type send throught the connection.
            /// </summary>
            Signal Signal { get; }

            /// <summary>
            /// Calculates the <see cref="SignalFlowDirection"/> of the signal between the points.
            /// </summary>
            /// <returns>The flow direction between the points.</returns>
            SignalFlowDirection GetFlowDirection();
        }

        /// <summary>
        /// Describes the direction of the signal flow.
        /// </summary>
        /// <remarks>
        /// A positive <see cref="SignalFlowDirection"/> measn that the signal is flowing from the
        /// origin in direction of the target.
        /// A negative value measn the opposite.
        /// 0 means that the signal flows from both points to eachother, the signal meets in
        /// the middle of the connection.
        /// </remarks>
        public enum SignalFlowDirection 
        { 
            /// <summary>
            /// The signal is not reaching the other point.
            /// </summary>
            None = 0, 
            /// <summary>
            /// The signal is flowing from the other point through this one.
            /// </summary>
            CloserTarget = -2, 
            /// <summary>
            /// The signal is flowing from the other point into this one.
            /// </summary>
            In = -1, 
            /// <summary>
            /// The signal is flowing from this point to the other one.
            /// </summary>
            Out = 1, 
            /// <summary>
            /// The signal is flowing from this point over the other one.
            /// </summary>
            CloserOrigin = 2 
        }
    }
}
