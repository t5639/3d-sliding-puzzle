using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Options_Sound : MonoBehaviour
{
    public AudioMixer mixer;

    public Slider sliderMaster;
    public Slider sliderSFX;
    public Slider sliderMusic;

    public void SetAll()
    {
        SetMaster();
        SetSFX();
        SetMusic();
    }

    public void SetMaster()
    {
        //Debug.Log(Mathf.Log10(sliderMaster.value) * 20);
        mixer.SetFloat("masterVol", sliderMaster.value);
    }

    public void SetSFX()
    {
        mixer.SetFloat("sfxVol", sliderSFX.value);
        mixer.SetFloat("footVol", sliderSFX.value);
        mixer.SetFloat("ambVol", sliderSFX.value);
    }

    public void SetMusic()
    {
        mixer.SetFloat("musicVol", sliderMusic.value);
    }



}
