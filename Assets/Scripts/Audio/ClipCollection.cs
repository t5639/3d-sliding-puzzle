using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "stepTerrain", menuName = "Audio/ClipCollection")]
public class ClipCollection : ScriptableObject
{
    [System.NonSerialized]
    public string path = "Assets/Scripts/Audio/Enums/";
    public List<NamedAudioClip> ambience, music, sfx;


    public List<string> ToStringList(List<NamedAudioClip> clipList)
    {
        List<string> newList = new List<string>();
        foreach(NamedAudioClip c in clipList)
        {
            newList.Add(c.name);
        }

        return newList;
    }
}

[System.Serializable]
public class NamedAudioClip
{
    public string name;
    public AudioClip clip;
}
