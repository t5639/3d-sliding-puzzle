using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyItem : MonoBehaviour
{
    GameObject itemCamera;

    public void DestroyHandItem()
    {
        itemCamera = GameObject.Find("Item Camera");
        Destroy(itemCamera.transform.GetChild(0).gameObject);
    }
}
