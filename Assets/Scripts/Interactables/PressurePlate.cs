using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    public MovingPlatform movingPlatform;
    public MovingPlatform_anim anim;

    float timeElapsed;
    float fallDuration;
    float fallMax = 2;

    private void OnTriggerStay(Collider other)
    {
        movingPlatform.moveUp();
    }

    private void OnTriggerExit(Collider other)
    {
        timeElapsed = 0;
        StartCoroutine(MoveBack());
    }

    public IEnumerator MoveBack()
    {
        fallDuration = fallMax * (movingPlatform.current / movingPlatform.max);
        Vector3 goal = new Vector3(movingPlatform.transform.position.x, movingPlatform.min, movingPlatform.transform.position.z);

        while (timeElapsed < fallDuration)
        {
            movingPlatform.transform.position = Vector3.Lerp(movingPlatform.transform.position, goal, timeElapsed / (fallDuration));
            timeElapsed += Time.deltaTime;

            yield return null;
        }
        movingPlatform.transform.position = goal;
        movingPlatform.current = movingPlatform.min;

        yield return null;
    }
}
