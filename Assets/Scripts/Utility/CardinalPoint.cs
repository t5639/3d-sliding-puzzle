using System.Collections.Generic;
using UnityEngine;

namespace ShiftingMaze
{
    /// <summary>
    /// The cardinal points.
    /// </summary>
    /// <seealso cref="CardinalPoints"/>
    public enum CardinalPoint { North = 1, East = 2, South = 4, West = 8 }
    /// <summary>
    /// The cardinal points as a flagged enum.
    /// </summary>
    /// <seealso cref="CardinalPoint"/>
    [System.Flags]
    public enum CardinalPoints { None = 0, North = 1, East = 2, South = 4, West = 8 }

    /// <summary>
    /// The connections between the different <see cref="CardinalPoint"/>s.
    /// </summary>
    /// <seealso cref="CardinalConnections"/>
    public enum CardinalConnection
    {
        NorthSouth = 1,
        NorthEast = 2,
        NorthWest = 4,
        SouthEast = 8,
        SouthWest = 16,
        EastWest = 32
    }
    /// <summary>
    /// The connections between the different <see cref="CardinalPoint"/>s as a flagged enum.
    /// </summary>
    /// <seealso cref="CardinalConnection"/>
    [System.Flags]
    public enum CardinalConnections
    {
        None = 0,
        NorthSouth = 1,
        NorthEast = 2,
        NorthWest = 4,
        SouthEast = 8,
        SouthWest = 16,
        EastWest = 32
    }

    public static class CardinalPointUtilities
    {
        /// <summary>
        /// Rotates the directions clockwise.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="amount">The amount of rotations to perform clockwise.</param>
        /// <returns>The rotated points.</returns>
        public static CardinalPoints RotatateClockwise(CardinalPoints points, int amount) => (CardinalPoints)RotatateClockwise((int)points, amount);
        /// <summary>
        /// Rotates the directions clockwise.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="degrees">The rotation.</param>
        /// <returns>The rotated points.</returns>
        public static CardinalPoints RotatateClockwise(CardinalPoints points, float degrees) => (CardinalPoints)RotatateClockwise((int)points, degrees);
        /// <summary>
        /// Rotates the directions clockwise.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="amount">The amount of rotations to perform clockwise.</param>
        /// <returns>The rotated points.</returns>
        public static CardinalPoint RotatateClockwise(CardinalPoint points, int amount) => (CardinalPoint)RotatateClockwise((int)points, amount);
        /// <summary>
        /// Rotates the directions clockwise.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="degrees">The rotation.</param>
        /// <returns>The rotated points.</returns>
        public static CardinalPoint RotatateClockwise(CardinalPoint points, float degrees) => (CardinalPoint)RotatateClockwise((int)points, degrees);
        /// <summary>
        /// Rotates the directions clockwise.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="amount">The amount of rotations to perform clockwise.</param>
        /// <returns>The rotated points.</returns>
        private static int RotatateClockwise(int points, int amount)
        {
            while (amount < 0)
            {
                amount += 4;
            }
            int i = points << (amount % 4);
            while (i >= 16)
            {
                i -= 15;
            }
            return i;
        }
        /// <summary>
        /// Rotates the directions clockwise.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="degrees">The rotation.</param>
        /// <returns>The rotated points.</returns>
        private static int RotatateClockwise(int points, float degrees) => RotatateClockwise(points, Mathf.RoundToInt((degrees + 316f) / 90f));

        /// <summary>
        /// Converts a <see cref="Vector2"/> to a cardinal point.
        /// </summary>
        /// <param name="vector">The vector to convert.</param>
        /// <returns>The converted point.</returns>
        public static CardinalPoint Vector2ToPoint(Vector2 vector)
        {
            if (vector.sqrMagnitude == 0f)
            {
                return default;
            }
            if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
            {
                return vector.x > 0 ? CardinalPoint.East : CardinalPoint.West;
            }
            else
            {
                return vector.y > 0 ? CardinalPoint.North : CardinalPoint.South;
            }
        }
    }

    /// <summary>
    /// A class extendint the functionality of <see cref="CardinalConnection"/>s.
    /// </summary>
    public static class CardinalConnectionsExtention
    {
        /// <summary>
        /// Converts the <see cref="CardinalConnection"/> to two <see cref="CardinalPoint"/>s.
        /// </summary>
        /// <param name="connection">The connection to convert.</param>
        /// <returns>The two points of the connection.</returns>
        public static (CardinalPoint from, CardinalPoint to) ToPoint(this CardinalConnection connection)
        {
            switch (connection)
            {
                default: return default;
                case CardinalConnection.NorthSouth: return (CardinalPoint.North, CardinalPoint.South);
                case CardinalConnection.NorthEast: return (CardinalPoint.North, CardinalPoint.East);
                case CardinalConnection.NorthWest: return (CardinalPoint.North, CardinalPoint.West);
                case CardinalConnection.SouthEast: return (CardinalPoint.South, CardinalPoint.East);
                case CardinalConnection.SouthWest: return (CardinalPoint.South, CardinalPoint.West);
                case CardinalConnection.EastWest: return (CardinalPoint.East, CardinalPoint.West);
            }
        }

        /// <summary>
        /// Converts the <see cref="CardinalConnection"/> to a <see cref="CardinalPoints"/> value.
        /// </summary>
        /// <param name="connection">The connection to convert.</param>
        /// <returns>The <see cref="CardinalPoints"/> from the connection.</returns>
        public static CardinalPoints ToPoints(this CardinalConnection connection)
        {
            switch (connection)
            {
                default: return default;
                case CardinalConnection.NorthSouth: return CardinalPoints.North | CardinalPoints.South;
                case CardinalConnection.NorthEast: return CardinalPoints.North | CardinalPoints.East;
                case CardinalConnection.NorthWest: return CardinalPoints.North | CardinalPoints.West;
                case CardinalConnection.SouthEast: return CardinalPoints.South | CardinalPoints.East;
                case CardinalConnection.SouthWest: return CardinalPoints.South | CardinalPoints.West;
                case CardinalConnection.EastWest: return CardinalPoints.East | CardinalPoints.West;
            }
        }

        /// <summary>
        /// Converts the <see cref="CardinalPoint"/> to a <see cref="Vector2Int"/>.
        /// </summary>
        /// <param name="direction">The direction to convert.</param>
        /// <returns>The converted <see cref="Vector2Int"/>.</returns>
        public static Vector2Int ToVector2Int(this CardinalPoint direction)
        {
            switch (direction)
            {
                default: return Vector2Int.zero;
                case CardinalPoint.North: return Vector2Int.up;
                case CardinalPoint.East: return Vector2Int.right;
                case CardinalPoint.South: return Vector2Int.down;
                case CardinalPoint.West: return Vector2Int.left;
            }
        }

        /// <summary>
        /// Inverts the direction of a <see cref="CardinalPoint"/>.
        /// </summary>
        /// <param name="direction">The direction to invert.</param>
        /// <returns>The inverted <see cref="CardinalPoint"/>.</returns>
        public static CardinalPoint Invert(this CardinalPoint direction)
        {
            switch (direction)
            {
                default: return default;
                case CardinalPoint.North: return CardinalPoint.South;
                case CardinalPoint.East: return CardinalPoint.West;
                case CardinalPoint.South: return CardinalPoint.North;
                case CardinalPoint.West: return CardinalPoint.East;
            }
        }

        public static IEnumerable<CardinalConnection> EachConnection(this CardinalPoint point)
        {
            switch (point)
            {
                case CardinalPoint.North:
                    yield return CardinalConnection.NorthEast;
                    yield return CardinalConnection.NorthSouth;
                    yield return CardinalConnection.NorthWest;
                    break;
                case CardinalPoint.East:
                    yield return CardinalConnection.SouthEast;
                    yield return CardinalConnection.EastWest;
                    yield return CardinalConnection.NorthEast;
                    break;
                case CardinalPoint.South:
                    yield return CardinalConnection.SouthWest;
                    yield return CardinalConnection.NorthSouth;
                    yield return CardinalConnection.SouthEast;
                    break;
                case CardinalPoint.West:
                    yield return CardinalConnection.NorthWest;
                    yield return CardinalConnection.EastWest;
                    yield return CardinalConnection.SouthWest;
                    break;
            }
        }
    }
}
