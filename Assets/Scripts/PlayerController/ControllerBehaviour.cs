namespace ShiftingMaze.Player
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    public abstract class ControllerBehaviour : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script managing the controller.")]
        protected PlayerController controller = default;

        [Space, SerializeField, Tooltip("Called whent the controller gets enabled.")]
        protected UnityEvent onEnabled = default;

        [SerializeField, Tooltip("Called when the controller got disabled.")]
        protected UnityEvent onDisabled = default;

        // --- | Variables | -----------------------------------------------------------------------------------------------

        private static Dictionary<int, Dictionary<System.Type, ControllerBehaviour>> controllerByID 
            = new Dictionary<int, Dictionary<System.Type, ControllerBehaviour>>();


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // MonoBehaviour --------------------------------------------------------------------------

        protected virtual void Awake()
        {
            RegisterController();
        }

        protected virtual void OnDestroy()
        {
            UnregisterController();
        }

        protected virtual void OnEnable()
        {
            onEnabled?.Invoke();   
        }

        protected virtual void OnDisable()
        {
            onDisabled?.Invoke();
        }

        // Controller -----------------------------------------------------------------------------

        /// <summary>
        /// Registers the controller on the gamaobject.
        /// </summary>
        private void RegisterController()
        {
            int id = controller.GetInstanceID();
            if (!controllerByID.ContainsKey(id))
            {
                controllerByID[id] = new Dictionary<System.Type, ControllerBehaviour>();
            }
            controllerByID[id][GetType()] = this;
        }

        /// <summary>
        /// Unregisters the controller from the gameobject.
        /// </summary>
        private void UnregisterController()
        {
            int id = controller.GetInstanceID();
            controllerByID[id].Remove(GetType());
            if (controllerByID[id].Count == 0)
            {
                controllerByID.Remove(id);
            }
        }

        /// <summary>
        /// Looks for a controller on the object.
        /// </summary>
        /// <typeparam name="T">The type of the controller.</typeparam>
        /// <param name="player">The player controller to look for controller behaviours.</param>
        /// <returns>The controller if one was found.</returns>
        public static T GetController<T>(PlayerController player) where T: ControllerBehaviour
        {
            if (controllerByID.TryGetValue(player.GetInstanceID(), out var controllerSet))
            {
                if (controllerSet.TryGetValue(typeof(T), out ControllerBehaviour controller))
                {
                    return (T)controller;
                }
            }
            return null;
        }
    }
}
