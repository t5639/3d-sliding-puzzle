namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using UnityEngine;

    public class TestTileBuilder : PuzzleTileBuilder
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The prefab to use to build the puzzle.")]
        private TestTileDisplay tilePrefab = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override Vector2 TileSize => tilePrefab.Size;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override PuzzleTile BuildTile(TileStructure structure, CardinalPoints borders)
        {
            TestTileDisplay display = Instantiate(tilePrefab);
            display.Display(structure);
            return PuzzleTile.AddComponent(display.gameObject, structure);
        }
    }
}
