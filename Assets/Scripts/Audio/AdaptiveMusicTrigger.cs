using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptiveMusicTrigger : MonoBehaviour
{
    public MusicLoopStartStop[] Actions;
    public bool triggerOnStart;
    public bool multipleTriggers = false;


    private void Start()
    {
        if (triggerOnStart)
        {
            Trigger();
        }
    }

    private void DoAction(MusicLoopStartStop act)
    {
        if(act.action == MusicLoopStartStop.opt.start)
        {
            if (act.endlessLoop)
            {
                AdaptiveMusicManager.Instance.Play(act.musicLoop);
            }
            else
            {
                AdaptiveMusicManager.Instance.Play(act.musicLoop, act.loopCount);
            }
        }
        else
        {

            AdaptiveMusicManager.Instance.Stop(act.musicLoop);
        }
        this.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!triggerOnStart && other.gameObject.tag == "Player")
        {
            Trigger();
        }
    }

    public void Trigger()
    {
        foreach (MusicLoopStartStop act in Actions)
        {
            DoAction(act);
        }
    }
}


[System.Serializable]
public class MusicLoopStartStop
{
    public enum opt { start, stop};
    public music musicLoop;
    public opt action;
    public bool endlessLoop = true;
    public int loopCount = 0;
    
}
