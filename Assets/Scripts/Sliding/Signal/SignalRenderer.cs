namespace ShiftingMaze.Signals
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using static ShiftingMaze.Signals.SignalPoint;

    public class SignalRenderer : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The prefab for the signal.")]
        private LineRenderer prefab = default;

        [SerializeField, Tooltip("The colors for the different signals.")]
        private SignalCollection<Color> signalColors = new SignalCollection<Color>
        (
            new Color(1.0f, 1.0f, 1.0f, 0.0f),
            new Color(0.4f, 0.5f, 1.0f, 1.0f),
            new Color(0.5f, 1.0f, 1.0f, 1.0f),
            new Color(0.0f, 0.0f, 0.0f, 1.0f),
            new Color(0.5f, 0.2f, 0.1f, 1.0f)
        );

        [SerializeField, Tooltip("The renderers that are currently not used.")]
        private List<LineRenderer> unusedRenderer = new List<LineRenderer>();


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        private Transform Container => transform;

        /// <summary>
        /// The minimum distance to render.
        /// </summary>
        private const float MIN_DISTANCE = 0.005f;

        private Dictionary<ISignalConnection, SignalConnection> connections = new Dictionary<ISignalConnection, SignalConnection>();

        private AnimationCurve switchMotion = new AnimationCurve(new Keyframe(), new Keyframe(0.15f, 1f));


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public void RenderConnection(ISignalConnection connection)
        {
            if (!connections.ContainsKey(connection))
            {
                connections.Add(connection, new SignalConnection(GetUnusedRenderer()));
                connection.OnChange += UpdateConnection;
            }
            UpdateConnection(connection);
        }

        public void RemoveConnection(ISignalConnection connection)
        {
            if (connections.ContainsKey(connection))
            {
                connection.OnChange -= UpdateConnection;
                connections[connection].renderer.gameObject.SetActive(false);
                unusedRenderer.Add(connections[connection].renderer);
                if (connections[connection].update != null)
                {
                    StopCoroutine(connections[connection].update);
                }
                connections.Remove(connection);
            }
        }

        private void UpdateConnection(ISignalConnection connection)
        {
            if (connections.ContainsKey(connection))
            {
                if (connection.Distance < MIN_DISTANCE)
                {
                    connections[connection].gameObject.SetActive(false);
                }
                else
                {
                    connections[connection].renderer.SetPosition(1, connections[connection].transform.InverseTransformVector(connection.Vector) * connection.Distance * 0.5f);
                    if (connections[connection].update != null)
                    {
                        StopCoroutine(connections[connection].update);
                    }
                    connections[connection].update = StartCoroutine(DoSwitchColor(connections[connection], connections[connection].color, signalColors.GetValue(connection.Signal)));
                }
            }
            else
            {
                RenderConnection(connection);
            }
        }

        private IEnumerator DoSwitchColor(SignalConnection line, Color prev, Color next)
        {
            float time = 0f;
            float duration = switchMotion[switchMotion.length - 1].time;
            line.renderer.gameObject.SetActive(true);
            while (time < duration)
            {
                time += Time.deltaTime;
                line.color = Color.Lerp(prev, next, switchMotion.Evaluate(time));
                yield return null;
            }
            if (next.a == 0f)
            {
                line.renderer.gameObject.SetActive(false);
            }
        }

        private LineRenderer GetUnusedRenderer()
        {
            LineRenderer renderer;
            if (unusedRenderer.Count > 0)
            {
                renderer = unusedRenderer[0];
                unusedRenderer.RemoveAt(0);
                return renderer;
            }
            else
            {
                renderer = Instantiate(prefab, Container);
                renderer.startColor = signalColors.None;
                renderer.endColor = signalColors.None;
            }
            return renderer;
        }

        private class SignalConnection
        {
            public LineRenderer renderer;
            public Coroutine update;

            public Color color
            {
                get => renderer.startColor;
                set => renderer.startColor = renderer.endColor = value;
            }

            public GameObject gameObject => renderer.gameObject;

            public Transform transform => renderer.transform;

            public SignalConnection(LineRenderer renderer, Coroutine update)
            {
                this.renderer = renderer;
                this.update = update;
            }
            public SignalConnection(LineRenderer renderer) : this(renderer, null) { }
        }
    }
}
