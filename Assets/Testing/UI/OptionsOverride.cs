using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace bugiflo
{

    public class OptionsOverride : MonoBehaviour
    {
        public Options_Sound sound;
        public Options_Graphics graphics;
        public Options_Controls controls;

        public float soundMas = -10, soundSfx = -10, soundMus = -10;
        public float gamma = 0.2f;
        public float sensit = 10;
        public bool control = true;

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        private void Start()
        {
            UpdateOptions();
        }

        private void OnLevelWasLoaded(int level)
        {
            UpdateOptions();
        }

        public void UpdateOptions()
        {
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                sound = Resources.FindObjectsOfTypeAll<Options_Sound>()[0];
                graphics = Resources.FindObjectsOfTypeAll<Options_Graphics>()[0];
                controls = Resources.FindObjectsOfTypeAll<Options_Controls>()[0];
            }

            UploadVals();

            sound.SetAll();
            graphics.SetGamma();
            controls.SetAll();
        }

        public void StoreVals()
        {
            soundMas = sound.sliderMaster.value;
            soundSfx = sound.sliderSFX.value;
            soundMus = sound.sliderMusic.value;

            gamma = graphics.sliderGamma.value;

            sensit = controls.sensitSlider.value;
            control = controls.controlMode;
        }

        public void UploadVals()
        {
            sound.sliderMaster.value = soundMas;
            sound.sliderSFX.value = soundSfx;
            sound.sliderMusic.value = soundMus;

            graphics.sliderGamma.value = gamma;

            controls.sensitSlider.value = sensit;
            controls.controlMode = control;
        }


    }
}
