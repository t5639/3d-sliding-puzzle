namespace ShiftingMaze.Puzzle.Management
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class TileMover : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script managing the puzzles layout.")]
        private PuzzleMatrix manager = default;

        [SerializeField, Tooltip("The movement of the tile.")]
        private AnimationCurve movement = new AnimationCurve(new Keyframe(), new Keyframe(0.3f, 1f));

        [SerializeField, Tooltip("True if the tiles origin is in the center.")]
        private bool tileOriginInCenter = false;

        [Space, SerializeField, Tooltip("Called when a tile is starting to move.")]
        private UnityEvent<Vector2Int, PuzzleTile, Vector2Int> onMoveStart;
        /// <summary>
        /// Called when a tile is starting to move.
        /// </summary>
        public event UnityAction<Vector2Int, PuzzleTile, Vector2Int> OnMoveStart
        {
            add => onMoveStart.AddListener(value);
            remove => onMoveStart.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when a tile has been moved.")]
        private UnityEvent<Vector2Int, PuzzleTile, Vector2Int> onMoveEnd;
        /// <summary>
        /// Called when a tile has been moved.
        /// </summary>
        public event UnityAction<Vector2Int, PuzzleTile, Vector2Int> OnMoveEnd
        {
            add => onMoveEnd.AddListener(value);
            remove => onMoveEnd.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Vector2 tileDimensions = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            manager.OnMoved += MoveTile;
            manager.OnSynchronized += UpdatePuzzle;
            tileDimensions = manager.Size / new Vector2(manager.Width, manager.Height);
        }

        private void OnDisable()
        {
            manager.OnSynchronized -= UpdatePuzzle;
            manager.OnMoved -= MoveTile;
        }

        /// <summary>
        /// Move a tile from a position to a new one.
        /// </summary>
        /// <param name="from">The current position.</param>
        /// <param name="tile">The tile to move.</param>
        /// <param name="to">The target position.</param>
        public void MoveTile(Vector2Int from, PuzzleTile tile, Vector2Int to)
        {
            tile.name = UpdateName(tile.name, to.x, to.y);
            onMoveStart?.Invoke(from, tile, to);
            StartCoroutine(DoMove(CoordinateToPosition(from), tile, CoordinateToPosition(to), () => onMoveEnd?.Invoke(from, tile, to)));
        }

        /// <summary>
        /// Applys the changes of a matrix.
        /// </summary>
        /// <param name="moves"></param>
        private void UpdatePuzzle(IEnumerable<(PuzzleTile tile, Vector2Int coorinate)> moves)
        {
            foreach (var move in moves)
            {
                move.tile.transform.localPosition = CoordinateToPosition(move.coorinate);
            }
        }

        /// <summary>
        /// Updates the name so it will display the current coordinates.
        /// </summary>
        /// <param name="name">The current name of the tile.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <returns>The new name for the tile.</returns>
        private string UpdateName(string name, int x, int y)
        {
            int start = name.LastIndexOf('[');
            int end = name.LastIndexOf(']');
            if (start < 0)
            {
                return $"[{x}/{y}]" + name;
            }
            else
            {
                return name.Substring(0, start + 1) + $"{x}/{y}" + (end > 0 ? name.Substring(end, name.Length - end) : "");
            }
        }

        /// <summary>
        /// Converts a cooridinate to a position in the map.
        /// </summary>
        /// <param name="coordinate">the coordinate to convert.</param>
        /// <returns>The position in the map.</returns>
        private Vector3 CoordinateToPosition(Vector2Int coordinate)
        {
            return manager.LocalOrigin + new Vector3
            (
                coordinate.x * tileDimensions.x, 
                0f, 
                coordinate.y * tileDimensions.y
            ) + (tileOriginInCenter ? new Vector3(tileDimensions.x, 0f, tileDimensions.y) * 0.5f : Vector3.zero);
        }

        /// <summary>
        /// Animates the tile from one position to another.
        /// </summary>
        /// <param name="from">The starting point.</param>
        /// <param name="tile">The tile moving.</param>
        /// <param name="to">The target position.</param>
        /// <param name="callback">Called when the tile was moved.</param>
        private IEnumerator DoMove(Vector3 from, PuzzleTile tile, Vector3 to, Action callback)
        {
            float startTime = Time.time;
            float duration = movement[movement.length - 1].time;
            while (Time.time - startTime <= duration)
            {
                tile.transform.localPosition = Vector3.Lerp(from, to, movement.Evaluate(Time.time - startTime));
                yield return null;
            }
            tile.transform.localPosition = to;
            callback?.Invoke();
        }
    }
}
