namespace ShiftingMaze
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public static class Util
    {

    }

    public static class Vecto3Extentions
    {
        /// <summary>
        /// Returns the vector without the x-axis.
        /// </summary>
        /// <param name="vector">The vector to edit.</param>
        /// <returns>The vector without the x-axis</returns>
        public static Vector3 IgnoreX(this Vector3 vector) => ScaleVector(ref vector, 0f, 1f, 1f);
        /// <summary>
        /// Returns the vector without the y-axis.
        /// </summary>
        /// <param name="vector">The vector to edit.</param>
        /// <returns>The vector without the y-axis</returns>
        public static Vector3 IgnoreY(this Vector3 vector) => ScaleVector(ref vector, 1f, 0f, 1f);
        /// <summary>
        /// Returns the vector without the z-axis.
        /// </summary>
        /// <param name="vector">The vector to edit.</param>
        /// <returns>The vector without the z-axis</returns>
        public static Vector3 IgnoreZ(this Vector3 vector) => ScaleVector(ref vector, 1f, 1f, 0f);

        /// <summary>
        /// Returns the vector with the x-axis only.
        /// </summary>
        /// <param name="vector">The vector to edit.</param>
        /// <returns>The vector without the x-axis</returns>
        public static Vector3 SelectX(this Vector3 vector) => ScaleVector(ref vector, 1f, 0f, 0f);
        /// <summary>
        /// Returns the vector with the y-axis only.
        /// </summary>
        /// <param name="vector">The vector to edit.</param>
        /// <returns>The vector without the y-axis</returns>
        public static Vector3 SelectY(this Vector3 vector) => ScaleVector(ref vector, 0f, 1f, 0f);
        /// <summary>
        /// Returns the vector with the z-axis only.
        /// </summary>
        /// <param name="vector">The vector to edit.</param>
        /// <returns>The vector without the z-axis</returns>
        public static Vector3 SelectZ(this Vector3 vector) => ScaleVector(ref vector, 0f, 0f, 1f);

        public static Vector3 SignedDelta(this Vector3 vector)
        {
            for (int i = 0; i < 3; i++)
            {
                vector[i] = vector[i] > 0f ? 1f - vector[i] : vector[i] + 1f;
            }
            return vector;
        }

        private static Vector3 ScaleVector(ref Vector3 vector, params float[] scale)
        {
            for (int i = 0; i < 3; i++)
            {
                vector[i] *= scale[i];
            }
            return vector;
        }
    }
}
