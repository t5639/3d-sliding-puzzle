using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioZone : MonoBehaviour
{
    public enum AudioZoneType { Three_D, Two_D};
    public AudioSource source;
    public AudioZoneType type;
    public Vector3 size;
    public Vector3 falloff;
    public bool syncWithMusic = false;
    private BoxCollider coll;
    private bool inRange = false;
    private float setVolume;
    private Transform player;

    private void Awake()
    {
        if (type == AudioZoneType.Two_D)
        {
            size += new Vector3(0, 100000, 0);

        }

        transform.rotation = Quaternion.identity;
        setVolume = source.volume;
        coll = this.gameObject.AddComponent<BoxCollider>();
        Rigidbody rb = this.gameObject.AddComponent<Rigidbody>();
        coll.size = size + falloff*2;
        coll.isTrigger = true;
        rb.isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (inRange)
        {
            Vector3 playerPos = player.position;
            Vector3 max = new Vector3(transform.position.x + size.x / 2, transform.position.y + size.y / 2, transform.position.z + size.z / 2);
            Vector3 min = new Vector3(transform.position.x - size.x / 2, transform.position.y - size.y / 2, transform.position.z - size.z / 2);

            Vector3 diff = new Vector3();
            float maxDiff = 0;
            int maxAxis = -1;

            for(int i = 0; i < 3; i++)
            {
                diff[i] = playerPos[i] > max[i] ? playerPos[i] - max[i] : playerPos[i] < min[i] ? playerPos[i] - min[i] : 0;

                if (Mathf.Abs(diff[i]) > maxDiff)
                {
                    maxDiff = Mathf.Abs(diff[i]);
                    maxAxis = i;
                }
            }

            //Debug.Log(diff+" max= "+maxAxis);

            if (maxAxis == -1)
            {
                source.volume = setVolume;
            }
            else if (diff[maxAxis] > 0)
            {
                float maxD = max[maxAxis] + falloff[maxAxis];
                Debug.Log(playerPos[maxAxis] + " from " + max[maxAxis] + " to " + maxD);
                source.volume = MapToOne(playerPos[maxAxis], max[maxAxis] + falloff[maxAxis], max[maxAxis]) * setVolume;
            }
            else
            {
                source.volume = MapToOne(playerPos[maxAxis], min[maxAxis] - falloff[maxAxis], min[maxAxis] ) * setVolume;
            }


            /*
            float x = playerPos.x > max.x ? playerPos.x - max.x : playerPos.x < min.x ? playerPos.x - min.x : 0;
            float y = playerPos.y > max.y ? playerPos.y - max.y : playerPos.y < min.y ? playerPos.y - min.y : 0;
            float z = playerPos.z > max.z ? playerPos.z - max.z : playerPos.z < min.z ? playerPos.z - min.z : 0;
            */
            
        }

    }

    private float MapToOne(float val, float from, float to)
    {
        {
            return (1 / (to - from)) * (val-from);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Color c = Color.blue;
        c.a = 0.5f;
        Gizmos.color = c;

        if (type == AudioZoneType.Two_D)
        {
            size.y = 0;
            falloff.y = 0;
        }

        Gizmos.DrawCube(transform.position, size);
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(transform.position, size);
        Gizmos.DrawWireCube(transform.position, size+falloff*2);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            inRange = true;
            player = other.gameObject.transform;
            if (syncWithMusic)
            {
                AdaptiveMusicManager.Instance.PlaySource(source);
            }
            else
            {
                source.Play();

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            inRange = false;
            player = null;
            source.Stop();
        }
    }
}
