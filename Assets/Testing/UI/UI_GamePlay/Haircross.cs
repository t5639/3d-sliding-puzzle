using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Haircross : MonoBehaviour
{

    public GameObject hairinAnim;
    public GameObject hairoutAnim;
    public GameObject hairin;
    public GameObject hairout;



    public void setAnim()
    {
        hairinAnim.SetActive(true);
        hairoutAnim.SetActive(true);
        hairin.SetActive(false);
        hairout.SetActive(false);
    }

    public void setStandard()
    {
        hairinAnim.SetActive(false);
        hairoutAnim.SetActive(false);
    }
}
