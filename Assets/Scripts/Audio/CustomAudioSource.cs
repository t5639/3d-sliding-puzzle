using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

public class CustomAudioSource : MonoBehaviour
{
    private AudioSource source;
    public bool threeD = true;
    public bool loop = false;
    public float minRange = 3, maxRange = 6;
    [Range(0, 1)]
    public float volume = 1;
    public AudioMixerGroup mixer;

    public UnityEvent OnStart;

    private float fadeTime;

    private float fadeTimer = -1;
    private bool fadeIn = false, fadeOut = false;
    private float vol = 0;


    private void Awake()
    {
        GameObject go = new GameObject("AudioSource"+this.gameObject.ToString());
        go.transform.position = this.transform.position;
        source = go.AddComponent<AudioSource>();

        
    }

    // Start is called before the first frame update
    void Start()
    {
        source.spatialBlend = threeD ? 1.0f : 0.0f;
        source.outputAudioMixerGroup = AudioManager.Instance.sfxMix;
        source.minDistance = minRange;
        source.maxDistance = maxRange;
        source.volume = volume;
        source.loop = loop;
        source.playOnAwake = false;
        source.rolloffMode = AudioRolloffMode.Linear;
        source.outputAudioMixerGroup = mixer;

        OnStart.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeIn || fadeOut)
        {
            fadeTimer -= Time.deltaTime;
            vol = fadeOut ? 1 / fadeTime * fadeTimer : 1 / fadeTime * (fadeTime - fadeTimer);
            source.volume = vol;
            if (fadeTimer <= 0)
            {
                source.volume = fadeOut ? 0 : 1;
                if (fadeOut)
                {
                    source.Stop();
                    fadeOut = false;
                }
                else
                {
                    fadeIn = false;

                }
            }
        }
    }

    public void Play(AudioClip clip = null)
    {
        if (clip != null)
        {
            source.clip = clip;
        }
        source.Play();
    }

    public void SetClip(AudioClip clip)
    {
        source.clip = clip;
    }

    public void SetStartTime(float startTime = 0)
    {
        source.time = startTime;
    }


    public void PlayFadeIn(float fadeTime)
    {
        this.fadeTime = fadeTime;
        fadeIn = true;
        fadeTimer = fadeTime;
        vol = 0;
        source.volume = vol;
        source.Play();
    }

    public void PlayFadeOut(float fadeTime)
    {
        this.fadeTime = fadeTime;
        fadeOut = true;
        fadeTimer = fadeTime;
        vol = 1;
        source.volume = vol;
    }

    public bool IsPlaying()
    {
        return source.isPlaying;
    }

    private void OnDrawGizmosSelected()
    {
        if (threeD) { 
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, maxRange);
        Gizmos.DrawWireSphere(transform.position, minRange);
        Gizmos.color = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.4f) ;
        Gizmos.DrawSphere(transform.position, minRange);
        }
        else
        {
            minRange = 0;
            maxRange = 0;
        }
    }

}
