namespace ShiftingMaze.Puzzle.Management
{
    using ShiftingMaze.Puzzle.Strcuture;
    using UnityEngine;
    using UnityEngine.Events;

    public class Room : PuzzleTile
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the room was entered.")]
        protected UnityEvent onEnter = default;

        [SerializeField, Tooltip("Called when the room was left.")]
        protected UnityEvent onExit = default;

        [SerializeField, Tooltip("Called when the room gets discovered.")]
        protected UnityEvent onDiscover = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private bool isDiscovered = false;
        private bool isActive = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            UpdateLock();
        }

        /// <summary>
        /// Sets the room active.
        /// </summary>
        public void EnterRoom()
        {
            DiscoverRoom();
            isActive = true;
            UpdateLock();
            onEnter?.Invoke();
        }

        /// <summary>
        /// Sets the room inactive.
        /// </summary>
        public void LeaveRoom()
        {
            isActive = false;
            UpdateLock();
            onExit?.Invoke();
        }
        [ContextMenu("Discover")]
        private void DiscoverRoom()
        {
            if (!isDiscovered)
            {
                isDiscovered = true;
                onDiscover?.Invoke();
            }
        }


        /// <summary>
        /// Updates the locked state.
        /// </summary>
        protected void UpdateLock()
        {
            if (structure is BorderedStructure)
            {
                ((BorderedStructure)structure).MoveLock = isDiscovered && !isActive;
            }
        }
    }
}
